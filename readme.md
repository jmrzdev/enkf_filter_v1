

# Ensemble Kalman Filter coupled with HydroGeoSphere
*Project: for Master Thesis*  
*@autor: Jaime Martinez Rodriguez*  
*jmartinez.rgz@gmail.com*  
*Date: 06/2021*  


This project uses the Ensemble Kalman filter method to make a parameter optimization of an integrated hydrological model. The software used for the integrated model is HydroGeoSphere(HGS).

This version includes the optimization of Hydraulic conductivity and the Van Genuchten parameters for the unsaturated zone alpha and beta.

## Prerequisites
* Python 3.7 or later
* The software HydrogeoSphere is needed, the information can be found at: https://www.aquanty.com/hydrogeosphere

## Installation
clone the repository on a directory with adminsitrator rights to avoid issues with writing and file creation.
```bash
cd <directory you want to install to>
git clone https://jmartinez_rgz@bitbucket.org/jmartinez_rgz/enkf_filter_v1.git
```


## Usage
* The main file required for the initialization of the filter is "enkf\_hgs.py" and is located in main_dir/\_enkf/
* The location of the HGS model template is located at main_dir/\_da/hgs\_name/hgs\_mode\_test\_name/hgs_instance/

A sample test is already pre-configured and can be run with the definition of the following variables:
```python
model_name = 'hgs_model'
test_name = 'hgs_model_test'  #
times_file = 'output_times.txt'
data_file = 'model_data.dat'
```
The filter contains a parallel computing implementation in which the following variables can be defined.
```python
parallel = True
cpus = 4
```
## Authors and acknowledgment
This code is based on Emilio Sanchez version, which repository can be found at https://bitbucket.org/eesl_01/enkf_2020
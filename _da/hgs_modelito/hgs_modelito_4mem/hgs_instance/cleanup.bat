del modelito_jmro.*
del scratch*
del local*
del *.dbg
::del parallel*
del debug*
del progress*
del *.control
del *.prf, *.rec, *.sen, *.rst, *.cnd
del *.log, *.rmr

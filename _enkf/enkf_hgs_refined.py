"""
Modified version of the Lotka-Volterra model
to HGS

Created on 12.12.2020
@author: Emilio Sanchez-Leon
Modified on 05.05.2020
by Jaime Martinez
"""
import os
import sys
import time
import pickle
import shutil
import numpy as np, matplotlib.pylab as plt
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..','_enkf')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__),'..', 'models')))

#sys.path.append(r'C:\Users\Jaime\Documents\1_Master_AEG\SP_&_Thesis\SP2\5_EnKF\_enkf')
#sys.path.append(r'C:\Users\Jaime\Documents\1_Master_AEG\SP_&_Thesis\SP2\5_EnKF')

import enkf_utilities.gwt as gwt
import enkf_utilities.forwardmodel as fw
import enkf_utilities.gridmanip as gm
import enkf_utilities.dirs as dm
import enkf_utilities.kalman as kf
import enkf_utilities.data as dc
import enkf_utilities.plotResults as pptr
import enkf_utilities.fieldgen as fg
import enkf_utilities.hgs as hg
import enkf_utilities.mystats as mysst
# todo: Add localization
# todo: Add additional parameters like S, poros, etc.
# todo: Test it with updates not every observation
# todo: add possible update of states
# todo: include different update schemes

if __name__ == '__main__':
    """ Start user settings section """
        # Source directories
    homedir = os.path.abspath(os.path.join(os.path.curdir, '..'))
    model_dir  = os.path.join(homedir, 'models')
    da_dir     = os.path.join(homedir, '_da')

        # Model specific information
    model_name = 'hgs_refined'
    test_name = 'hgs_refined_case1_new'  #
    #test_name = 'hgs_refined_case2_new'  #
    #test_name = 'hgs_refined_case3_new'  #
    #test_name = 'hgs_refined_case4_new'  #
    #test_name = 'hgs_refined_case5_65nem'  #

        # required files
    #times_file = 'output_times_90days_every3days.txt'
    times_file = 'output_times_5days.txt'  
        
    data_file = 'hgs_refined_HH_60obs_1year.dat'
    #data_file = 'refined_depth2gwt_1year.dat'
    #data_file = 'refined_obs_WC_8elem_1year.dat'
    #data_file = 'refined_obs_WC_190elem_1year.dat'
    
    obs_nodes_file = 'refined_HH_60obswellsnodesfl.npy'
    #obs_nodes_file = 'refined_obswellsnodesfl.npy'
    
    obs_loc_file = 'refined_HH_60obswellscoordfl.dat'
    #obs_loc_file = 'refined_WC_190obswellscoordfl.dat'
    #obs_loc_file = 'refined_obswellscoordfl.dat'
    
    #obs_loc_gwt_idx_file = None
    obs_loc_gwt_idx_file = 'refined_obswells_idx_gwt.dat'
    
    #obs_element_file = 'refined_obswells190elementsfl.npy'
    obs_element_file = 'refined_obswellselementsfl.npy'

    states_file_id =  ['.head_pm.']         # ['.iconc_pm.', '.conc_pm.']              #
    states_t_end_file_id = ['hen']            # '.hen

        # grid settings
    mesh_file = 'mesh_refriver.dat'
    grid_settings_file = 'refined_grid_settings.dat'
    subgrid = False
    plot = False

        # filter settings
    nmem = 4 # number of ensemble members
    nupd_states = 0 # if 0 the model is updated by re-running all members
    exptr = True             # Exponential transformation when dealing with parameters?

    ga = False #
    da_method = 'enkf'
    beta = 1      # EnKF beta
    alpha = 1     # EnKF alpha
    damp = .6     # dampening factor, multiply directly to the kalman gain

    OL = True      # if True: Open loop, models will run for all timesteps without update for each member.
    spinup = True  # if True: Spinup for each member, for the specified time in instance template.

        # Pre Spin-up(the spin-up was performed already and the folders are provided for all memebers)
    pre_spinup = False        # If true only 1 of the two following varibales is applied 
    copy_all_insta = False    # All instances folders are provided
    copy_only_mprops = False  # Only the .mprops files are provided. 

    pre_spinup_dir = 'pre_spinup'
    para_frm_mprops = False # if true parameters 
    para_frm_mprops_dir = 'insta_mprops_files' # directory where the .mprops files are taken
    heads_frm_hen = False # initial heads are also taken from the pre spin-up directory
	
		# PROCESSORS
    parallel = False # to run the HGS models in parallel, using the multiprocessing package
    cpus = 4 # number of processesors

        # parameter settings
    kkk = False  # heterogeneous hydraulic conductivity 
    kkk_eff = False # effective homogeneous
    k_settings_file = 'modelito_kkkfields_settings.dat'
    gen_kkk = False # to generate the hydraulic conductivity field

    storativity = False 
    storativity_eff = False
    sss_settings_file = '_sssfields_settings.dat'
    gen_storativity = False

    porosity = False
    porosity_eff = False
    poro_settings_file = '_nnnfields_settings.dat'
    gen_porosity = False

    dispersivity = False
    disp_eff = False
    disp_settings_file = '_dddfields_settings.dat'
    gen_disp = False

    kzones = True # hydraulic conductivity zonified
    kzones_settings_file = 'modelito_kzones_fields_settings_desf.dat'
    
    alpha_vanG = False # alpha parameter from van genuchten by zones
    alpha_vanG_settings_file = 'modelito_alpha_vanG_settings_desf.dat'
    
    beta_vanG = False # beta parameter from van genuchten by zones
    beta_vanG_settings_file = 'modelito_beta_vanG_settings_desf.dat'
    
    # ======= Constrains  ==============================
    # apply manual contrains to parameters if conditions meet
    constrain_kzones = True
    if constrain_kzones==True:
        min_kzones=1e-10
        max_kzones=.1
    else:
        min_kzones=None
        max_kzones=None
    
    constrain_alphavg = False
    if constrain_alphavg==True:
        min_alphavg=.1
        max_alphavg=20
    else:
        min_alphavg=None
        max_alphavg=None
        
    constrain_betavg = False
    if constrain_betavg==True:
        min_betavg=1
        max_betavg=10
    else:
        min_betavg=None
        max_betavg=None    
    #===================================================

    nsoil_zones = 9  #number of soil zones, this must coincide with the zones of K,alpga and beta.

      # Observations data settings:
    perturb_data = True
    #obs_noise = np.sqrt(0.03)
    error_abs = 0.001  # Absolute error
    error_rel = .05  # *np.sqrt(36) #si es 0, es igual que antes

    
    process_data = False  # for pumping test measurements only.
    
        # Type of observations
    # ====  Only one of this must be True ====
    hyd_heads = True
    head2gwt = False
    wat_cont = False
    # ===================================
    
    depth2gwt = False # convert gwt height to depth from surface, (head2gwt must be True to use this)
    
    poro_tecplot_file = 'nnn_to_tecplot.pm.dat'  #tecplot format file of the porosity, this must coincide with the mesh file
    
    ini_head = 10 # initial head value for all, in case no initial data is given
    ini_head_from_file = 'initialhead.head_pm.7300.IC' 
    iniwatdepth = 1.0e-4   # initial water depth value for all, in case no initial data is given
    iniwatdepth_from_file = 'modelitoo.head_olf.'
    neg2zero = False # to avoid negative numbers
    head2dwdn = False #head to drawdown, for pumping test data only.
        
    cum = False #cumulative
    norm = False #normalize
    
    """ Verification """
    hg.settings_verification(kkk,kzones,alpha_vanG,beta_vanG,hyd_heads,head2gwt,wat_cont,copy_all_insta,copy_only_mprops) #verify settings
    
    """ Finish user settings section """
    hg.printSummary(nmem, kkk, ini_head, OL, iniwatdepth, spinup, parallel, cpus, subgrid, plot, model_name, test_name) # print settings summary


    da_dirs = dm.init_maindir(homedir, modelname=model_name, testname=test_name)  #create the directory dictionary
    
    if pre_spinup:
        da_dirs['pre_spinup_dir'] = os.path.join(da_dirs['modelname_dir'], test_name, pre_spinup_dir)  #add the pre spin-up directory to the dictiorary
        da_dirs['pre_spinup_mprops_dir'] =  os.path.join(da_dirs['pre_spinup_dir'],para_frm_mprops_dir)
    
    """  Other obs      ============================================================================"""
    # This are extra observations for monitoring only, not for the update.
    et_obsnodes_file = 'et_obsnodes.txt' #evapotranspiration nodes.
    et_obsnodes = np.loadtxt(os.path.join(da_dirs['model_data_dir'], et_obsnodes_file), dtype='int')
    et_sum = True # to sum all the values at the surface, for a total evapotranspiration.
    
    wc_obselem_file = 'wc_obselem.txt' # water content as extra observation
    wc_obselem = np.loadtxt(os.path.join(da_dirs['model_data_dir'], wc_obselem_file), dtype='int')  
    
    HH_obsnodes_file = 'HH_obsnodes.txt' #hydraulic heads
    HH_obsnodes = np.loadtxt(os.path.join(da_dirs['model_data_dir'], HH_obsnodes_file), dtype='int')  
    
        #add  extra observations dirs to the dictionary
    da_dirs['extra_obs_et_dir']    =  os.path.join(da_dirs['results_dir'],'extra_obs_et_dir')
    da_dirs['extra_obs_disch_dir'] =  os.path.join(da_dirs['results_dir'],'extra_obs_disch_dir')
    da_dirs['extra_obs_WC_dir']    =  os.path.join(da_dirs['results_dir'],'extra_obs_WC_dir')
    da_dirs['extra_obs_HH_dir']    =  os.path.join(da_dirs['results_dir'],'extra_obs_HH_dir')
    
    # create the folders for the extra observations of doesnt exist
    if not os.path.exists(da_dirs['extra_obs_et_dir']):    os.mkdir(da_dirs['extra_obs_et_dir'])
    if not os.path.exists(da_dirs['extra_obs_disch_dir']): os.mkdir(da_dirs['extra_obs_disch_dir'])
    if not os.path.exists(da_dirs['extra_obs_WC_dir']):    os.mkdir(da_dirs['extra_obs_WC_dir'])
    if not os.path.exists(da_dirs['extra_obs_HH_dir']):    os.mkdir(da_dirs['extra_obs_HH_dir'])

    """============================================================================================="""
    
    # Get full reference data:
    ref_data = np.loadtxt(os.path.join(da_dirs['model_data_dir'], data_file))
    ref_times, upd_bool = np.loadtxt(os.path.join(da_dirs['model_data_dir'], times_file), unpack=True, dtype='float,bool')
    ref_times_OL = np.copy(ref_times)
    ref_times = np.r_[ref_times[0], np.diff(ref_times)]
    obs_loc_id, obs_loc_x, obs_loc_y, obs_loc_z = \
        np.loadtxt(os.path.join(da_dirs['model_data_dir'], obs_loc_file), unpack=True, dtype='U25,float,float,float')
    obs_loc_nodes = np.load(os.path.join(da_dirs['model_data_dir'], obs_nodes_file), allow_pickle='TRUE').item()
    
    if wat_cont ==True:
        obs_loc_element = np.load(os.path.join(da_dirs['model_data_dir'], obs_element_file), allow_pickle='TRUE').item()
        obs_loc_nodes = obs_loc_element
    
    obs_loc_gwt_idx = np.loadtxt(os.path.join(da_dirs['model_data_dir'], obs_loc_gwt_idx_file), skiprows=1,usecols=(1,2), dtype=int)
    
    
    # Get observations used for update:
    #data2upd = ref_data[upd_bool, :]
    #time2upd = ref_times[upd_bool]

    # Time step controller:
    run_values, run_starts, run_lengths, true_values = kf.find_runs(upd_bool)
    nupd_stp = np.sum(run_lengths[run_values])

    # Dealing with the grid:
    # If subgrid is True, read original grid, and differentiate elements and nodes from bpth areas:
    xlen, ylen, zlen, dx, dy, dz, xlim, ylim, zlim = fg.readGridSettings(os.path.join(da_dirs['model_data_dir'], grid_settings_file))
    nxel, nyel, nzel = (xlim[1] - xlim[0]) / dx, (ylim[1] - ylim[0]) / dy, (zlim[1] - zlim[0]) / dz

    #nnodes, nelem = gm.readmesh(os.path.join(da_dirs['model_data_dir'], mesh_file), fulloutput=False)

    nnodes, nelem, nodes_coord, element_nodes = gm.readmesh(os.path.join(da_dirs['model_data_dir'], mesh_file), fulloutput=True, gridformat='tp2018')
    #poro_tecplot = hg.read_poro_tecplot(os.path.join(da_dirs['model_data_dir'],poro_tecplot_file),nodes_coord)
    
    #if wat_cont==True:
    tecplot_dict = hg.tecplot_reader(os.path.join(da_dirs['model_data_dir'],poro_tecplot_file), separator_key='#')
    poro_data_cc = tecplot_dict['var5_por(cell-centered)']
    #poro_data = hg.cell_centered2nodes(poro_data_cc, nodes_coord)
    poro_data = poro_data_cc
        #elevations_obj = gwt.create_elevations_class(da_dirs['model_data_dir'], mesh_file)()
    #else:
    #poro_data=None

    if subgrid is True:
        if not os.path.isfile(os.path.join(da_dirs['model_data_dir'], 'subGrid.elements.npy')):
            gm.sel_grid(os.path.join(da_dirs['model_data_dir'], mesh_file), xlim, ylim, zlim, store=True, return_val=False)
    """
    postX, nupd_param_k, nupd_param_s, nupd_param_alpha_vanG, nupd_param_beta_vanG = hg.deal_w_parameters(
                                                            kkk, kkk_eff, gen_kkk, k_settings_file,
                                                             storativity, storativity_eff, gen_storativity, sss_settings_file,
                                                             alpha_vanG, alpha_vanG_settings_file,
                                                             beta_vanG, beta_vanG_settings_file,
                                                             nxel, nyel, nzel, da_dirs, parallel, nmem, cpus, xlim, ylim, zlim, exptr)
    """
    
    postX, nupd_param_kzones, nupd_param_alpha_vanG, nupd_param_beta_vanG = hg.deal_w_parameters_homo_by_zones(da_dirs,para_frm_mprops,
                                                                                        kzones, kzones_settings_file,
                                                                                        alpha_vanG, alpha_vanG_settings_file,
                                                                                        beta_vanG, beta_vanG_settings_file,
                                                                                        nmem, nsoil_zones
                                                                                        )
    nupd_param_s = 0
    nupd_param_k = 0
    # Initialize and generate state/parameter matrices:
    priorX = np.empty(postX.shape)

    # Perturbing all measurements:
    if perturb_data:
        flat_data = ref_data.flatten('F')
        #meas_std = np.ones(flat_data.shape) * obs_noise
        meas_std = np.abs((flat_data * error_rel)) + error_abs
        refDataObj = dc.DataClass(flat_data, nmem)
        cdd_all = refDataObj.get_cdd(meas_std)  # This is R in my original code
        pert_all = refDataObj.pert_chol(cdd_all).reshape(ref_data.shape + (nmem,), order='F').squeeze()
    else:
        pert_all = np.zeros(ref_data.shape + (nmem,)).squeeze()

    np.save(os.path.join(da_dirs['obs_dir'], 'data_perturbations_%s' % test_name), pert_all)

    model2copy = da_dirs['source_inst_dir']
    grok_string = dm.getdirs(model2copy, mystr='.grok', fullpath=False)[0].replace('.grok','')

    if spinup:  # Run open loop if True:
        print('\n ================\n SPINUP STARTS \n ================')
        t1_start = time.time()
        files2keep = ['.grok', '.pfx', 'kkk', '.mprops', 'letme', 'wprops', 'oprops', 'etprops', 'default', 'control', 'asc', 'rasters', 'parallelindx', '.png', '.lic', 'gms', '.IC']
        dm.rmfiles(files2keep, da_dirs['inst_dir'])
        hg.gen_all_hgs_instance(da_dirs['source_inst_dir'], da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel) #Este genera las carpetas de las instancias
        #hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-Kini', n_inst=nmem, n_cpu=cpus, parallel=parallel)
        #hg.upd_all_mprops_inst(da_dirs['inst_dir'],  n_inst=nmem, n_cpu=cpus, parallel=parallel, kzones=kzones)
        #hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, alpha_vanG=alpha_vanG)
        #hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, beta_vanG= beta_vanG)
        
        #hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-watdepthini', n_inst=nmem, n_cpu=cpus, parallel=parallel, iniwatdepth=iniwatdepth)
        #hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-headini', n_inst=nmem, n_cpu=cpus, parallel=parallel, inihead=ini_head)
        
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-headini', n_inst=nmem, n_cpu=cpus, parallel=parallel, inihead=ini_head_from_file)
        
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-meshini_deact', n_inst=nmem, n_cpu=cpus, parallel=parallel)

        #hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-transientini_deact', n_inst=nmem, n_cpu=cpus, parallel=parallel)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-skipini', n_inst=nmem, n_cpu=cpus, parallel=parallel)

        # todo: Deal with writing parameters: Important if additional parameters are also updated
        hg.wr_parameters(kkk, kzones, porosity, storativity, dispersivity, alpha_vanG, beta_vanG, da_dirs, postX, nmem, cpus, parallel, exptr, subgrid,
                      nelem, nupd_param_k, nupd_param_kzones, nupd_param_s, nupd_param_alpha_vanG, nupd_param_beta_vanG, storativity_eff, kkk_eff)
       
        
        hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, kzones=kzones)
        hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, alpha_vanG=alpha_vanG)
        hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, beta_vanG= beta_vanG)
              
        
        fw.run_all_hgs(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, hsplot=False)
        #IC_simdata = hg.read_all_states_obs(da_dirs['inst_dir'], obs_loc_id, obs_loc_nodes, nnodes, neg2zero=neg2zero, ts=1, file_id=states_t_end_file_id, n_inst=nmem, n_cpu=cpus, parallel=parallel)
        IC_simdata = hg.read_all_states_obs(da_dirs['inst_dir'], obs_loc_id, obs_loc_nodes, nnodes, nodes_coord, element_nodes, hyd_heads=hyd_heads,
                                            head2gwt=head2gwt, obs_loc_gwt_idx=obs_loc_gwt_idx, depth2gwt=depth2gwt,
                                            wat_cont=wat_cont, poro_data=poro_data,
                                            neg2zero=neg2zero, ts=1,
                                            file_id=states_t_end_file_id,
                                            n_inst=nmem, n_cpu=cpus, parallel=parallel)
        
        np.save(os.path.join(da_dirs['obs_dir'], '%s_all.obs.IC.npy' % test_name), IC_simdata)

        hg.collect_all_ic(da_dirs['inst_dir'], da_dirs['ic_dir'], ext=states_t_end_file_id[0], n_inst=nmem, n_cpu=cpus, parallel=False)
        
        files2keep = ['.grok', '.pfx', 'kkk', '.mprops', 'letme', 'wprops', 'oprops', 'etprops', 'default', 'control', 'asc', 'rasters', 'parallelindx', '.png', '.lic', 'gms']
        dm.rmfiles(files2keep, da_dirs['inst_dir'])

        t2_stop = time.time()
        print("Time for Spinup:\n", t2_stop - t1_start)

    if OL:  # Run open loop if True:
        print('\nOL STARTS')
        t1_start = time.time()
        #hg.save_other_obsdata('et',da_dirs,0,et_obsnodes,OL=OL,upd=False)
        if not spinup:
            hg.gen_all_hgs_instance(model2copy, da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel)
            files2keep = ['.grok', '.pfx', 'kkk', '.mprops', 'letme', 'wprops', 'oprops', 'etprops', 'default', 'control', 'asc', 'rasters', 'parallelindx', '.png', '.lic', 'gms']
            dm.rmfiles(files2keep, da_dirs['inst_dir'])
            #hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-Kini', n_inst=nmem, n_cpu=cpus, parallel=parallel)
            hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-meshini_deact', n_inst=nmem, n_cpu=cpus, parallel=parallel)
            
            hg.wr_parameters(kkk, kzones, porosity, storativity, dispersivity, alpha_vanG, beta_vanG, da_dirs, postX, nmem, cpus, parallel, exptr, subgrid,
                             nelem, nupd_param_k, nupd_param_kzones, nupd_param_s, nupd_param_alpha_vanG, nupd_param_beta_vanG, storativity_eff, kkk_eff)
            
            hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, kzones=kzones)
            hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, alpha_vanG=alpha_vanG)
            hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, beta_vanG=beta_vanG)
        
        if pre_spinup==True:
            
            print('\ninitial conditions .hen files  were copied')
            shutil.copytree(os.path.join(da_dirs['pre_spinup_dir'],'hen_files'),
                            da_dirs['ic_dir'],dirs_exist_ok=True) 
         
        hg.cp_all_ic2inst(da_dirs['inst_dir'], da_dirs['ic_dir'], ext=states_t_end_file_id[0], file_id=grok_string, n_inst=nmem, n_cpu=cpus, parallel=parallel)
        
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-skipini_deact', n_inst=nmem, n_cpu=cpus, parallel=parallel)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-watdepthini_deact', n_inst=nmem, n_cpu=cpus, parallel=parallel, iniwatdepth=iniwatdepth)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-timestartini', n_inst=nmem, n_cpu=cpus, parallel=parallel, simtimes=ref_times_OL,curr_time_id=0)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-timesoutini', n_inst=nmem, n_cpu=cpus, parallel=parallel, simtimes=ref_times_OL,curr_time_id=0)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-headini_rfic', n_inst=nmem, n_cpu=cpus, parallel=parallel,inihead=grok_string)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-transientini', n_inst=nmem, n_cpu=cpus, parallel=parallel)

        #hg.cp_all_ic2inst(da_dirs['inst_dir'], da_dirs['ic_dir'], ext=states_t_end_file_id[0], file_id=grok_string, n_inst=nmem, n_cpu=cpus, parallel=parallel)

        fw.run_all_hgs(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, hsplot=False)
        #
        OL_simdata = hg.read_all_states_obs(da_dirs['inst_dir'], obs_loc_id, obs_loc_nodes, nnodes, nodes_coord, element_nodes,
                                            hyd_heads=hyd_heads,
                                            head2gwt=head2gwt, obs_loc_gwt_idx=obs_loc_gwt_idx,
                                            depth2gwt=depth2gwt,
                                            wat_cont=wat_cont, poro_data=poro_data, 
                                            neg2zero=neg2zero,
                                            ts=len(ref_times_OL),
                                            file_id=states_file_id,
                                            n_inst=nmem,
                                            n_cpu=cpus,
                                            parallel=parallel)
        if process_data:
            OL_simdata_tr = np.empty(OL_simdata.shape)
            for cur_obs in range(0, len(obs_loc_id)):
                OL_simdata_tr[:, cur_obs, :] = hg.transf_state_obs(OL_simdata[:, cur_obs, :], ini_head=ini_head, neg2zero=neg2zero, head2dwdn=head2dwdn, cum=cum, norm=norm, OL=True)
            OL_simdata = OL_simdata_tr

        hg.save_other_obsdata('et',   da_dirs, nnodes, test_name,0,  et_obsnodes=et_obsnodes, et_sum=et_sum, OL=OL, upd=False)
        hg.save_other_obsdata('disch',da_dirs, nnodes, test_name,0,                           OL=OL, upd=False)
        hg.save_other_obsdata('WC',   da_dirs, nnodes, test_name,0,  wc_obselem=wc_obselem, poro_data=poro_data, element_nodes=element_nodes, OL=OL, upd=False)
        hg.save_other_obsdata('HH',   da_dirs, nnodes, test_name,0,  HH_obsnodes=HH_obsnodes, OL=OL, upd=False)
        
        np.save(os.path.join(da_dirs['obs_dir'], '%s_all.obs.OL.npy' % test_name), OL_simdata)
        
        files2keep = ['.grok', '.pfx', 'kkk', '.mprops', 'letme', 'wprops',  'oprops', 'etprops', 'default', 'control', 'asc', 'rasters', 'parallelindx', '.png', '.lic','gms']
        dm.rmfiles(files2keep, da_dirs['inst_dir'])
        t2_stop = time.time()
        print("Time for OL:\n", t2_stop - t1_start)

    # Define necessary counters:
    counter_updSteps = 0
    counter_simsteps = 0
    updating = True
    t2_stop = time.time()
    print("Initialize the filter")
    for cur_run_value in range(0, len(ref_times)):
        counter_simsteps += 1

        # prepare data for current time step:
        tOut = ref_times[cur_run_value: cur_run_value + 1]
        real_tOut = ref_times_OL[cur_run_value: cur_run_value + 1]
        sel_data = np.asarray(ref_data[cur_run_value])
        # How to select
        if pert_all.ndim == 2:
            sel_pert = pert_all[np.newaxis, cur_run_value, :]
        elif pert_all.ndim == 3:
            sel_pert = pert_all[cur_run_value, :, :]
        # Get cdd (or R) for the current data:
        #meas_std = np.ones((len(sel_data))) * obs_noise
        meas_std = np.abs((flat_data * error_rel)) + error_abs
        refDataObj = dc.DataClass(sel_data, nmem)
        cdd = refDataObj.get_cdd(meas_std)
        # Store the cdd
        pickle.dump(cdd, open(os.path.join(da_dirs['cdd_dir'], '%s.cdd.%.3d.npy' % (test_name, counter_simsteps)), "wb"))
        # Initialize required variables:
        res_o = np.empty((len(sel_data), nmem))
        llkhd_o = np.empty((nmem))
        mod_data = np.empty((len(sel_data), nmem))
        mod_data_new = np.empty((len(sel_data), nmem))
        res_new = np.empty((len(sel_data), nmem))
        llkhd_new = np.empty((nmem))

        # Initial forward run :
        if (not OL) and (not spinup) and (cur_run_value == 0):
            
                if pre_spinup==True:
                    
                    if copy_all_insta==False:
                        print('\nnew hgs instances folders are being generated')
                        hg.gen_all_hgs_instance(model2copy, da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel)
                        
                        if copy_only_mprops==True:
                            print('\nonly .mprops files are being copied')
                            hg.copy_mprops(os.path.join(da_dirs['pre_spinup_dir'],'insta_mprops_files'),da_dirs['inst_dir'])                
                    
                    if copy_all_insta==True:
                        print('\ninstances were copied form given directory')
                        shutil.copytree(os.path.join(da_dirs['pre_spinup_dir'],'insta'),
                                        da_dirs['inst_dir'],dirs_exist_ok=True) 
                    
                    print('\ninitial conditions .hen files  were copied')
                    shutil.copytree(os.path.join(da_dirs['pre_spinup_dir'],'hen_files'),
                                    da_dirs['ic_dir'],dirs_exist_ok=True)       
                                     
                else:
                    print('\nnew hgs instances are being generated')
                    hg.gen_all_hgs_instance(model2copy, da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel)
                
                #hg.gen_all_hgs_instance(model2copy, da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel)
                hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-meshini_deact', n_inst=nmem, n_cpu=cpus, parallel=parallel)

                    

        hg.wr_parameters(kkk, kzones, porosity, storativity, dispersivity, alpha_vanG, beta_vanG, da_dirs, postX, nmem, cpus, parallel, exptr, subgrid,
                         nelem, nupd_param_k, nupd_param_kzones, nupd_param_s, nupd_param_alpha_vanG, nupd_param_beta_vanG, storativity_eff, kkk_eff)
        

        hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, kzones=kzones)
        hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, alpha_vanG= alpha_vanG)
        hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, beta_vanG=beta_vanG)

        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-watdepthini_deact', n_inst=nmem, n_cpu=cpus, parallel=parallel, iniwatdepth=iniwatdepth)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-timestartini', n_inst=nmem, n_cpu=cpus, parallel=parallel, simtimes=real_tOut, all_times=ref_times_OL, curr_time_id=cur_run_value)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-timesoutini', n_inst=nmem, n_cpu=cpus, parallel=parallel, simtimes=real_tOut)
        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-transientini', n_inst=nmem, n_cpu=cpus, parallel=parallel)
            
        if cur_run_value == 0: # Use spinup initial conditions if first update step
            hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-headini_rfic', n_inst=nmem, n_cpu=cpus, parallel=parallel, inihead=grok_string)
            
            #if pre_spinup:
            #    hg.cp_all_ic2inst(da_dirs['inst_dir'], da_dirs['ic_dir'], ext=ps_ext, file_id=grok_string, n_inst=nmem, n_cpu=cpus, parallel=parallel)
            #else:
            hg.cp_all_ic2inst(da_dirs['inst_dir'], da_dirs['ic_dir'], ext=states_t_end_file_id[0], file_id=grok_string, n_inst=nmem, n_cpu=cpus, parallel=parallel)
            
        
        else:
            hg.cp_all_states2inst(da_dirs['inst_dir'], da_dirs['states_dir'], tstep_id=counter_simsteps-1, updating=updating)
            hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-headini_rflast', n_inst=nmem, n_cpu=cpus, parallel=parallel,
                                 inihead=grok_string, tstep_id=counter_simsteps-1, updating=updating)
        
        start_time = time.time()
        fw.run_all_hgs(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, hsplot=False)
        print("--- time needed to run all ensembles %s seconds ---" % (time.time() - start_time))
        t = time.localtime()
        print(time.strftime("%Y-%m-%d %H:%M:%S", t))
        time.sleep(5)
        
        hg.cp_all_states(da_dirs['inst_dir'], da_dirs['states_dir'], file_id=states_t_end_file_id[0], tstep_id=counter_simsteps, upd_states=False, n_inst=nmem, n_cpu=cpus, parallel=parallel)


        # Get model output (observations):
        """
        # Get only value at nodes

        mod_data = hg.read_all_states_obs(da_dirs['inst_dir'], obs_loc_id, obs_loc_nodes, nnodes,
                                          neg2zero=neg2zero,
                                          ts=len(tOut),
                                          file_id=states_t_end_file_id[0],
                                          n_inst=nmem,
                                          n_cpu=cpus,
                                          parallel=parallel)
        """

        # Get all data to calculate gwt

        mod_data = hg.read_all_states_obs(da_dirs['inst_dir'], obs_loc_id, obs_loc_nodes, nnodes, nodes_coord, element_nodes,
                                          hyd_heads=hyd_heads,
                                          head2gwt=head2gwt, obs_loc_gwt_idx=obs_loc_gwt_idx, depth2gwt=depth2gwt,
                                          wat_cont=wat_cont, poro_data=poro_data,
                                          neg2zero=neg2zero,
                                          ts=len(tOut),
                                          file_id=states_t_end_file_id[0],
                                          n_inst=nmem,
                                          n_cpu=cpus,
                                          parallel=parallel)        
        

        """        
        if process_data_gwt:
            mod_data_pr = np.empty(mod_data.shape)
            for cur_obs in range(0, len(obs_loc_id)):
                mod_data_pr[:, cur_obs, :] = hg.transf_state_obs(mod_data[:, cur_obs, :], process_data_gwt=process_data_gwt)
        """   
        
        if process_data:
            mod_data_pr = np.empty(mod_data.shape)
            for cur_obs in range(0, len(obs_loc_id)):
                mod_data_pr[:, cur_obs, :] = hg.transf_state_obs(mod_data[:, cur_obs, :], ini_head=ini_head,
                                                                   neg2zero=neg2zero, head2dwdn=head2dwdn,
                                                                   cum=cum, norm=norm, OL=False)     
            mod_data = mod_data_pr
            
        if (mod_data.ndim == 3) and (mod_data.shape[0] == 1):
            mod_data = mod_data.squeeze()
            
        hg.save_other_obsdata('et',    da_dirs, nnodes, test_name, counter_simsteps, et_obsnodes=et_obsnodes, et_sum=et_sum, upd=False)  #???
        hg.save_other_obsdata('disch', da_dirs, nnodes, test_name, counter_simsteps, upd=False)  #???
        hg.save_other_obsdata('WC',    da_dirs, nnodes, test_name, counter_simsteps, wc_obselem=wc_obselem, poro_data=poro_data, element_nodes=element_nodes, upd=False)  #???
        hg.save_other_obsdata('HH',    da_dirs, nnodes, test_name, counter_simsteps, HH_obsnodes=HH_obsnodes, upd=False)  #???

        files2keep = ['.grok', '.pfx', 'kkk', '.mprops', 'letme', 'wprops', 'parallelindx', 'oprops', 'etprops', 'default', 'control', 'asc', 'rasters','gms']
        dm.rmfiles(files2keep, da_dirs['inst_dir'])

        # Get current model parameters:
        priorX = np.copy(postX)
        del postX

        for cur_mem in range(0, nmem):
            res_o[:, cur_mem], llkhd_o[cur_mem] = refDataObj.likelihood(sel_data + sel_pert[:, cur_mem], mod_data[:, cur_mem], cdd)
        np.save(os.path.join(da_dirs['obs_dir'], '%s_all.obs.%.3d.npy' % (test_name,counter_simsteps)), mod_data)
        np.savetxt(os.path.join(da_dirs['obs_dir'], '%s_all.res.%.3d.txt' % (test_name,counter_simsteps)), res_o,header="residuals (data-simdata). ts=%5.4e" % counter_simsteps)
        np.savetxt(os.path.join(da_dirs['obs_dir'], '%s_all.lkl.%.3d.txt' % (test_name,counter_simsteps)), llkhd_o, header="likelihood. ts=%5.4e" % counter_simsteps)

        if ga is True:
            ga_fctns = []
            mod_data_ga = np.empty(mod_data.shape)
            sel_data_ga = np.empty((sel_data.shape[0],))

            for ii in range(0, sel_data.shape[0], 1):
                mod_data_ga[ii, :], cur_fctn = mysst.GausAnam(mod_data[ii, :] + sel_pert[ii,:], '', myplot=False, oneDstats=False)
                ga_fctns.append(cur_fctn)
                sel_data_ga[ii] = cur_fctn(sel_data[ii])
            np.save(os.path.join(da_dirs['ga_dir'], 'sel_data_ga_%.3d.npy' % counter_simsteps), sel_data_ga)
            np.save(os.path.join(da_dirs['ga_dir'], 'mod_data_ga_%.3d.npy' % counter_simsteps), mod_data_ga)
            np.save(os.path.join(da_dirs['ga_dir'], 'ga_functions_%.3d.npy' % counter_simsteps), ga_fctns)
            mod_data = mod_data_ga

        print('\nStop pushing ensemble forward @ simulation time step %s (timespan=%s) (total time=%s)' % (counter_simsteps, tOut[-1],real_tOut[-1]))
        np.save(os.path.join(da_dirs['param_dir'], 'X_prior_%.3d.npy' % counter_simsteps), priorX)
        pptr.plotlikelihood(llkhd_o, res_o, cdd, fignum='%sa' % counter_simsteps,
                            savefile=os.path.join(da_dirs['plot_dir'], '%s_likelihood.%.3d' % (test_name, counter_simsteps)))

        if upd_bool[cur_run_value] == True:
            counter_updSteps += 1
            updating = True
            print('###############################################')
            print('Initializing update step %s (total update steps %s)...' % (counter_updSteps, np.sum(upd_bool)))
            print('###############################################')

            updObj = kf.UpdateClass(priorX)
            cyy = updObj.get_cyy(mod_data)
            cxy = updObj.get_cxy(mod_data)
            kgain = updObj.kgain(cxy, cyy, cdd, beta=beta, alpha=alpha)

            if ga == False:
                postX, d_minus_y1, upd1 = updObj.upd_eq(kgain, sel_data, mod_data, sel_pert, damp=damp)
            if ga == True:
                postX, d_minus_y1, upd1 = updObj.upd_eq(kgain, sel_data_ga, mod_data, sel_pert*0, damp=damp)
           
            #???
            if constrain_kzones==True:
                postX = hg.verify_constrain_kzones(postX,nsoil_zones,min_kzones=min_kzones,max_kzones=max_kzones)
            if constrain_alphavg==True:
                postX = hg.verify_constrain_alphavg(postX,nsoil_zones,min_alphavg=min_alphavg,max_alphavg=max_alphavg)   
            if constrain_betavg==True:
                postX = hg.verify_constrain_betavg(postX,nsoil_zones,min_betavg=min_betavg,max_betavg=max_betavg) 

                
            postX_exp = np.exp(postX)
            np.save(os.path.join(da_dirs['param_dir'], 'X_post_%.3d.npy' % counter_simsteps), postX)

            # If upd_states is False, re-run entire ensemble to get updated states
            if not nupd_states:
                print('Updating model states by re-running ensemble with updated parameters (update step %s, from %s)' % ((counter_updSteps), np.sum(upd_bool)))
                # Re-define initial states: (model dependent?)
                hg.wr_parameters(kkk, kzones, porosity, storativity, dispersivity, alpha_vanG, beta_vanG, da_dirs, postX, nmem, cpus, parallel, exptr, subgrid,
                                 nelem, nupd_param_k, nupd_param_kzones, nupd_param_s, nupd_param_alpha_vanG, nupd_param_beta_vanG, storativity_eff, kkk_eff)
                hg.upd_all_mprops_inst(da_dirs['inst_dir'],  n_inst=nmem, n_cpu=cpus, parallel=parallel, kzones=kzones)
                hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, alpha_vanG=alpha_vanG)
                hg.upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, beta_vanG= beta_vanG)
        
                if cur_run_value == 0:  # Use spinup initial conditions
                    hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-headini_rfic', n_inst=nmem, n_cpu=cpus, parallel=parallel,
                                         inihead=grok_string)
                    hg.cp_all_ic2inst(da_dirs['inst_dir'], da_dirs['ic_dir'], ext=states_t_end_file_id[0], file_id=grok_string, n_inst=nmem,
                                      n_cpu=cpus, parallel=parallel)
                else:
                    if upd_bool[cur_run_value-1] == False:
                        hg.cp_all_states2inst(da_dirs['inst_dir'], da_dirs['states_dir'], tstep_id=counter_simsteps,
                                              updating=False)
                        hg.upd_all_grok_inst(da_dirs['inst_dir'], startstr='!-headini_rflast', n_inst=nmem, n_cpu=cpus, parallel=parallel,
                                             inihead=grok_string, tstep_id=counter_simsteps, updating=False)
                    else:
                        hg.cp_all_states2inst(da_dirs['inst_dir'], da_dirs['states_dir'], tstep_id= counter_simsteps-1, updating=updating)
                
                start_time = time.time()

                fw.run_all_hgs(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, hsplot=False)
                print("--- time needed to run all ensembles %s seconds ---" % (time.time() - start_time))
                time.sleep(5)
                
                hg.cp_all_states(da_dirs['inst_dir'], da_dirs['states_dir'], file_id=states_t_end_file_id[0], tstep_id=counter_simsteps, upd_states=True,
                                 n_inst=nmem, n_cpu=cpus, parallel=parallel)
                """
                mod_data_new = hg.read_all_states_obs(da_dirs['inst_dir'], obs_loc_id, obs_loc_nodes, nnodes,
                                                      neg2zero=neg2zero,
                                                      ts=len(tOut),
                                                      file_id=states_t_end_file_id[0],
                                                      n_inst=nmem,
                                                      n_cpu=cpus,
                                                      parallel=parallel)
                """
                
                mod_data_new = hg.read_all_states_obs(da_dirs['inst_dir'], obs_loc_id, obs_loc_nodes, nnodes,nodes_coord, element_nodes,
                                                      hyd_heads=hyd_heads,
                                                      head2gwt=head2gwt, obs_loc_gwt_idx=obs_loc_gwt_idx,depth2gwt=depth2gwt,
                                                      wat_cont=wat_cont, poro_data=poro_data,
                                                      neg2zero=neg2zero,
                                                      ts=len(tOut),
                                                      file_id=states_t_end_file_id[0],
                                                      n_inst=nmem,
                                                      n_cpu=cpus,
                                                      parallel=parallel)
                
                

                
                if process_data:
                    mod_data_pr_new = np.empty(mod_data_new.shape)
                    for cur_obs in range(0, len(obs_loc_id)):
                        mod_data_pr_new[:, cur_obs, :] = hg.transf_state_obs(mod_data_new[:, cur_obs, :], ini_head=ini_head,
                                                                         neg2zero=neg2zero, head2dwdn=head2dwdn, cum=cum, norm=norm,
                                                                         OL=False)
                    mod_data_new = mod_data_pr_new

                if (mod_data_new.ndim == 3) and (mod_data_new.shape[0] == 1):
                    mod_data_new = mod_data_new.squeeze()

                hg.save_other_obsdata('et',    da_dirs, nnodes, test_name, counter_simsteps, et_obsnodes=et_obsnodes, et_sum=et_sum, upd=True)  #???
                hg.save_other_obsdata('disch', da_dirs, nnodes, test_name, counter_simsteps, upd=True)  #???
                hg.save_other_obsdata('WC',    da_dirs, nnodes, test_name, counter_simsteps, wc_obselem=wc_obselem, poro_data=poro_data, element_nodes=element_nodes, upd=True)  #???
                hg.save_other_obsdata('HH',    da_dirs, nnodes, test_name, counter_simsteps, HH_obsnodes=HH_obsnodes, upd=True)  #???
                
                files2keep = ['.grok', '.pfx', 'kkk', '.mprops', 'letme', 'wprops', 'parallelindx', 'oprops', 'etprops', 'default', 'control', 'asc', 'rasters','gms']
                dm.rmfiles(files2keep, da_dirs['inst_dir'])
                for cur_mem in range(0, nmem):
                    res_new[:, cur_mem], llkhd_new[cur_mem] = refDataObj.likelihood(sel_data + sel_pert[:, cur_mem],
                                                                                    mod_data_new[:, cur_mem], cdd)

                accepted = refDataObj.acc_rej_chi2(llkhd_o, refDataObj.ndata)

    

                np.save(os.path.join(da_dirs['obs_dir'], '%s_all.obs.%.3d.upd.npy' % (test_name, counter_simsteps)), mod_data_new)
                np.savetxt(os.path.join(da_dirs['obs_dir'], '%s_all.res.%.3d.upd.txt' % (test_name, counter_simsteps)), res_new,
                           header="residuals (data-simdata). ts=%5.4e" % counter_simsteps)
                np.savetxt(os.path.join(da_dirs['obs_dir'], '%s_all.lkl.%.3d.upd.txt' % (test_name, counter_simsteps)), llkhd_new,
                           header="likelihood. ts=%5.4e" % counter_simsteps)

        if upd_bool[cur_run_value] == False:
            updating = False
            postX = priorX
            mod_data_new = np.empty(mod_data_new.shape); mod_data_new[:] = np.NaN
            res_new = np.empty(res_new.shape); res_new[:] = np.NaN
            llkhd_new = np.empty(llkhd_new.shape); llkhd_new[:] = np.NaN

            if et_sum==True: et_data = np.empty([nmem,1]) * np.NaN 
            if et_sum==False: et_data = np.empty([len(et_obsnodes),nmem]) * np.NaN
            wc_data = np.empty([len(wc_obselem),nmem]) * np.NaN
            HH_data = np.empty([len(HH_obsnodes),nmem]) * np.NaN
            #disch_data = np.empty([nmem,2]); disch_data[:] = np.NaN
            
            
            disch_data_dic={}
            for i in range(nmem):  disch_data_dic['insta'+str(i)] = np.empty([1,2])*np.NaN 
            

            np.save(os.path.join(da_dirs['param_dir'], 'X_post_%.3d.noassim.npy' % counter_simsteps), postX)
            np.save(os.path.join(da_dirs['obs_dir'], '%s_all.obs.%.3d.upd.noassim.npy' % (test_name, counter_simsteps)), mod_data_new)
            np.savetxt(os.path.join(da_dirs['obs_dir'], '%s_all.res.%.3d.upd.noassim.txt' % (test_name, counter_simsteps)), res_new,
                       header="residuals (data-simdata). ts=%5.4e" % counter_simsteps)
            np.savetxt(os.path.join(da_dirs['obs_dir'], '%s_all.lkl.%.3d.upd.noassim.txt' % (test_name, counter_simsteps)), llkhd_new,
                       header="likelihood. ts=%5.4e" % counter_simsteps)
           
            np.savetxt(os.path.join(da_dirs['extra_obs_et_dir'], '%s_all.et_obs.%.3d.upd.noassim.txt' % (test_name, counter_simsteps)), et_data,
                       header="et_obs. ts=%5.4e" % counter_simsteps)
            np.savetxt(os.path.join(da_dirs['extra_obs_WC_dir'], '%s_all.wc_obs.%.3d.upd.noassim.txt' % (test_name, counter_simsteps)), wc_data,
                       header="wc_obs. ts=%5.4e" % counter_simsteps)
            np.savetxt(os.path.join(da_dirs['extra_obs_HH_dir'], '%s_all.HH_obs.%.3d.upd.noassim.txt' % (test_name, counter_simsteps)), HH_data,
                       header="HH_obs. ts=%5.4e" % counter_simsteps)
            
            np.save(os.path.join(da_dirs['extra_obs_disch_dir'], '%s_all.disch_obs.%.3d.upd.noassim.npy' % (test_name, counter_simsteps)), disch_data_dic)



        print('%%%%%%%%%%%%%%%%%%%%%%%%%')
        print('Simulated time steps: %s ' % counter_simsteps)
        print('%%%%%%%%%%%%%%%%%%%%%%%%%')
    assert nupd_stp == counter_updSteps, 'Oops something is messed up with the time stepping...'

    print('DA finished')

    print('Start Plotting')


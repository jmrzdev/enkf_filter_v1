
"""
Functions to get gw table
Created on 02/04/2021
@author: Emilio Sanchez-Leon
modified by: Jaime Martinez on 06/2021
"""
import sys
import os
import numpy as np
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'enkf_utilities')))

import enkf_utilities.gridmanip as gm




def get_elevations(nodes_coord):
    """ get the elevatation values from the nodes coordinates from the mesh file
    Arguments:
        nodes_coord: the nodes coordinates array obtained from the mesh file
    Returns:
    x,y,z coordinates and evelation values
    """
    print ('=============  READ data HGSbin  =============')
    x = np.unique(nodes_coord[:,0]).shape[0]
    y = np.unique(nodes_coord[:,1]).shape[0]
    z = int(nodes_coord.shape[0]/(x*y))
    elevations = np.empty((x, y, z))
    xy = x * y
    for ii in range(elevations.shape[2]):

        elevations[:, :, ii] = np.reshape(nodes_coord[xy*ii:xy*(ii+1),-1],(x,y), order='F')
        
    return x,y,z,elevations



def heads2gwt(heads_1Darray, nodes_coord, depth2gwt):
    """
    Arguments:
        heads_1Darray: array with the hydraulic heads
        nodes_coord: the nodes coordinates array obtained from the mesh file
        depth2gwt: True or false 
    Returns:
    depth2gwt_array: deoth to the groundwater table mesured from the topo surface
    gwt_array: groundwater leve from the given reference
    """
    cur_press = heads_1Darray
    
    x,y,z,elevations = get_elevations(nodes_coord)
    
    pm_array = np.zeros((x,y,z))
    gwt_array = np.zeros((x,y))
    gwt_depth_array = np.zeros((x,y))
    #depth2gwt_array = np.zeros((x,y))
    
    cur_press_rshp = np.reshape(cur_press,(x,y,z), order='F')
    pressure =  cur_press_rshp - elevations

    for cur_x in range(pressure.shape[0]):
        for cur_y in range(pressure.shape[1]):
            cur_col = pressure[cur_x, cur_y, :]
            
                
            asign = np.sign(cur_col)
            signchange = ((np.roll(asign, 1) - asign) != 0).astype(int)
            signchange[0] = 0
            id_change = np.where(signchange == 1)[0]
            
            if all(cur_col>0):
                gwt_array[cur_x, cur_y] = elevations[cur_x, cur_y, -1]
            elif all(cur_col<0):
                gwt_array[cur_x, cur_y] = elevations[cur_x, cur_y, 0]

            else:
                id_change = id_change.squeeze()
                if id_change.size > 1:
                    id_change = id_change[0]
                xvals = [-.00001, 0, .00001]  # values where the interpolation takes place
                elev_interp = np.interp(xvals, [cur_col[id_change], cur_col[id_change-1]],
                                               [elevations[cur_x, cur_y, id_change], elevations[cur_x, cur_y, id_change-1]])
                gwt_array[cur_x, cur_y] = elev_interp[1]
                
                gwt_depth_array[cur_x, cur_y] = elevations[cur_x, cur_y, -1] - elev_interp[1] 
    
    if depth2gwt == False:
        return gwt_array

    if depth2gwt == True:
        depth2gwt_array = np.abs(gwt_array[:,:] - elevations[:,:,-1])
        return depth2gwt_array


"""
Functions to deal with GROK and HGS
Created on Wed Mar 25 20:39:31 2015
@author: Emilio Sanchez-Leon
"""
import sys
import os
import numpy as np
import datetime
import shutil
import subprocess as sp
import platform
import multiprocessing as mp
import time
import scipy
from itertools import repeat
from statistics import mean
import scipy.sparse as ssp
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'enkf_utilities')))
import enkf_utilities.fieldgen as fg
import enkf_utilities.dirs as dm
import enkf_utilities.gridmanip as gm
import enkf_utilities.mystats as mysst
import enkf_utilities.kalman as kf
import enkf_utilities.gwt as gwt
import matplotlib.pyplot as plt

def printime(message='Provide your message as a string'):
    """  Print date time message
    :param message:
    :return: printed message with current time/date
    """
    print(time.strftime("%d-%m %H:%M:%S",
                        time.localtime(time.time())) + ': ' + message)
    return

def gen_all_hgs_instance(model2copy, all_inst_dir, n_inst=1, n_cpu=1, parallel=False):
    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(gen_hgs_instance, zip(np.arange(0, n_inst, 1), repeat(model2copy), repeat(all_inst_dir)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0,n_inst):
            gen_hgs_instance(ii, model2copy, all_inst_dir)
    return

def gen_hgs_instance(inst_idx, model2copy, all_inst_dir):
    """

    :param model_dir:
    :param inst_dir:
    :param inst_idx:
    :return:
    """
    assert os.path.isdir(model2copy), 'Model source does not exist!'
    if not os.path.isdir(all_inst_dir):
        os.makedirs(all_inst_dir)

    inst_string = 'hgs_inst_%.5d' % inst_idx

    # First copy files:
    inst_folder = os.path.join(all_inst_dir, inst_string)

    grok_orig = dm.getdirs(model2copy, mystr='.grok', fullpath=False)[0]
    grok_inst = grok_orig.split('.grok')[0] + '_%s.grok' % inst_string

    if not os.path.isdir(inst_folder):
        shutil.copytree(model2copy, inst_folder, ignore=shutil.ignore_patterns('mesh*', '*.npy','kkk*'))
        os.rename(os.path.join(inst_folder, grok_orig), os.path.join(inst_folder, grok_inst))


    elif os.path.isdir(inst_folder):
        os.remove(os.path.join(inst_folder, grok_inst))
        shutil.copy2(os.path.join(model2copy, grok_orig), os.path.join(inst_folder, grok_inst))
    # Update files:
    CreatePrefixBatch(inst_folder, grok_inst.split('.grok')[0])
    Createletmerun(inst_folder, grok_inst.split('.grok')[0], hsplot=False)

    return


def upd_all_grok_inst(all_inst_dir, startstr='!-Kini', n_inst=1, n_cpu=1, parallel=False, simtimes=None, inihead=None, iniwatdepth=None, tstep_id=None, updating=True, all_times=None, curr_time_id=None):#(inst_idx, inst_dir, startstr='!-Kini'):

    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(upd_grok_inst, zip(np.arange(0, n_inst, 1), repeat(all_inst_dir), repeat(startstr), repeat(simtimes), repeat(inihead), repeat(iniwatdepth), repeat(tstep_id), repeat(updating), repeat(all_times), repeat(curr_time_id)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0,n_inst):
            upd_grok_inst(ii, all_inst_dir, startstr, simtimes, inihead, iniwatdepth, tstep_id, updating, all_times, curr_time_id)
    return


def upd_grok_inst(inst_idx, inst_dir, startstr, simtimes, inihead, iniwatdepth, tstep_id, updating, all_times, curr_time_id):
    # startstr='!-Kini', '!-timesini', '!-headini', '!-headini_rf', ''
    # simtimes, should be a list
    mprops = False
    inst_string = 'hgs_inst_%.5d' % inst_idx

    cur_grok_dir = os.path.join(inst_dir, inst_string)
    grokfile = dm.getdirs(cur_grok_dir, mystr='.grok', fullpath=True)[0]
    mpropsfile = dm.getdirs(cur_grok_dir, mystr='.mprops', fullpath=True)[0]

    if 'Kini' in startstr:
        endstr='!-Kend'
        str2add = "Read elemental k from file\n" \
                  "./kkk_%.5d.txt" % int(inst_idx)

    elif 'timesoutini' in startstr:
        endstr = '!-timesoutend'
        str2add = "output times\n"
        for jj in simtimes:
            str2add += "%i\n" % jj
        str2add += 'end'

    elif 'timestartini' in startstr:
        endstr = '!-timestartend'
        str2add = "initial time\n"
        if curr_time_id == 0:
            str2add += str(0)
            
        else:
            #str2add += str(all_times[curr_time_id])
            str2add += str(all_times[curr_time_id-1])
            #str2add += 'end'
        

    elif 'headini' in startstr:
        endstr = '!-headend'
        if '_rfic' in startstr:
            startstr = startstr.split('_')[0]
            #hen_str = os.path.split(grokfile)[-1].split('.')[0]
            hen_str = inihead + '_' + inst_string
            str2add = "restart file for heads\nic_%so.hen" % hen_str
        elif '_rflast' in startstr:
                startstr = startstr.strip('_rflast')
                # hen_str = os.path.split(grokfile)[-1].split('.')[0]
                hen_str = inihead + '_' + inst_string
                if updating == True:
                    step = '_ts%.3dupd' % tstep_id
                else:
                    step = '_ts%.3d' % tstep_id
                str2add = "restart file for heads\n%so.hen%s" % (hen_str, step)
        
        elif '.h3n' in inihead:
            str2add = "Restart file for heads\n%s" % inihead
        elif '.IC' in inihead:
            str2add = "initial head from output file\n%s" % inihead
        elif 'head_pm' in inihead:
            str2add = "initial head from output file\n%s" % inihead
        else:
            str2add = "Initial head\n%f" % inihead


    elif 'meshini' in startstr:
        endstr = '!-meshend'
        if '_deact' in startstr:
            startstr = startstr.strip('_deact')
            str2add = "!Mesh to tecplot\n!mesh_%.5d.dat" % inst_idx
        else:
            str2add = "Mesh to tecplot\nmesh_%.5d.dat" % inst_idx

    elif 'transientini' in startstr:
        endstr = '!-transientend'
        if '_deact' in startstr:
            startstr = startstr.strip('_deact')
            str2add = "!Transient flow"
        else:
            str2add = "Transient flow"
    elif 'skipini' in startstr:
        endstr = '!-skipend'
        if '_deact' in startstr:
            startstr = startstr.strip('_deact')
            str2add = "!skip on"
        else:
            str2add = "skip on"

    elif 'watdepthini' in startstr:
        endstr = '!-watdepthend'
        if '_deact' in startstr:
            startstr = startstr.strip('_deact')
            str2add = "!initial water depth\n!%f" % iniwatdepth
        else:
            str2add = "initial water depth\n%f" % iniwatdepth
            
            
    addstr2grok(grokfile, startstr=startstr, endstr=endstr, str2add=[str2add])

    return


def upd_all_mprops_inst(all_inst_dir, n_inst=1, n_cpu=1, parallel=False, kkk=None, sss=None, mpor=None, impor=None, trcf=None, ddd=None, exptr=False, alpha_vanG=None, beta_vanG=None,  kzones=None):
    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(upd_mprops_inst, zip(np.arange(0, n_inst, 1), repeat(all_inst_dir), repeat(kkk), repeat(sss), repeat(ddd), repeat(mpor), repeat(impor), repeat(trcf), repeat(exptr), repeat(alpha_vanG), repeat(beta_vanG), repeat(kzones)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0,n_inst):
            upd_mprops_inst(ii, all_inst_dir, kkk, sss, ddd, mpor, impor, trcf, exptr, alpha_vanG, beta_vanG, kzones)
    return


def upd_mprops_inst(inst_idx, inst_dir, kkk, sss, ddd, mpor, impor, trcf, exptr, alpha_vanG, beta_vanG, kzones):
    inst_string = 'hgs_inst_%.5d' % inst_idx

    cur_grok_dir = os.path.join(inst_dir, inst_string)
    mpropsfile = dm.getdirs(cur_grok_dir, mystr='.mprops', fullpath=True)[0]

    if kkk is not None:
        if exptr:
            kkk = np.exp(kkk)
        inistr = '!-inikkk'
        endstr = '!-endkkk'
        str2add = "k isotropic\n" \
                  "%5.4e" % kkk[0,inst_idx]
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])
    if sss is not None:
        if exptr:
            sss = np.exp(sss)
        inistr = '!-inistorage'
        endstr = '!-endstorage'
        str2add = "specific storage\n" \
                  "%5.4e" % sss[0,inst_idx]
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])
    if ddd is not None:
        if exptr:
            ddd = np.exp(ddd)
        inistr = '!-inildisp'
        endstr = '!-endldisp'
        str2add = "longitudinal dispersivity\n" \
                  "%5.4e" % ddd[0, inst_idx]
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])

        inistr = '!-initrdisp'
        endstr = '!-endtrdisp'
        str2add = "transverse dispersivity\n" \
                  "%5.4e" % (ddd[0, inst_idx] /10.)
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])

        inistr = '!-inivtrdisp'
        endstr = '!-endvtrdisp'
        str2add = "vertical transverse dispersivity\n" \
                  "%5.4e" % (ddd[0, inst_idx] /100.)
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])

    if mpor is not None:
        if exptr:
            mpor = np.exp(mpor)

        inistr = '!-inimobile'
        endstr = '!-endmobile'
        str2add = "porosity\n" \
                  "%5.4e" % mpor[0, inst_idx]
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])

    if impor is not None:
        if exptr:
            impor = np.exp(impor)
        inistr = '!-iniimmopor'
        endstr = '!-endimmopor'
        str2add = "immobile zone porosity\n" \
                  "%5.4e" % impor[0, inst_idx]
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])

    if trcf is not None:
        if exptr:
            trcf = np.exp(trcf)
        inistr = '!-inimasstransf'
        endstr = '!-endmasstransf'
        str2add = "immobile zone mass transfer coefficient\n" \
                  "%5.4e" % trcf[0, inst_idx]
        addstr2grok(mpropsfile, startstr=inistr, endstr=endstr, str2add=[str2add])


    if alpha_vanG is True:
        str2add = "alpha\n"
        alphas_vanG_file = dm.getdirs(cur_grok_dir, mystr='alpha', fullpath=True)[0]
        #alphas_vanG_file = cur_grok_dir+'alpha_'+ int(inst_idx)
        alphas_vanG = np.loadtxt(alphas_vanG_file)
        for i in range(len(alphas_vanG)):
            startstr = '!-alphaini'+str(i+1)
            endstr = '!-alphaend'+str(i+1)
            str2add = "alpha\n" \
                      "%5.4e" % alphas_vanG[i]
            addstr2grok(mpropsfile, startstr=startstr, endstr=endstr, str2add=[str2add])
            
    if beta_vanG is True:
        str2add = "beta\n"
        betas_vanG_file = dm.getdirs(cur_grok_dir, mystr='beta', fullpath=True)[0]
        betas_vanG = np.loadtxt(betas_vanG_file)
        for i in range(len(betas_vanG)):
            startstr = '!-betaini'+str(i+1)
            endstr = '!-betaend'+str(i+1)
            str2add = "beta\n" \
                      "%5.4e" % betas_vanG[i]
            addstr2grok(mpropsfile, startstr=startstr, endstr=endstr, str2add=[str2add])
            
    if kzones is True:

        kzones_file = dm.getdirs(cur_grok_dir, mystr='kzones', fullpath=True)[0]
        kzones = np.loadtxt(kzones_file)
        for i in range(len(kzones)):
            startstr = '!-Kzoneini'+str(i+1)
            endstr = '!-Kzoneend'+str(i+1)
            str2add = "k isotropic\n" \
                      "%5.4e" % kzones[i]
            addstr2grok(mpropsfile, startstr=startstr, endstr=endstr, str2add=[str2add])
            
            
            
    return

def deal_w_parameters_homo_by_zones(da_dirs, para_frm_mprops,
                                    kzones, kzones_settings_file,
                                    alpha_vanG, alpha_vanG_settings_file,
                                    beta_vanG, beta_vanG_settings_file,
                                    nmem,nsoil_zones):
								
    print('\n Dealing with parameters starts...')
    
    
    if para_frm_mprops == True:
        
        all_insta_dir = os.path.join(da_dirs['pre_spinup_mprops_dir'])
        
        if kzones==True:
            
            kzones_params = np.log(rd_all_insta_mprops(all_insta_dir,nsoil_zones,mystr = 'k isotropic'))
            
            
        if alpha_vanG==True:
            alpha_vanG_params = np.log(rd_all_insta_mprops(all_insta_dir,nsoil_zones,mystr = 'alphaini'))
            
            
        if beta_vanG==True:
                beta_vanG_params = np.log(rd_all_insta_mprops(all_insta_dir,nsoil_zones,mystr = 'betaini'))
                
                
    elif para_frm_mprops == False:
    
        #settings_data = getdata_from_settings_file(alpha_VanG_settings_file)
        
        if kzones==True:
            kzones_mu, kzones_sigma = np.loadtxt(os.path.join(da_dirs['model_data_dir'], kzones_settings_file), skiprows=1, unpack=True, usecols = (0,1))
            kzones_params = np.zeros([nsoil_zones,nmem])
            nupd_param_k = nsoil_zones
            for i in range(nsoil_zones):
        
                kzones_params[i,:] = np.random.normal(np.log(kzones_mu[i]), kzones_sigma[i], nmem)
                #kkk_params[i,:] = np.exp(kkk_params[i,:])
                #print(kkk_params[i,:])
            
        if alpha_vanG==True:
            alpha_vanG_mu, alpha_vanG_sigma = np.loadtxt(os.path.join(da_dirs['model_data_dir'],alpha_vanG_settings_file), skiprows=1, unpack=True, usecols = (0,1))
            alpha_vanG_params = np.zeros([nsoil_zones,nmem])
            nupd_param_alpha = nsoil_zones
            for i in range(nsoil_zones):
        
                alpha_vanG_params[i,:] = np.random.normal(np.log(alpha_vanG_mu[i]), alpha_vanG_sigma[i], nmem)
                #print(alpha_vanG_params[i,:])
                #print(np.max(np.exp(alpha_vanG_params)))
        
        if beta_vanG==True:
            beta_vanG_mu, beta_vanG_sigma = np.loadtxt(os.path.join(da_dirs['model_data_dir'],beta_vanG_settings_file), skiprows=1, unpack=True, usecols = (0,1))
            beta_vanG_params = np.zeros([nsoil_zones,nmem])
            nupd_param_beta = nsoil_zones
            for i in range(nsoil_zones):
        
                beta_vanG_params[i,:] = np.random.normal(np.log(beta_vanG_mu[i]), beta_vanG_sigma[i], nmem)
                #print(beta_vanG_params[i,:])

    if (kzones == True and alpha_vanG == False and beta_vanG == False):

        postX = np.r_[kzones_params]
        nupd_param_k = nsoil_zones
        nupd_param_alpha = 0
        nupd_param_beta = 0    
        
    if (kzones == True and alpha_vanG == True and beta_vanG == True):
        
        nupd_param_k = nsoil_zones
        nupd_param_alpha = nsoil_zones 
        nupd_param_beta = nsoil_zones
        postX = np.r_[kzones_params, alpha_vanG_params, beta_vanG_params] 

    #postX_exp = np.r_[np.exp(kkk_params), np.exp(alpha_vanG_params), np.exp(beta_vanG_params)] #el exp se debe hacer justo antes de escribir en grok y no aqui        
            
    #count, bins, ignored = plt.hist(kkk_params, 30, density=True)
    #plt.show()

    #count, bins, ignored = plt.hist(np.exp(kkk_params), 30, density=True)
    #plt.show()
    
    return postX, nupd_param_k, nupd_param_alpha, nupd_param_beta


def deal_w_parameters(kkk, kkk_eff, gen_kkk, k_settings_file,
                    storativity, storativity_eff, gen_storativity, sss_settings_file,
                    alpha_vanG, alpha_settings_file,
                    beta_vanG, beta_settings_file,
                    nxel, nyel, nzel, da_dirs, parallel, nmem, cpus, xlim, ylim, zlim, exptr):
    if kkk == False:
        nupd_param_k = 0
    elif kkk == True:
        if kkk_eff == True:
            nupd_param_k = 1
        elif kkk_eff == False:
            nupd_param_k =  nxel * nyel * nzel        # This is valid for kkk as it is now

        if gen_kkk == True:
            k_params = fg.readParam(os.path.join(da_dirs['model_data_dir'], k_settings_file))
            t1_start = time.time()
            if kkk_eff == False:

                if parallel:
                    postX = fg.parall_genFields_FFT_3d(nmem, cpus, xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel,
                                                       nzel, k_params['Y_model'],
                                                       k_params['Y_var'], k_params['beta_Y'], k_params['lmbx'], k_params['lmby'], k_params['lmbz'])
                elif not parallel:
                    postX = np.empty((int(nupd_param_k), nmem))  # Parameter and states
                    for ii in range(nmem):
                        postX[:, ii] = fg.genFields_FFT_3d(ii, xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel,
                                                           k_params['Y_model'],
                                                           k_params['Y_var'], k_params['beta_Y'], k_params['lmbx'], k_params['lmby'],
                                                           k_params['lmbz'])

            if kkk_eff == True:
                postX = fg.genGaussParams(k_params['beta_Y'], k_params['Y_var'], nmem)
            t2_stop = time.time()
            print("Time for generation of fields:", t2_stop - t1_start)


    if storativity == False:
        nupd_param_s = 0
    elif storativity == True:
        if storativity_eff == True:
            nupd_param_s = 1
        elif storativity_eff == False:
            nupd_param_s = nxel * nyel * nzel

        if gen_storativity == True:
            s_params = fg.readParam(os.path.join(da_dirs['model_data_dir'], sss_settings_file))
            t1_start = time.time()

            if storativity_eff == False:
                if parallel:
                    temp_postX = fg.parall_genFields_FFT_3d(nmem, cpus, xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel,
                                                            nzel, s_params['Y_model'],
                                                           s_params['Y_var'], s_params['beta_Y'], s_params['lmbx'], s_params['lmby'],
                                                           s_params['lmbz'])
                elif not parallel:
                    temp_postX = np.empty((int(nupd_param_s), nmem))  # Parameter and states
                    for ii in range(nmem):
                        temp_postX[:, ii] = fg.genFields_FFT_3d(ii, xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel,
                                                                nzel, s_params['Y_model'],
                                                           s_params['Y_var'], s_params['beta_Y'], s_params['lmbx'], s_params['lmby'],
                                                           s_params['lmbz'])

            if storativity_eff == True:
                temp_postX = fg.genGaussParams(s_params['beta_Y'], s_params['Y_var'], nmem)
            t2_stop = time.time()
            print("Time for generation of fields:", t2_stop - t1_start)

            if (kkk == True and gen_kkk == True):
                postX = np.r_[postX, temp_postX]
            if kkk == False:
                postX = temp_postX
            del temp_postX

        if gen_storativity == True or gen_kkk == True:
            if exptr is True:  # means that log parameters are transformed prior to model run
                postX = np.log(postX)
            np.save(os.path.join(da_dirs['param_dir'], 'X_prior_000.npy'), postX)
        else:
            postX = np.load(os.path.join(da_dirs['param_dir'], 'X_prior_000.npy'))

    if alpha_vanG == False or beta_vanG==False:
        nupd_param_alpha = 0
        nupd_param_beta= 0
    elif alpha_vanG == True and beta_vanG == True:
        nupd_param_alpha = 9
        nupd_param_beta= 9
        #lower, upper = 2, 13.7
        #mu, sigma = .5, 0.1
        #N = 9
        #X = stats.truncnorm((lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma)
        #temp_alpha_postX = X.rvs(9)
        #lower, upper = 1.3, 2.5
        #temp_beta_postX = X.rvs(9)
        temp_alpha_postX = np.random.default_rng().uniform(2,13.7,(9,4))
        temp_beta_postX = np.random.default_rng().uniform(1.3,2.5,(9,4))

    if (kkk == True and alpha_vanG == True and beta_vanG == True):
        postX = np.r_[postX, temp_alpha_postX, temp_beta_postX]

    #return postX, int(nupd_param_k), (nupd_param_s)
    return postX, int(nupd_param_k), (nupd_param_s), nupd_param_alpha, nupd_param_beta


def deal_w_parameters_tr(param_file, kkk, kkk_eff, dispersivity, dispersivity_eff, mpor, mpor_eff, impor, impor_eff, trcf, trcf_eff,
                    nxel, nyel, nzel):
    if kkk == False:
        nupd_param_k = 0
    elif kkk == True:
        if kkk_eff == True:
            nupd_param_k = 1
        elif kkk_eff == False:
            nupd_param_k =  nxel * nyel * nzel        # This is valid for kkk as it is now

    if dispersivity == False:
        nupd_param_disp = 0
    elif dispersivity == True:
        if dispersivity_eff == True:
            nupd_param_disp = 1
        elif dispersivity_eff == False:
            nupd_param_disp = nxel * nyel * nzel

    if mpor == False:
        nupd_param_mpor = 0
    elif mpor == True:
        if mpor_eff == True:
            nupd_param_mpor = 1
        elif mpor_eff == False:
            nupd_param_mpor = nxel * nyel * nzel

    if impor == False:
        nupd_param_impor = 0
    elif impor == True:
        if impor_eff == True:
            nupd_param_impor = 1
        elif impor_eff == False:
            nupd_param_impor = nxel * nyel * nzel

    if trcf == False:
        nupd_param_trcf = 0
    elif trcf == True:
        if trcf_eff == True:
            nupd_param_trcf = 1
        elif trcf_eff == False:
            nupd_param_trcf = nxel * nyel * nzel

    postX = np.load(param_file)

    return postX, int(nupd_param_k), int(nupd_param_disp), int(nupd_param_mpor), int(nupd_param_impor),int(nupd_param_trcf)


def wr_parameters_tr(kkk, disp, mpor, impor, trcf, da_dirs, postX, nmem, cpus, parallel, exptr, subgrid,
                  nelem, nupd_param_k, nupd_param_disp, nupd_param_mpor, nupd_param_impor, nupd_param_trcf):

    if (kkk is True) and (disp is False) and (mpor is False) and (impor is False) and (trcf is False):
        # todo: a single kkk is not really implemented.
        wr_all_kkk(da_dirs['inst_dir'], postX, n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr, subgrid=subgrid,
                      subgrid_dir=da_dirs['model_data_dir'], nelem=nelem)

    elif kkk is True:
        wr_all_kkk(da_dirs['inst_dir'], postX[0:nupd_param_k, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr, subgrid=subgrid,
                      subgrid_dir=da_dirs['model_data_dir'], nelem=nelem)
    if disp is True:
        ddd = postX[nupd_param_k:nupd_param_k + nupd_param_disp, :]
        upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, ddd=ddd, exptr=exptr)
    if mpor is True:
        mpor = postX[nupd_param_k+nupd_param_disp : nupd_param_k + nupd_param_disp + nupd_param_mpor, :]
        upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, mpor=mpor, exptr=exptr)
    if impor is True:
        impor = postX[nupd_param_k + nupd_param_disp + nupd_param_mpor : nupd_param_k + nupd_param_disp + nupd_param_mpor + nupd_param_impor, :]
        upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, impor=impor, exptr=exptr)
    if trcf is True:
        trcf = postX[nupd_param_k + nupd_param_disp + nupd_param_mpor + nupd_param_impor: nupd_param_k + nupd_param_disp + nupd_param_mpor + nupd_param_impor + nupd_param_trcf,:]
        upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, trcf=trcf, exptr=exptr)

    return

def wr_parameters(kkk, kzones, porosity, storativity, dispersivity, alpha_vanG, beta_vanG, da_dirs, postX, nmem, cpus, parallel, exptr, subgrid,
                  nelem, nupd_param_k, nupd_param_kzones, nupd_param_s, nupd_param_alpha_vanG, nupd_param_beta_vanG, storativity_eff, kkk_eff):
    
    if (kkk is True) and (porosity is False) and (storativity is False) and (dispersivity is False) and (alpha_vanG is False) and (beta_vanG is False):
        # todo: a single kkk is not really implemented.
        wr_all_kkk(da_dirs['inst_dir'], postX, n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr, subgrid=subgrid,
                      subgrid_dir=da_dirs['model_data_dir'], nelem=nelem)

    elif (kkk is True) and ((porosity is True) or (storativity is True) or (dispersivity is True)):
        wr_all_kkk(da_dirs['inst_dir'], postX[0:nupd_param_k, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr, subgrid=subgrid,
                      subgrid_dir=da_dirs['model_data_dir'], nelem=nelem)
        if storativity is True:
            # todo: a heterogeneous sss is not really implemented.
            sss = postX[nupd_param_k:nupd_param_k + nupd_param_s, :]
            if storativity_eff is True:
                upd_all_mprops_inst(da_dirs['inst_dir'], n_inst=nmem, n_cpu=cpus, parallel=parallel, sss=sss, exptr=exptr)
            elif storativity_eff is False:
                print("Heterogeneous sss is not yet supported")
    
    if (kkk is True) and (alpha_vanG is True) and (beta_vanG is True):
        # todo: a single kkk is not really implemented.
        wr_all_kkk(da_dirs['inst_dir'], postX[0:nupd_param_k, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr, subgrid=subgrid,
                      subgrid_dir=da_dirs['model_data_dir'], nelem=nelem)

        wr_all_alpha_vanG(da_dirs['inst_dir'], postX[nupd_param_k:nupd_param_k+nupd_param_alpha_vanG, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel)
        wr_all_beta_vanG(da_dirs['inst_dir'], postX[nupd_param_k+nupd_param_alpha_vanG:nupd_param_k+nupd_param_alpha_vanG+nupd_param_beta_vanG, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel)

    if (kzones is True) and (alpha_vanG is False) and (beta_vanG is False):
        # todo: a single kkk is not really implemented.
        wr_all_kkk(da_dirs['inst_dir'], postX[0:nupd_param_kzones, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr, subgrid=subgrid,
                      subgrid_dir=da_dirs['model_data_dir'], nelem=nelem,
                      kzones=kzones)

    if (kzones is True) and (alpha_vanG is True) and (beta_vanG is True):
        # todo: a single kkk is not really implemented.
        wr_all_kkk(da_dirs['inst_dir'], postX[0:nupd_param_kzones, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr, subgrid=subgrid,
                      subgrid_dir=da_dirs['model_data_dir'], nelem=nelem,
                      kzones=kzones)

        wr_all_alpha_vanG(da_dirs['inst_dir'], postX[nupd_param_kzones:nupd_param_kzones+nupd_param_alpha_vanG, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr)
        wr_all_beta_vanG(da_dirs['inst_dir'], postX[nupd_param_kzones+nupd_param_alpha_vanG:nupd_param_kzones+nupd_param_alpha_vanG+nupd_param_beta_vanG, :], n_inst=nmem, n_cpu=cpus,
                      parallel=parallel,
                      exptr=exptr)
        
    return


def wr_all_kkk(all_inst_dir, all_kkk, n_inst=1, n_cpu=1, parallel=False, exptr=False, subgrid=False, subgrid_dir=None, nelem=None, kzones=None):  # (inst_idx, inst_dir, startstr='!-Kini'):

    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(wr_kkk, zip(np.arange(0, n_inst, 1), all_kkk.T, repeat(all_inst_dir), repeat(exptr), repeat(subgrid), repeat(subgrid_dir), repeat(nelem), repeat(kzones)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0, n_inst):
            wr_kkk(ii, all_kkk[:,ii], all_inst_dir, exptr, subgrid, subgrid_dir, nelem, kzones)
    return


def wr_kkk(inst_idx, inst_kkk, inst_dir, exptr, subgrid, subgrid_dir, nelem, kzones):

    inst_string = 'hgs_inst_%.5d' % inst_idx
    cur_inst_dir = os.path.join(inst_dir, inst_string)
    cur_kkk_file = os.path.join(cur_inst_dir, "kkk_%.5d.txt" % int(inst_idx))
    printime(message='writing kkk file for < %s > ' % inst_string)

    if subgrid == True: # Do this before the exponential transformation
        mean_inst_kkk = inst_kkk.mean()

    if exptr == True:
        inst_kkk = np.exp(inst_kkk)
        if subgrid == True:
            mean_inst_kkk = np.exp(mean_inst_kkk)

    if subgrid == True:
        subgridelem = np.load(os.path.join(subgrid_dir, 'subGrid.elements.npy'))
        inst_kkk_temp = np.ones((nelem,)) * mean_inst_kkk
        inst_kkk_temp[subgridelem] = inst_kkk
        inst_kkk = inst_kkk_temp
        
    if kzones == False:    
        np.savetxt(cur_kkk_file, np.transpose((np.arange(1, len(inst_kkk) + 1, 1), inst_kkk, inst_kkk, inst_kkk)),
                   fmt='%d %.6e %.6e %.6e')
        
    if kzones == True:
        cur_kkk_file = os.path.join(cur_inst_dir, "kzones_%.5d.txt" % int(inst_idx))
        np.savetxt(cur_kkk_file, np.transpose(inst_kkk),
           fmt='%.6e')
    return


def wr_all_alpha_vanG(all_inst_dir, all_alpha_vanG, n_inst=1, n_cpu=1, parallel=False, exptr=False):  # (inst_idx, inst_dir, startstr='!-Kini'):

    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(wr_alpha_vanG, zip(np.arange(0, n_inst, 1), all_alpha_vanG.T, repeat(all_inst_dir), repeat(exptr)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0, n_inst):
            wr_alpha_vanG(ii, all_alpha_vanG[:,ii], all_inst_dir, exptr)
    return

def wr_all_beta_vanG(all_inst_dir, all_beta_vanG, n_inst=1, n_cpu=1, parallel=False, exptr=False):  # (inst_idx, inst_dir, startstr='!-Kini'):

    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(wr_beta_vanG, zip(np.arange(0, n_inst, 1), all_beta_vanG.T, repeat(all_inst_dir), repeat(exptr)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0, n_inst):
            wr_beta_vanG(ii, all_beta_vanG[:,ii], all_inst_dir, exptr)
    return


def wr_alpha_vanG(inst_idx, inst_alpha_vanG, inst_dir, exptr):

    inst_string = 'hgs_inst_%.5d' % inst_idx
    cur_inst_dir = os.path.join(inst_dir, inst_string)
    cur_alpha_vanG_file = os.path.join(cur_inst_dir, "alpha_%.5d.txt" % int(inst_idx))
    printime(message='writing alpha_vanG file for < %s > ' % inst_string)

    if exptr == True:
        inst_alpha_vanG = np.exp(inst_alpha_vanG)

    np.savetxt(cur_alpha_vanG_file, np.transpose(inst_alpha_vanG), fmt='%.6e')
    return

def wr_beta_vanG(inst_idx, inst_beta_vanG, inst_dir, exptr):

    
    def get_truncated_normal(mean=0, sd=.5, low=0, upp=1):
        return scipy.stats.truncnorm(
            (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)
    
    inst_string = 'hgs_inst_%.5d' % inst_idx
    cur_inst_dir = os.path.join(inst_dir, inst_string)
    cur_beta_vanG_file = os.path.join(cur_inst_dir, "beta_%.5d.txt" % int(inst_idx))
    printime(message='writing beta_vanG file for < %s > ' % inst_string)

    if exptr == True:
        inst_beta_vanG = np.exp(inst_beta_vanG)

    #inst_beta_vanG[1.01>inst_beta_vanG] = 1.01 #grok da error si el valor de beta es menor a 1
    
    if any(inst_beta_vanG<1.01):
        # SE AGREGÓ ESTA FUNCION PARA SUMARLE AL VALOR DE BETA (UNA DISTRIBUCION NORMAL) HASTA QUE QUE ALCANCE VALOR MAYOR A 1.01
        myrvs = get_truncated_normal(mean=0, sd=.2, low=0, upp=1)
            
        while any(1.01>inst_beta_vanG):
            inst_beta_vanG[1.01>inst_beta_vanG] = inst_beta_vanG[1.01>inst_beta_vanG] + myrvs.rvs()
    
    np.savetxt(cur_beta_vanG_file, np.transpose(inst_beta_vanG), fmt='%.6e')
    
    return


def CreatePrefixBatch(mydir, myString):
    """
    Create a new Prefix file for running GROK and HGS
        Arguments:
        ----------
    mydir:      str, directory where the file should be located
    myString:   str, string to add to the prefix file
        Returns:
        --------
    The batch file batch.pfx with problem prefix
    """
    prefFile = 'batch.pfx'
    if os.path.isfile(prefFile):
        os.remove(os.path.join(mydir, prefFile))

    prefix = open(os.path.join(mydir, prefFile), 'w')
    prefix.write(myString)
    prefix.close()
    return

def Createletmerun(mydir, myString, hsplot=False):
    """
    Create a letmerun batch file for running GROK, HGS and HSPLOT
        Argument:
        ---------
    mydir:      str, directory where the file should be located
    myString:   str, name of the GROK file to be executed
    hsplot:     bool, True: include instruction to run hsplot
                             False: just include grok and phgs
        Returns:
        --------
    The batch file letmerun.bat
    """
    batchfile = 'letmerun.bat'
    if batchfile in os.listdir(mydir):
        os.remove(os.path.join(mydir, batchfile))

    letmerun = open(os.path.join(mydir, batchfile), 'w')
    letmerun.write('@echo off\n\ndel *' + myString + 'o* > nul\n\n')
    if platform.system() == 'Linux':
        letmerun.write('grok.x\nhgs.x\n')
    else:
        letmerun.write('grok\nphgs\n')

    if hsplot is True:
        if platform.system() == 'Linux':
            letmerun.write('hsplot.x\n')
        else:
            letmerun.write('hsplot\n')

    letmerun.close()
    return


def collect_all_ic(all_inst_dir, ic_dir, ext='hen', n_inst=1, n_cpu=1, parallel=False):  # (inst_idx, inst_dir, startstr='!-Kini'):

    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(collect_ic, zip(np.arange(0, n_inst, 1), repeat(ic_dir), repeat(all_inst_dir), repeat(ext)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0, n_inst):
            collect_ic(ii, ic_dir, all_inst_dir, ext)
    return


def collect_ic(inst_idx, ic_dir, inst_dir, ext):

    inst_string = 'hgs_inst_%.5d' % inst_idx

    cur_inst_dir = os.path.join(inst_dir, inst_string)
    ic_file = dm.getdirs(cur_inst_dir, mystr=ext, fullpath=True)[0]
    new_ic_file = 'ic_' + os.path.split(ic_file)[-1]
    new_ic_file = os.path.join(ic_dir, new_ic_file)

    shutil.copy(ic_file, new_ic_file)
    printime(message='ic file << %s >> for %s copied to ic dir' % (os.path.split(ic_file)[-1], inst_string))
    return

def cp_all_ic2inst(all_inst_dir, ic_dir, ext='hen', file_id=None, n_inst=1, n_cpu=1, parallel=False):  # (inst_idx, inst_dir, startstr='!-Kini'):
    ext = ext.strip('.')
    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(cp_ic2inst, zip(np.arange(0, n_inst, 1), repeat(ic_dir), repeat(all_inst_dir), repeat(file_id), repeat(ext)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0, n_inst):
            cp_ic2inst(ii, ic_dir, all_inst_dir, file_id, ext)
    return


def cp_ic2inst(inst_idx, ic_dir, inst_dir, file_id, ext):

    inst_string = 'hgs_inst_%.5d' % inst_idx

    cur_inst_dir = os.path.join(inst_dir, inst_string)
    ic_file = 'ic_' + file_id + '_' + inst_string + 'o.' + ext

    shutil.copy(os.path.join(ic_dir, ic_file), os.path.join(cur_inst_dir, ic_file))
    printime(message='ic file << %s >> copied back to %s' % (ic_file, inst_string))
    return


def read_all_states_obs(all_inst_dir, obs_id, obs_nodes, nnodes, nodes_coord, element_nodes, hyd_heads, head2gwt=False, obs_loc_gwt_idx=None, depth2gwt=False, wat_cont=False, poro_data=None, neg2zero=False, ts=1, file_id=None, n_inst=1, n_cpu=1, parallel=False):
    # comparison = obs_all2 == obs_all
    # equal_arrays = comparison.all()
    obs_all = np.empty((ts, len(obs_id), n_inst))
    if parallel:
        
        
        mypool = mp.Pool(n_cpu)
        
        if hyd_heads==True:
            obs_all_temp= mypool.starmap(read_states_obs, zip(np.arange(0, n_inst, 1), repeat(all_inst_dir), repeat(file_id), repeat(obs_id),
                                                repeat(obs_nodes), repeat(nnodes), repeat(element_nodes), repeat(wat_cont), repeat(poro_data), repeat(neg2zero), repeat(ts)))

        elif head2gwt == True:
            obs_all_temp= mypool.starmap(read_states_obs_gwt, zip(np.arange(0, n_inst, 1), repeat(all_inst_dir), repeat(file_id), repeat(obs_id),
                                                repeat(obs_nodes), repeat(nnodes) , repeat(nodes_coord), repeat(obs_loc_gwt_idx), repeat(depth2gwt), repeat(neg2zero), repeat(ts)))
        
        elif wat_cont == True:
            file_id='sat_pm.'
            obs_all_temp= mypool.starmap(read_states_obs, zip(np.arange(0, n_inst, 1), repeat(all_inst_dir), repeat(file_id), repeat(obs_id),
                                                repeat(obs_nodes), repeat(nnodes), repeat(element_nodes), repeat(wat_cont), repeat(poro_data), repeat(neg2zero), repeat(ts)))
              
        
        mypool.close()
        mypool.join()
        # Reorder model output
        for ii in range(0, len(obs_all_temp)):
            obs_all[:, :, ii] = np.asarray(obs_all_temp[ii])
    
    if not parallel:
            
        if head2gwt:
            for ii in range(0, n_inst):
                obs_all[:,:,ii] = read_states_obs_gwt(ii, all_inst_dir, file_id, obs_id, obs_nodes, nnodes, nodes_coord, obs_loc_gwt_idx, depth2gwt, neg2zero, ts)
        
        elif wat_cont:
            file_id='sat_pm.'
            for ii in range(0, n_inst):
                obs_all[:,:,ii] = read_states_obs(ii, all_inst_dir, file_id, obs_id, obs_nodes, nnodes, element_nodes, wat_cont, poro_data, neg2zero, ts)
        
        elif hyd_heads:
            for ii in range(0, n_inst):
                obs_all[:,:,ii] = read_states_obs(ii, all_inst_dir, file_id, obs_id, obs_nodes, nnodes, element_nodes, poro_data, wat_cont, neg2zero, ts)
            

    return obs_all


def read_states_obs(inst_idx, inst_dir, file_id, obs_id, obs_nodes, nnodes, element_nodes, wat_cont, poro_data, neg2zero, ts):

    inst_string = 'hgs_inst_%.5d' % inst_idx
    cur_inst_dir = os.path.join(inst_dir, inst_string)
    print('Reading states from %s' % inst_string)
    # cur_inst_dir = r'D:\Users\Emilio\_codedev2020\enkf_2020\models\tr_3a_first'

    if isinstance(file_id, str) is True: file_id = [file_id]
    for aa, cur_file_id in enumerate(file_id):
        cur_files = dm.getdirs(cur_inst_dir, mystr=cur_file_id, onlylast=False, fullpath=True)

        # This is in case no convergeance happens in hgs
        if len(cur_files) == 0:
            obs_all = np.zeros((ts, len(obs_id)))
            obs_all_tot = np.zeros((ts, len(obs_id)))
        else:
            if len(cur_files) > 1:
                cur_files = [x for x in cur_files if ("ic_" not in x)]
                cur_files = [x for x in cur_files if ("upd" not in x)]
                cur_files = [x for x in cur_files if ("hen_ts" not in x)]
            if (len(file_id) > 1) and (aa == 0):
                obs_all_tot = np.zeros((len(cur_files), len(obs_id)))

            obs_all = np.empty((len(cur_files), len(obs_id)))

            for jj, cur_file in enumerate(cur_files):
                try:
                    cur_states, dummy = readHGSbin(cur_file, nnodes, endfile=False)
                except:
                    cur_states, dummy = readHGSbin(cur_file, nnodes, endfile=True)
                if len(cur_states) == 0:
                    obs_all = np.zeros((ts, len(obs_id)))
                    obs_all_tot = np.zeros((ts, len(obs_id)))
                else:
                    if wat_cont==True:
                        
                        cur_states_cc = nodes2cc(cur_states,element_nodes) #jaime
                        cur_states = cur_states_cc*poro_data #jaime
                    
                    for ii, cur_obs in enumerate(obs_id):
                        cur_nodes = obs_nodes[cur_obs]
                        if isinstance(cur_nodes, int): cur_nodes = np.array([cur_nodes])[np.newaxis].astype(int)
                        if isinstance(cur_nodes, list) or isinstance(cur_nodes,np.ndarray): cur_nodes = cur_nodes.astype(int)
                        # if : cur_nodes = cur_nodes.astype(int)
                        # else:
                        #     cur_nodes = np.array([cur_nodes]).astype(int)
                        cur_states_obs = cur_states[cur_nodes-1]

                        if neg2zero == True:
                            zero_id = np.where(cur_states_obs < 0)
                            try:
                                cur_states_obs[zero_id[0]] = 0
                            except (IndexError, TypeError):
                                cur_states_obs[np.newaxis][zero_id[0]] = 0
                                cur_states_obs = cur_states_obs[np.newaxis]
                                cur_nodes = cur_nodes[np.newaxis]
                        if cur_nodes.ndim == 0: cur_nodes = cur_nodes[np.newaxis]
                        if len(cur_nodes) > 1:
                            cur_states_obs = mean(cur_states_obs)

                        obs_all[jj, ii] = cur_states_obs
                    del cur_states

        if len(file_id) > 1:
            obs_all_tot += obs_all

    if len(file_id) > 1:
        obs_all = obs_all_tot

    return obs_all

def read_states_obs_gwt(inst_idx, inst_dir, file_id, obs_id, obs_nodes, nnodes, nodes_coord, obs_loc_gwt_idx, depth2gwt, neg2zero, ts):
    
    inst_string = 'hgs_inst_%.5d' % inst_idx
    cur_inst_dir = os.path.join(inst_dir, inst_string)
    print('Reading states from %s' % inst_string)
    # cur_inst_dir = r'D:\Users\Emilio\_codedev2020\enkf_2020\models\tr_3a_first'

    if isinstance(file_id, str) is True: file_id = [file_id]
    for aa, cur_file_id in enumerate(file_id):
        cur_files = dm.getdirs(cur_inst_dir, mystr=cur_file_id, onlylast=False, fullpath=True)

        # This is in case no convergeance happens in hgs
        if len(cur_files) == 0:
            obs_all = np.zeros((ts, len(obs_id)))
            obs_all_tot = np.zeros((ts, len(obs_id)))
        else:
            if len(cur_files) > 1:
                cur_files = [x for x in cur_files if ("ic_" not in x)]
                cur_files = [x for x in cur_files if ("upd" not in x)]
                cur_files = [x for x in cur_files if ("hen_ts" not in x)]
            if (len(file_id) > 1) and (aa == 0):
                obs_all_tot = np.zeros((len(cur_files), len(obs_id)))

            obs_all = np.empty((len(cur_files), len(obs_id)))

            for jj, cur_file in enumerate(cur_files):
                try:
                    cur_states, dummy = readHGSbin(cur_file, nnodes, endfile=False)
                except:
                    cur_states, dummy = readHGSbin(cur_file, nnodes, endfile=True)
                if len(cur_states) == 0:
                    obs_all = np.zeros((ts, len(obs_id)))
                    obs_all_tot = np.zeros((ts, len(obs_id)))
                else:
                    #for ii, cur_obs in enumerate(obs_id):
                        #cur_nodes = obs_nodes[cur_obs]
                        #cur_idx = obs_nodes[cur_obs]
                        #cur_states_obs = cur_states[cur_nodes-1]
                        #cur_states_rshp = np.reshape(cur_press,(x,y,z), order='F')
                        
                    all_gwt = gwt.heads2gwt(cur_states, nodes_coord, depth2gwt)
                    cur_states_obs_gwt = all_gwt[obs_loc_gwt_idx[:,0],obs_loc_gwt_idx[:,1]]
                    obs_all[jj,:] = cur_states_obs_gwt
                    del cur_states

        if len(file_id) > 1:
            obs_all_tot += obs_all

    if len(file_id) > 1:
        obs_all = obs_all_tot

    return obs_all


    
def read_old_states_obs(states_dir, counter_simSteps, mod_data, nmem, nnodes, obs_loc_id, obs_loc_nodes):

    # if (counter_simSteps-1) == 0:
    #     allfiles = dm.getdirs(states_dir, mystr='upd', fullpath=True)
    #     if len(allfiles) > 0:
    #         dummy = [os.remove(s) for s in allfiles]
    mod_data_pr = np.zeros((mod_data.shape[0] + (counter_simSteps-1), mod_data.shape[1], mod_data.shape[2]))

    old_states_files_all = dm.getdirs(states_dir, mystr='ts', fullpath=True)
    old_states_files = []
    if len(old_states_files_all) > 0:
        # old_states_files_noupd = [s for s in old_states_files_all if not "upd" in s]
        # old_states_files_upd = [s for s in old_states_files_all if "upd" in s]
        for cur_member in range(0, nmem):
            cur_files = [s for s in old_states_files_all if "hgs_inst_%.5d" % cur_member in s]
            for cur_ts in range(1, counter_simSteps+1):
                cur_ts_files = [s for s in cur_files if "ts%.3d" % cur_ts in s]
                if any('upd' in string for string in cur_ts_files) == True:
                    the_file = [x for x in cur_ts_files if ("upd" in x)]
                else:
                    the_file = [x for x in cur_ts_files if not ("upd" in x)]
                old_states_files.append(the_file)
        old_states_files = [item for sublist in old_states_files for item in sublist]

    if len(old_states_files) > 0:
        for ff in range(0, nmem):
            matching = [s for s in old_states_files if "hgs_inst_%.5d" % ff in s]
            for jj, cur_file in enumerate(matching):
                try:
                    cur_states, dummy = readHGSbin(cur_file, nnodes, endfile=False)
                except:
                    cur_states, dummy = readHGSbin(cur_file, nnodes, endfile=True)
                if len(cur_states) == 0:
                    cur_states = np.zeros((nnodes))
                for ii, cur_obs in enumerate(obs_loc_id):
                    cur_nodes = obs_loc_nodes[cur_obs]
                    if isinstance(cur_nodes, int): cur_nodes = np.array([cur_nodes])[np.newaxis].astype(int)
                    if isinstance(cur_nodes, list) or isinstance(cur_nodes, np.ndarray): cur_nodes = cur_nodes.astype(int)
                    cur_states_obs = cur_states[cur_nodes - 1]
                    if cur_nodes.ndim == 0: cur_nodes = cur_nodes[np.newaxis]
                    if len(cur_nodes) > 1:
                        cur_states_obs = np.mean(cur_states_obs)
                    mod_data_pr[jj, ii, ff] = cur_states_obs
    mod_data_pr[-1, :, :] = mod_data
    mod_data = mod_data_pr
    mod_data_pr = np.zeros(mod_data.shape)

    return mod_data_pr, mod_data

    
def transf_state_obs(obs, ini_head=None, neg2zero=False, head2dwdn=False, cum=False, norm=False, OL=False):

    copy_modmeas = np.copy(obs)

    if neg2zero:
        zero_id = np.where(copy_modmeas < 0)
        try:
            copy_modmeas[zero_id] = 0
        except IndexError:
            copy_modmeas[np.newaxis][zero_id] = 0

    if head2dwdn:
        assert ini_head != None, 'Initial head to get drawdown from heads not defined!'
        copy_modmeas = np.subtract(ini_head, copy_modmeas)

    if cum:
        if OL:
            copy_modmeas = np.cumsum(copy_modmeas, axis=0)
        else:
            copy_modmeas = np.cumsum(copy_modmeas, axis=0)
            # todo: No se si esto esta bien....copy_modmeas = np.cumsum(copy_modmeas, axis=0)[-1,:]  # Not yet normalized!

    if (norm is True) and (cum is True):
        copy_modmeas /= np.sum(copy_modmeas, axis=0)
    elif (norm is True) and (cum is False):
        copy_modmeas = (copy_modmeas - min(copy_modmeas)) / (max(copy_modmeas) - min(copy_modmeas))

    # if moments:
    #     copy_modmeas = mysst.calc_tempMoments(mytimes, copy_modmeas)


    return copy_modmeas

def rd_obsnodes(myfile):
    """ """
    n_headers = 3
    try:
        idx_line, hgsTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)
        obs_type = 'well'
    except:
        obs_type = 'point'
        idx_line = n_headers

    with open(myfile) as current_file:
        for ii in range(0, n_headers+1):
            last_column_index = len(current_file.readline().split())

    if isinstance(idx_line, int) is True:
        obs_nodes = np.loadtxt(myfile, usecols=(last_column_index-1),max_rows=1, skiprows=n_headers)

    if isinstance(idx_line, int) is False:
        obs_nodes = np.loadtxt(myfile, usecols=(last_column_index - 1,), max_rows=((idx_line[1] - 1) - (idx_line[0] + 1) + 1),
                               skiprows=n_headers)

    return obs_type, obs_nodes


def read_ref_obs_data(model_dir, store_dir, coord_file, nodes_file, sim_file, output_times=None, mystr=None, transform=False,
                      ini_head=None, neg2zero=False, head2dwdn=False, cum=False, norm=False, OL=False, dualdomain=False):

    try:
        bb = len(output_times)
    except TypeError:
        output_times = output_times[np.newaxis]
    files = dm.getdirs(model_dir, mystr=mystr, fullpath=True, onlylast=False)
    # Read node and coordinates:
    X, Y, Z, nodes_dict = np.empty((len(files),)), np.empty((len(files),)), np.empty((len(files),)), {}
    obs_id = np.empty((len(files),),dtype='U25')

    for ii, cur_file in enumerate(files):
        try:
            X[ii], Y[ii], Z[ii] = np.loadtxt(cur_file, skiprows=3, usecols=(4, 5, 6), unpack=True, max_rows=1)
        except:
            X[ii], Y[ii], Z[ii] = np.loadtxt(cur_file, skiprows=3, usecols=(3, 4, 5), unpack=True, max_rows=1)
        if dualdomain == False:
            obs_id[ii] = cur_file.split('.')[-2]
        elif dualdomain is True:
            obs_id[ii] = cur_file.split('.')[-3]
        nodes_dict[obs_id[ii]] = rd_obsnodes(cur_file)[-1]

    sim_data = np.empty((len(output_times), len(files)))
    if transform:
        sim_data_tr = np.empty((len(output_times), len(files)))
    for ii, cur_file in enumerate(files):
        sim_data[:, ii] = rmodout(cur_file, output_times, dualdomain=dualdomain)
        if transform:
            sim_data_tr[:, ii] = transf_state_obs(sim_data[:, ii], ini_head=ini_head, neg2zero=neg2zero, head2dwdn=head2dwdn,
                                                  cum=cum, norm=norm, OL=OL)

    if transform:
        sim_data = sim_data_tr
    # Store stuff in different files:
    with open(os.path.join(store_dir, coord_file), 'w') as f:
        f.write("# name, Xcoo, Ycoo, Zcoo\n")
        for ii in range(0, len(obs_id)):
            f.write("%s %9.8e %9.8e %9.8e\n" %(obs_id[ii], X[ii], Y[ii], Z[ii]))
    print('File < %s > created' % os.path.join(store_dir, coord_file))
    np.savetxt(os.path.join(store_dir, sim_file), sim_data)
    np.save(os.path.join(store_dir, nodes_file), nodes_dict)
    print('File < %s > created' % os.path.join(store_dir, sim_file))

    return sim_data, X, Y, Z, nodes_dict, obs_id


def cp_all_states(all_inst_dir, states_dir, file_id=None, tstep_id='', upd_states=False, n_inst=1, n_cpu=1, parallel=False):

    if parallel:
        mypool = mp.Pool(n_cpu)
        mypool.starmap(cp_states, zip(np.arange(0, n_inst, 1), repeat(all_inst_dir), repeat(states_dir),
                                                            repeat(file_id), repeat(tstep_id), repeat(upd_states)))
        mypool.close()
        mypool.join()
    else:
        for ii in range(0, n_inst):
            cp_states(ii, all_inst_dir, states_dir, file_id, tstep_id, upd_states)
    return


def cp_states(inst_idx, inst_dir, states_dir, file_id, tstep_id, upd_states):

    inst_string = 'hgs_inst_%.5d' % inst_idx
    cur_inst_dir = os.path.join(inst_dir, inst_string)
    cur_files = dm.getdirs(cur_inst_dir, mystr=file_id, onlylast=False, fullpath=True)
    if len(cur_files) > 1:
        cur_files = [ x for x in cur_files if ("ic_" not in x) ]
        cur_files = [ x for x in cur_files if ("upd" not in x)]
        cur_files = [x for x in cur_files if ("hen_ts" not in x)]
    if (isinstance(cur_files, list)) or ((isinstance(cur_files, np.ndarray))):
        cur_files = cur_files[-1]
#    assert '_prev' not in cur_files, "Final states file has been already updated, cannot proceed"
    st_file = os.path.split(cur_files)[-1]
    if not upd_states:
        new_id = '_ts%.3d' % tstep_id
    elif upd_states:
        new_id = '_ts%.3dupd' % tstep_id
    new_cur_file = os.path.join(states_dir, st_file + new_id)
    shutil.copy2(cur_files, new_cur_file)

    return

def cp_all_states2inst(all_inst_dir, states_dir, tstep_id='', updating=True):

    states_files = dm.getdirs(states_dir, mystr='ts%.3d' % tstep_id, onlylast=False, fullpath=True)

    if updating == True:
        states_files = [x for x in states_files if "upd" in x]
    elif updating == False:
        states_files = [x for x in states_files if not "upd" in x]

    for ii, cur_file in enumerate(states_files):
        inst_string = 'hgs_inst_%.5d' % ii
        cur_inst_dir = os.path.join(all_inst_dir, inst_string)
        the_file = os.path.split(cur_file)[-1]
        shutil.copy2(cur_file, os.path.join(cur_inst_dir, the_file))
    return


def rmodout(myfile, mytimes, dualdomain=False):
    """ Read HydroGeoSphere outputs. Reads from well-like and point-like type of observation. It supports reading heads
    from flow simulations and concentrations from a single- and double-domain model. If the model outputs are from a
    well-type of observation, it returns the averaged value over all
            Arguments:
            ----------
        myfile:     str, fullpath with the name of the file to be loaded
        mytimes:    np array,  float or int array with times of interest
        mymode:     str, model run mode. 'fl_' for flow model, 'tr_for transport'
        dualdomain: bool, if True, reads mob and immob concentrations from a dual domain transport model. Default = True
            Returns:
            --------
        mymodmeas:     np.array, with model outputs at specific times (nmeas x 1)
        """

    if dualdomain is True:
        try:    # Observation well HGS format:
            mymodmeas = []
            idx_line, hgsTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)
            if isinstance(idx_line, int) is True:
                hgsTime = np.asarray(hgsTime, dtype=float)
                mymodmeas1, mymodmeas2 = np.loadtxt(myfile, skiprows=3, usecols=(0,1), unpack=True)
                mymodmeas = np.mean(mymodmeas1) + np.mean(mymodmeas2)
            if isinstance(idx_line, int) is False:
                with open(myfile, 'r') as f:
                    mylines = f.readlines()
                for yy in range(0, len(idx_line), 1):
                    mymodemeas1_temp, mymodemeas2_temp = [], []
                    if yy < len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1: idx_line[yy + 1]])
                    elif yy == len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1:])
                    for cur_line in myvalues:
                        cur_values = list(filter(None, np.asarray(cur_line.split(' ')).tolist()))
                        mymodemeas1_temp.append(float(cur_values[0]))
                        mymodemeas2_temp.append(float(cur_values[1]))
                    mymodmeas1, mymodmeas2 = mean(mymodemeas1_temp), mean(mymodemeas2_temp)
                    mymodmeas.append(mymodmeas1 + mymodmeas2)
            if (isinstance(mymodmeas, int) or isinstance(mymodmeas, np.float64)) is True:
                mymodmeas = np.asarray(mymodmeas)
                mymodmeas = np.expand_dims(mymodmeas, axis=1)
        except: # Observation point HGS format:
            try:
                hgsTime, mymodmeas1, mymodmeas2 = np.loadtxt(myfile, skiprows=2, usecols=(0, 1, 2), unpack=True)
            except:
                hgsTime, mymodmeas1, mymodmeas2 = np.loadtxt(myfile, skiprows=3, usecols=(0, 1, 2), unpack=True)

            mymodmeas = mymodmeas1 + mymodmeas2

    else:
        try:    # Observation well HGS format:
            mymodmeas = []
            idx_line, hgsTime = dm.str_infile(myfile, 'SOLUTIONTIME=', index=True)
            if isinstance(idx_line, int) is True:
                hgsTime = np.asarray(hgsTime, dtype=float)
                mymodmeas = np.loadtxt(myfile, skiprows=3, usecols=(0,))
                mymodmeas = np.mean(mymodmeas)
            if isinstance(idx_line, int) is False:
                with open(myfile, 'r') as f:
                    mylines = f.readlines()
                for yy in range(0, len(idx_line), 1):
                    if yy < len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1: idx_line[yy + 1]])
                    elif yy == len(idx_line) - 1:
                        myvalues = np.asarray(mylines[idx_line[yy] + 1:])
                    try:
                        mymodmeas.append(mean([float(cur_line.split(' ')[2]) for cur_line in myvalues]))
                    except:
                        mymodmeas.append(mean([float(cur_line.split(' ')[3]) for cur_line in myvalues]))
            if (isinstance(mymodmeas, int) or isinstance(mymodmeas, np.float64)) is True:
                mymodmeas = np.asarray(mymodmeas)
                mymodmeas = np.expand_dims(mymodmeas, axis=1)
        except: # Observation point HGS format:
            hgsTime, mymodmeas = np.loadtxt(myfile, skiprows=3, usecols=(0, 1), unpack=True)


    # If a single output time is generated, create an extra dimension:
    if (isinstance(mytimes, int) is True) or (isinstance(mytimes, float) is True):
        mytimes = np.expand_dims(mytimes, 1)

    # %% Mask to get only the elements of interest:
    if isinstance(mymodmeas, np.ndarray) is False:
        mymodmeas = np.expand_dims(mymodmeas, axis=1)
        if len(mytimes) == 1:
            mytimes = int(round(mytimes[0] * 10)) / 10

    mymodmeas = mymodmeas[np.where(np.in1d(hgsTime, mytimes))]

    if mymodmeas.ndim > 1:
        mymodmeas = mymodmeas.squeeze()
    return mymodmeas

def procmodout(modmeas, mytimes, ini_head=0.0, neg2zero=False, head2dwdn=False, cum=False, norm=False, moments=False, ref=False):
    """
    Process the loaded model outputs, according to the settings provided.
    Args:
        modmeas:
        mytimes:
        ini_head:
        neg2zero:
        head2dwdn:
        cum:
        norm:
        moments:
    Returns:
    """
    copy_modmeas = np.copy(modmeas)

    if (neg2zero is True) or (neg2zero == 'True'):
        zero_id = np.where(copy_modmeas < 0)
        try:
            copy_modmeas[zero_id[0]] = 0
        except IndexError:
            copy_modmeas[np.newaxis][zero_id[0]] = 0

    if (head2dwdn is True) or (head2dwdn == 'True'):
        copy_modmeas = np.subtract(ini_head, copy_modmeas)

    if (cum is True) or (cum == 'True'):

        if ref is False:
            copy_modmeas = np.cumsum(copy_modmeas)[-1]  # Not yet normalized!
        elif ref is True:
            copy_modmeas = np.cumsum(copy_modmeas)  # Not yet normalized!
    if ((norm is True) and (cum is True)) or ((norm == 'True') and (cum == 'True')):
        copy_modmeas /= np.sum(copy_modmeas)
    elif ((norm is True) and (cum is False)) or ((norm == 'True') and (cum == 'False')):
        copy_modmeas = (copy_modmeas - min(copy_modmeas)) / (max(copy_modmeas) - min(copy_modmeas))

    if (moments is True) or (moments == 'True'):
        copy_modmeas = mysst.calc_tempMoments(mytimes, copy_modmeas)

    return copy_modmeas


def getmodout(mypath, outputtime='', initial_head=7.0, neg2zero=False, head2dwdn=False, cumbtc=False, dualdomain=False,
              norm=False, moments=False, curtimestep=None, curreal=None, dirtree=None, ref=False):
    """
    Read model output from a file at specific output times.
    Time values will be integers. Corrects negative concentrations when
    working in transport mode.
        Arguments:
        ----------
    mypath:         str, directory where model outputs are located
    mode:           Str, 'fl_' or 'tr_'
    outputtime:     output times of interest
        Returns:
        --------
    all_modout:   2D np array, observed values at defined times
                   size of the array = [No times x No Obs Points]. Fortran order
    """
    mystrs = 'observation_well'
    # mystrs = 'well_conc'

    # Get a list of the files of all observation points:
    files = dm.getdirs(mypath, mystr=mystrs, fullpath=True, onlylast=False)

    # Start collecting them in a single 2D array
    all_modout = np.array([])

    for idx, cur_file in enumerate(files):
        selMeas = rmodout(cur_file, outputtime, dualdomain=dualdomain)

        if not isinstance(initial_head, (float, str)):
            local_inihead = initial_head[idx]
        else:
            local_inihead = initial_head

        transfMeas = procmodout(selMeas, outputtime, ini_head=local_inihead, neg2zero=neg2zero,
                                head2dwdn=head2dwdn, cum=cumbtc, norm=norm, moments=moments,
                                curtimestep=curtimestep, curreal=curreal, mymode=mode, obsid=idx, dirtree=dirtree,
                                ref=ref)
        # selMeas = rmodout(ii, outputtime.astype(np.int))
        all_modout = np.append(all_modout, transfMeas)  # All selected heads

    all_modout = np.reshape(all_modout, (-1, len(files)), order='F')

    return all_modout


def GenReal(str2pass):
    """ Run realizations with the possibility of doing it in parallel """
    # Create variables with the proper part of the string
    import re
    Str2Assign, homedir, mode, Ini_ensemble, genfields, grid_filedir, \
    initial_head, mytype, str_assim, \
    neg2zero, head2dwdn, cumbtc, norm, moments, porosity, storativity,\
    dispersivity, subgrid, spinup, hgs = list2var(str2pass)

    if subgrid is True:
        xlen, ylen, zlen, nxel, nyel, nzel, \
        xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, \
        lambda_y, lambda_z = fg.readParam(os.path.join(homedir, 'model_data', '_RandFieldsParameters.dat'))
        if os.path.isfile(os.path.join(os.path.split(grid_filedir)[0], 'subGrid.elements.npy')) is False:
            print('Gathering subgrid elements...')
            if re.match('[0-9]{4}',hgs):       # eg. hgs = 2014 --> gridformat = tp2014
                gridformat = 'tp' + hgs
            else:                              # what else: maybe hgs=gms? Since gridformat = gms is allowed
                gridformat = hgs
            subgridnodes, subgridelem = gm.sel_grid(grid_filedir, xlim, ylim, zlim, store=True, gridformat=gridformat)
            subgridnodes = None
        else:
            subgridelem = np.load(os.path.join(os.path.split(grid_filedir)[0], 'subGrid.elements.npy'))
            subgridnodes = None
    # %%
    try:
        NoReal = np.asarray(Str2Assign).astype('int') - 1
    except:
        NoReal = np.asarray(Str2Assign).astype('str')

    # %% Some headers for multiprocessing module:
    scipy.random.seed()
    time.sleep(np.random.uniform(low=1.0, high=1.5, size=1))  # Process_created = mp.Process()
    Process_current = mp.current_process()
    print('Process No. %s started by %s...' % (Str2Assign, Process_current.name))

    # %%  mesh data:
    meastimes = '_outputtimes%s.dat' % mode
    modeltimes = np.loadtxt(os.path.join(homedir, 'model_data', meastimes))

    # Generate subgridData if decided to work with a subset:
    # if subgrid is True:
    #     subgridNodes, subgridElements = gm.sel_grid(grid_filedir, xlim, ylim, zlim, store=True)

    nnodes = int(dm.str_infile(grid_filedir, 'N='))

    # %% Run function to get states:
    modmeas, Y = getstates(homedir, NoReal, str_assim, nnodes, mode, modeltimes, Y_i_upd='', curtimestep=0,
                                      genfield=genfields, plot=False, mytype=mytype,
                                      initial_head=initial_head,
                                      neg2zero=neg2zero, head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments,
                                      Ini_ensemble=Ini_ensemble, porosity=porosity, storativity=storativity,
                                      dispersivity=dispersivity, subgrid=subgrid, subgridnodes=subgridnodes,
                                      subgridelem=subgridelem, spinup=spinup, hgs=hgs)

    print('Exiting:', Process_current.name)
    # If initialization of ensemble, return only the index
    if Ini_ensemble is True:
        return Str2Assign
    # If not initialization of ensemble, return also model outputs and parameters
    elif Ini_ensemble is False:
        return Str2Assign, modmeas, Y


def list2var(str2pass):
    """
    Change a composite list to several variables using '-' as a symbol to
    separate the strings...
    """
    Str2Assign = str2pass.split('-')[0]
    homedir = str2pass.split('-')[1]
    mode = str2pass.split('-')[2]
    Ini_ensemble = str2bool(str2pass.split('-')[3])
    genfields = str2bool(str2pass.split('-')[4])
    grid_filedir = str2pass.split('-')[5]
    initial_head = str2pass.split('-')[6]
    mytype = str2pass.split('-')[7]
    str_assim = str2pass.split('-')[8]
    neg2zero = str2bool(str2pass.split('-')[9])
    head2dwdn = str2bool(str2pass.split('-')[10])
    cumbtc = str2bool(str2pass.split('-')[11])
    norm = str2bool(str2pass.split('-')[12])
    moments = str2bool(str2pass.split('-')[13])
    porosity = str2bool(str2pass.split('-')[14])
    storativity = str2bool(str2pass.split('-')[15])
    dispersivity = str2bool(str2pass.split('-')[16])
    subgrid = str2bool(str2pass.split('-')[17])
    spinup = str2bool(str2pass.split('-')[18])
    hgs = str2pass.split('-')[19]
    # subgridelem = str2pass.split('-')[18]

    if (initial_head == 'False') or (initial_head == 'None'):
        initial_head = str2bool(initial_head)
    else:
        initial_head = float(initial_head)

    return Str2Assign, homedir, mode, Ini_ensemble, genfields, grid_filedir, initial_head, mytype, \
           str_assim, neg2zero, head2dwdn, cumbtc, norm, moments, porosity, storativity, dispersivity, \
           subgrid, spinup, hgs


def caller_getstates(Y, homedir, str_assim, nnodes, mymode, fieldmeas_len, current_time, thetimes, type_update,
                     parallel=False, cpus='', genfield=False, plot=False, initial_head=7.0,
                     neg2zero=False, head2dwdn=False, cumbtc=False, norm=False, moments=False, OWnode='',
                     porosity=False, storativity=False, dispersivity=False,
                     subgrid=False, subgridnodes=None, subgridelem=None, hgs='2016'):
    """
    Support function to call getstates with the option of doing it in parallel
    Arguments:
        Y:              numpy 2D array, parameter array for all realizations
        homedir:        str, home directory (e.g. kalmanfilter_git)
        mymode:         str, 'fl_ or 'tr_'
        fieldmeas_len:  int, number of field observations (
        str_assim:
        nnodes:
        current_time:
        thetimes:
        type_update:
        parallel:
        cpus:
        genfield:
        plot:
        initial_head:
        neg2zero:
        head2dwdn:
        cumbtc:
        norm:
        moments:
        OWnode:
        porosity:
        storativity:
        dispersivity:
        subgrid:
        subgridnodes:
        subgridelem:
    Returns:

    """
    nel, nreal = Y.shape
    modmeas = np.zeros((fieldmeas_len, nreal))
    if parallel is False:
        for ii in range(0, nreal):
            modmeas_temp, idx_dummy = getstates_parall(ii, Y, homedir, str_assim, nnodes,
                                                       mymode, current_time, thetimes, type_update,
                                                       genfield, plot, initial_head, neg2zero, head2dwdn,
                                                       cumbtc, norm, moments, OWnode, porosity, storativity,
                                                       dispersivity, subgrid, subgridnodes, subgridelem, hgs)
            # if (len(modmeas_temp) != fieldmeas_len) or (len(modmeas_temp) == 0):
            #     modmeas[:, ii] = np.zeros(fieldmeas_len, )
            # else:
            modmeas[:, ii] = modmeas_temp

    elif parallel is True:
        mypool = mp.Pool(cpus)
        modmeas_temp = mypool.starmap(getstates_parall,
                                      zip(np.arange(0, nreal, 1), repeat(Y), repeat(homedir), repeat(str_assim),
                                          repeat(nnodes),
                                          repeat(mymode), repeat(current_time), repeat(thetimes), repeat(type_update),
                                          repeat(genfield), repeat(plot), repeat(initial_head),
                                          repeat(neg2zero), repeat(head2dwdn), repeat(cumbtc), repeat(norm),
                                          repeat(moments), repeat(OWnode), repeat(porosity), repeat(storativity),
                                          repeat(dispersivity), repeat(subgrid), repeat(subgridnodes),
                                          repeat(subgridelem), repeat(hgs)))
        mypool.close()
        mypool.join()

        for ii in range(0, len(modmeas_temp)):
            # if (len(modmeas_temp[ii][0]) != fieldmeas_len) or (len(modmeas_temp[ii][0]) == 0):
            #     modmeas[:, modmeas_temp[ii][-1]] = np.zeros(fieldmeas_len, )
            # else:
            modmeas[:, modmeas_temp[ii][-1]] = np.asarray(modmeas_temp[ii][0])

    return modmeas


def getstates_parall(ii, Y, homedir, str_assim, nnodes, mymode, current_time, thetimes, type_update, genfield, plot,
                     initial_head, neg2zero, head2dwdn, cumbtc, norm, moments, OWnode, porosity, storativity,
                     dispersivity, subgrid, subgridnodes, subgridelem, hgs):
    """ Function that calls getstates for the corresponding member of the ensemble. Prepares the
        cur_process folder, and add updates the time in the grok file.
    Arguments:
        initial_head:
        storebinary:
        plot:
        genfield:
        ii:
        Y:
        homedir:
        mymode:
        current_time:
        thetimes:
        type_update:
        moments:
    Returns:
        modeled observations and index of the current member of the ensemble
    """
    mypath_dict = dm.gendirtree(homedir, str_assim=str_assim, proc_idx=ii + 1, mode=mymode)
    concFile = '%ssim_%.5do.cen_prev' % (mymode, (ii + 1))

    if not os.listdir(mypath_dict['fld_mode']):
        prepfwd(ii, homedir, mymode, current_time, thetimes, str_assim=str_assim,
                type_update=type_update, concFile=concFile)

    # addt2grok(mypath_dict, current_time, thetimes, mode=mymode, mytype=type_update)
    if not isinstance(initial_head, (float, str)):
        initial_head = None  # initial_head[:, ii]

    modmeas, Y = getstates(homedir, ii, str_assim, nnodes, mymode, thetimes, Y_i_upd=np.exp(Y[:, ii]),
                                      curtimestep=current_time,
                                      genfield=genfield, plot=plot, mytype=type_update,
                                      initial_head=initial_head, neg2zero=neg2zero, head2dwdn=head2dwdn, cumbtc=cumbtc,
                                      norm=norm,
                                      moments=moments, OWnode=OWnode, porosity=porosity, storativity=storativity,
                                      dispersivity=dispersivity,
                                      subgrid=subgrid, subgridnodes=subgridnodes,
                                      subgridelem=subgridelem, hgs=hgs)
    # If there are no heads file, store them first individually:
    # if mymode != 'tr_':
    #     if (type_update == 'std') and (os.path.isfile(
    #             os.path.join(mypath_dict['fld_heads'], 'hydheads_%siter001_timestep_001.npy' % mymode)) is False):
    #         np.save(os.path.join(mypath_dict['fld_heads'], headsFile_str), headfield)

    return modmeas, ii


def getstates(homedir, ii, str_assim, nnodes, mode, modeltimes, Y_i_upd='', curtimestep=0, genfield=False, plot=False,
              mytype='restart', initial_head=7.0, neg2zero=False, head2dwdn=False,
              cumbtc=False, norm=False, moments=False, Ini_ensemble=False, OWnode='', porosity=False,
              storativity=False, dispersivity=False, subgrid=False, meshfile='meshtecplot.dat',
              subgridnodes=None, subgridelem=None, spinup=False, hgs='2016'):
    """
    Update states(model measurements) with updated parameters using a forward model
    run for the corresponding realization. Give the parameter values NOT in LOG SCALE!!!
        Arguments:
        ----------
    homedir:
    ii:                 int, process index
    mode:               str, type of model run. 'fl_' or 'tr_'
    Y_i_upd:            np.array, parameter values of the model. Not in log scale!
    genfield:           bool, generate a gaussian field or not
    plot:               bool, plot the generated fields or not
    moments:
        Returns:
        --------
    modmeas:      np.array of updated modeled measurements (or states). Fortran order ([no_meas*no_obsPoints] x 1 )
    """
    # %% Initialize necessary variables and directories:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mode, homedir, str_assim=str_assim, porosity=porosity,
                                                         storativity=storativity, dispersivity=dispersivity)
    randparam = '_RandFieldsParameters.dat'
    Model_data_dir = dir_dict['fld_modeldata']
    cur_processpath = dir_dict['fld_curproc']
    randfile = os.path.join(Model_data_dir, randparam)
    ModObsPath = dir_dict['fld_modobs']
    dest_kkkFolderBinary = dir_dict['fld_kkkfiles']
    curDestDir = dir_dict['fld_mode']
    concFile = '%ssim_%so.cen_prev' % (mode, fmt_string)
    headFile = '%ssim_%so.hen_prev' % (mode, fmt_string)

    if not os.listdir(curDestDir):
        prepfwd(ii, homedir, mode, curtimestep, modeltimes, str_assim=str_assim,
                type_update=mytype, concFile=concFile, porosity=porosity,
                storativity=storativity, dispersivity=dispersivity)

    files2keep = np.array(
        ['.grok', '.pfx', 'kkkGaus', '.mprops', 'letme', 'wprops', 'iniheads', 'parallelindx', 'meshwell', '.png',
         '.lic', '.cen', '.hen'])
    assert os.path.exists(curDestDir), 'Something is wrong in the definition of the tree directory!'
    assert os.path.exists(Model_data_dir), 'Model data directory does not exist or not in proper location!'

    if (subgrid is True) and (subgridelem is None):
        subgridelem = np.load(os.path.join(homedir, mode + 'ToCopy', 'subGrid.elements.npy'))
        subgridnodes = None

    # %% First Step: Generate new kkk files to be used by GROK and HGS in the proper folder
    # ---------------------#
    # Decide whether to create random fields or not:
    # Load random field parameter data from the main file:
    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(
        randfile)
    if genfield is True:

        # Generate the field: returns K in m/s.
        Y = fg.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel, Y_model,
                                Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot, mydir=curDestDir)
        if subgrid is True:
            nelements = int(dm.str_infile(os.path.join(homedir, mode + 'ToCopy', meshfile), 'E='))
            # Treat as a single mean value to the elements outside the subdomain
            if (nxel * nyel * nzel) < nelements:
                Y_temp = np.ones((nelements,)) * Y.mean()
                Y_temp[subgridelem] = Y
                np.savetxt(os.path.join(curDestDir, curKFile),
                           np.transpose((np.arange(1, len(Y_temp) + 1, 1), Y_temp.squeeze(), Y_temp.squeeze(),
                                         Y_temp.squeeze())), fmt='%d %.6e %.6e %.6e')
        if subgrid is False:
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(Y) + 1, 1), Y.squeeze(), Y.squeeze(),
                                     Y.squeeze())), fmt='%d %.6e %.6e %.6e')

    elif genfield is False:
        assert len(Y_i_upd) > 0, 'No parameters were provided!'
        # Save KKK file in the proper process folder:
        if subgrid is False:
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(Y_i_upd) + 1, 1), Y_i_upd.squeeze(), Y_i_upd.squeeze(),
                                     Y_i_upd.squeeze())), fmt='%d %.6e %.6e %.6e')
        elif subgrid is True:
            nelements = int(dm.str_infile(os.path.join(homedir, mode + 'ToCopy', meshfile), 'E='))
            # Treat as a single mean value to the elements outside the subdomain
            if (nxel * nyel * nzel) < nelements:
                Y_temp = np.ones((nelements,)) * np.exp(np.log(Y_i_upd).mean())
                Y_temp[subgridelem] = Y_i_upd.squeeze()
                np.savetxt(os.path.join(curDestDir, curKFile),
                           np.transpose((np.arange(1, len(Y_temp) + 1, 1), Y_temp.squeeze(), Y_temp.squeeze(),
                                         Y_temp.squeeze())), fmt='%d %.6e %.6e %.6e')

    # Read last porosity or storage or dispersivity file and update mprops if necessary:
    if Ini_ensemble is True:
        dummy1, dummy2 = read_extraparam(ii, dir_dict, mode, initialize=True, porosity=porosity,
                                         storativity=storativity,
                                         dispersivity=dispersivity, update_mprops=True)

    if (curtimestep == 0) and (mode == 'tr_'):
        dummy1, dummy2 = read_extraparam(ii, dir_dict, mode, initialize=True, porosity=porosity,
                                         storativity=storativity,
                                         dispersivity=dispersivity, update_mprops=True)

    # %% Second Step: Run the model: GROK and HGS :
    # ---------------------#
    # If spin-up first:
    if (mode == 'fl_') and (spinup is True):
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!IniTransSim\nTransient flow': '!IniTransSim\n!Transient flow',
                       '!IniNoWells\n!skip on': '!IniNoWells\nskip on'})
        print(time.strftime("%d-%m %H:%M:%S",time.localtime(time.time())) + ': Running spin-up for steady-state flow')
        fwdHGS(curDestDir, fmt_string, mytype=mode)
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!IniTransSim\n!Transient flow': '!IniTransSim\nTransient flow',
                       '!IniNoWells\nskip on': '!IniNoWells\n!skip on'})
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!Inihead\nInitial head\n10.0':'!Inihead\n!Initial head\n!10.0'})
        changeStrGrok(dir_dict['fld_mode'], '%ssim_%s.grok' % (mode, fmt_string),
                      {'!restart file for heads\n!fl_simo.hen': 'restart file for heads\n%s' %headFile})

        dm.rmv_rename(os.path.join(dir_dict['fld_mode'], headFile),
                      os.path.join(dir_dict['fld_mode'], 'fl_sim_%so.hen' % fmt_string))
        print(time.strftime("%d-%m %H:%M:%S", time.localtime(time.time())) + ': Finish spin-up for steady-state flow')

    if (mode == 'fl_') and (isinstance(initial_head, (float, str)) is False):
        headfield, dummytime = readHGSbin(os.path.join(dir_dict['fld_mode'], headFile), nnodes, endfile=True)
        OWnode = np.loadtxt(os.path.join(Model_data_dir, '_obswellscoord%s.dat' % mode), usecols=(-1,))
        initial_head = headfield[OWnode.astype('int')-1]

    fwdHGS(curDestDir, fmt_string, mytype=mode)

    # Remove previous .hen_prev file if exists and rename the current .hen file:
    if mode == 'fl_':
        dm.rmv_rename(os.path.join(dir_dict['fld_mode'], headFile),
                      os.path.join(dir_dict['fld_mode'], 'fl_sim_%so.hen' % fmt_string))

    if (mytype == 'std') and (mode == 'tr_'):
        dm.rmv_rename(os.path.join(dir_dict['fld_mode'], concFile),
                      os.path.join(dir_dict['fld_mode'], 'tr_sim_%so.hen' % fmt_string))

    # %% Third step: Read new model outputs at specified output times, store them as bin files.
    # If running flow, extract in addition final heads:
    # ---------------------#
    if 'std' in mytype:
        mytimes = modeltimes[curtimestep][np.newaxis]
    elif 'restart' in mytype:
        mytimes = modeltimes[0:curtimestep + 1]
    elif 'full' in mytype:
        mytimes = modeltimes
    all_modout = getmodout(curDestDir, mode=mode, outputtime=mytimes, initial_head=initial_head,
                           neg2zero=neg2zero, head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments,
                           curtimestep=curtimestep, curreal=Noreal, dirtree=dir_dict, hgs=hgs)

    # %% Fourth: save stuff in the corresponding directories
    # Just if Ini_ensemble is True, if not return the values for concatenating in a single array and binary file:
    # ---------------------#
    if Ini_ensemble is True:
        np.save(os.path.join(ModObsPath, 'ini_mod%s%s' % (mode, fmt_string)), all_modout.transpose())
        np.save(os.path.abspath(os.path.join(dest_kkkFolderBinary, 'ini_%s' % curKFile.split('.')[0])), np.transpose(Y))

    # Fifth: Remove unnecessary files:
    # ---------------------#
    dummypaths = dm.rmfiles(files2keep, mode + fmt_string, cur_processpath)
    del dummypaths

    # Sixth: Reorder model outputs (states) prior to return them, using Fortran convention:
    # ---------------------#
    try:
        modmeas = all_modout.flatten('F')  # Fortran convention is always used in this step
    except:
        raise Exception('Something is wrong with the model outputs, check e.g. cur time step in grok!')

    return modmeas, Y_i_upd


def prepfwd(ii, homedir, mymode, cur_timestep, thetimes, headsFile='', str_assim='', type_update='', concFile='',
            porosity=False, storativity=False, dispersivity=False):
    """
    Prepare folder for forward model run. Meaning: copy the source folder with
    all its files, into a folder that includes process number id (number of realization)
        Arguments:
        ----------
    ii:             int, process id
    homedir:        str, home directory of the whole project
    mymode:         str, fl_ or tr_, defines flow or transport
    headsFile:      str, name of the final(from heads) or initial(for transport) heads
        Returns:
        --------
    Creates corresponding directories for a forward model run
    """

    # %% Create process folder if does not exist:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mymode, homedir, str_assim=str_assim,
                                                         porosity=porosity,
                                                         storativity=storativity, dispersivity=dispersivity)
    SourceDir = os.path.abspath(os.path.join(homedir, mymode + 'ToCopy'))
    # Copy the necessary files from ...ToCopy folder with updated filename:
    grokfile = cpSrcfiles(SourceDir, dir_dict['fld_mode'], mymode, fmt_string)

    # Replace necessary stuff in grok file:
    if type_update != 'std':  # Dictionary of files which name is to be updated:
        StrFiles2Replace = {'kkkGaus.dat': curKFile,
                            'Mesh to tecplot': '!Mesh to no tecplot',
                            'meshtecplot.dat': '!meshtecplot no .dat'}
    else:
        StrFiles2Replace = {'kkkGaus.dat': curKFile,
                            'tr_simo.cen': concFile,
                            'Mesh to tecplot': '!Mesh to no tecplot',
                            'meshtecplot.dat': '!meshtecplot no .dat'}

    changeStrGrok(os.path.join(dir_dict['fld_mode']), grokfile, StrFiles2Replace)

    if (type_update == 'std') and (cur_timestep != 0) and (mymode == 'tr_'):
        addstr2grok(os.path.join(dir_dict['fld_mode'], grokfile), startstr='!---Injection point',
                    endstr='!---End injection point', str2add=[''])
        changeStrGrok(os.path.join(dir_dict['fld_mode']), grokfile,
                      {'!restart file for concentrations': 'restart file for concentrations',
                       '!%s' % concFile: concFile})

    if (type_update == 'std') and (cur_timestep != 0) and (mymode == 'fl_'):
        add_head2grok(os.path.join(dir_dict['fld_mode'], grokfile), headsFile)
        addstr2grok(os.path.join(dir_dict['fld_mode'], grokfile), startstr='!Inihead',
                endstr='!End Inihead', str2add=['!Initial head\n!10.0\n'])

    addt2grok(dir_dict, cur_timestep, thetimes, mode=mymode, mytype=type_update)

    return Noreal, dir_dict, fmt_string, curKFile


def read_extraparam(ii, dir_dict, mymode, initialize=False, porosity=False, storativity=False, dispersivity=False,
                    update_mprops=False):
    mpropsFile = os.path.join(dir_dict['fld_mode'], 'lauswiesen.mprops')
    Allparameters = []
    porosity_idx = 0
    storage_idx = 0
    dispersivity_idx = 0

    if initialize is True:
        if porosity is True:
            porosityFile = os.path.join(dir_dict['fld_porosity'], 'porosity_%siter000_timestep_000.npy' % mymode)
        if storativity is True:
            storativityFile = os.path.join(dir_dict['fld_storativity'],
                                           'storativity_%siter000_timestep_000.npy' % mymode)
        if dispersivity is True:
            dispersivityFile = os.path.join(dir_dict['fld_dispersivity'],
                                            'dispersivity_%siter000_timestep_000.npy' % mymode)

    elif initialize is False:
        if porosity is True:
            porosityFile = dm.getdirs(dir_dict['fld_porosity'], mystr='porosity_%s' % mymode, fullpath=True,
                                      onlylast=True)
        if storativity is True:
            storativityFile = dm.getdirs(dir_dict['fld_storativity'], mystr='storativity_%s' % mymode, fullpath=True,
                                         onlylast=True)
        if dispersivity is True:
            dispersivityFile = dm.getdirs(dir_dict['fld_dispersivity'], mystr='dispersivity_%s' % mymode,
                                          fullpath=True, onlylast=True)

    if porosity is True:
        porosity_idx = 3
        porosity_param = np.load(porosityFile)
        Allparameters.append(porosity_param)
        if update_mprops is True:
            update_mpropsFile('porosity', porosity_param[:, ii], mpropsFile)

    if storativity is True:
        storage_idx = 1
        storativity_param = np.load(storativityFile)
        Allparameters.append(storativity_param)
        if update_mprops is True:
            update_mpropsFile('storage', storativity_param[0, ii], mpropsFile)

    if dispersivity is True:
        dispersivity_idx = 1
        dispersivity_param = np.load(dispersivityFile)
        Allparameters.append(dispersivity_param)
        if update_mprops is True:
            update_mpropsFile('dispersivity', dispersivity_param[0, ii], mpropsFile)

            # Tot parameters,
    return Allparameters, [porosity_idx + storage_idx + dispersivity_idx, porosity_idx, storage_idx, dispersivity_idx]


def update_mpropsFile(param_name, param_values, mpropsFile):
    try:  # Check if it a single value, if not then do not transform to float
        param_values = float(param_values)
    except TypeError:
        pass

    if param_name == 'porosity':
        addstr2grok(mpropsFile, startstr='!mobile', endstr='!end mobpor',
                    str2add=['porosity', '%s' % float(param_values[0])])
        addstr2grok(mpropsFile, startstr='immobile zone porosity', endstr='!end immobpor',
                    str2add=['%s' % float(param_values[1])])
        addstr2grok(mpropsFile, startstr='immobile zone mass transfer coefficient ', endstr='!end transfer',
                    str2add=['%s' % float(param_values[2])])
    if param_name == 'storage':
        addstr2grok(mpropsFile, startstr='specific storage', endstr='!end storativity',
                    str2add=['%s' % param_values])
    if param_name == 'dispersivity':
        addstr2grok(mpropsFile, startstr='longitudinal dispersivity', endstr='!end long disp',
                    str2add=['%s' % param_values])
        addstr2grok(mpropsFile, startstr='!transverse', endstr='!end trans disp',
                    str2add=['transverse dispersivity', '%s' % (param_values / 10.0)])
        addstr2grok(mpropsFile, startstr='vertical transverse dispersivity', endstr='!end vtrans disp',
                    str2add=['%s' % (param_values / 100.0)])


def packXtra_params(param_name, mydir, nreal, cur_iter=0, cur_time=0, mymode='fl_'):
    """ Pack single parameter values previously stored in individual files, into a single file
        Args:
    param_name:
    mydir:
    nreal:
    cur_iter:
    cur_time:
    mymode:
        Returns:
    """
    if param_name == 'porosity':
        temp_string = 'porositytemp'
        filename = 'porosity_%siter%.3d_timestep_%.3d.npy' % (mymode, cur_iter, cur_time)
        upd_params = np.zeros((3, nreal))
    elif param_name == 'storativity':
        temp_string = 'storativitytemp'
        filename = 'storativity_%siter%.3d_timestep_%.3d.npy' % (mymode, cur_iter, cur_time)
        upd_params = np.zeros((1, nreal))
    elif param_name == 'dispersivity':
        temp_string = 'dispersivitytemp'
        filename = 'dispersivity_%siter%.3d_timestep_%.3d.npy' % (mymode, cur_iter, cur_time)
        upd_params = np.zeros((1, nreal))
    st_file = dm.getdirs(mydir, mystr=temp_string, fullpath=True, onlylast=False)

    for ww in range(0, len(st_file)):
        try:
            upd_params[:, ww] = np.load(st_file[ww])
        except ValueError:
            upd_params[:, ww] = np.load(st_file[ww]).squeeze()
        os.remove(st_file[ww])

    np.save(os.path.join(mydir, filename), upd_params)

    print('File < %s > created' % filename)


def cpSrcfiles(SourceDir, DestDir, mymode, fmt_string, hsplot=False):
    """
    Support for the <prepfwd>, this is the function that does copy the folder. It copies all
    dependencies, adds an identifier to each file, creates a batch file with proper prefix
    to make GROK run automatically, and replace corresponding file-names within GROK file
        Arguments:
        ----------
    SourceDir:          str, folder to be copied (full path)
    DestDir:            str, destination directory
    mymode:             str, fl_ or tr_, defines flow or transport
    fmt_string:         str, Identifier to assign to each new copied folder
    StrFiles2Replace:   dict, files to be replaced within GROK (eg.
                        {'kkk_file.dat': cur_kkkFile,'InitialHeads.head_pm.001': 'iniHeads_%s.head_pm'}) Not full path
    hsplot:             bool, default is False: not include hsplot in the batch file sequence
        Returns:
        --------
    Creates a brand new folder with the necessary files to make a forward HGS run using a
    current KKK field
    """
    # %% Copy directories and files:
    Ignorefiles = ('mesh*', '*.npy')
    try:
        shutil.copytree(SourceDir, DestDir, ignore=shutil.ignore_patterns(Ignorefiles))
    except:
        myfiles = os.listdir(SourceDir)
        if 'meshtecplot.dat' in myfiles: myfiles.remove('meshtecplot.dat')
        myfiles = [x for x in myfiles if '.npy' not in x]
        for item in myfiles:
            if 'groks' in os.path.join(DestDir, item):
                pass
            else:
                try:
                    shutil.copy2(os.path.join(SourceDir, item), os.path.join(DestDir, item))
                except:
                    pass

    # %% Rename grok file if necessary:
    new_str = mymode + 'sim' + ('_%s.grok' % fmt_string)
    oldGrok = '%ssim.grok' % mymode

    newFileslist = os.listdir(DestDir)
    if new_str in newFileslist:
        os.remove(os.path.join(DestDir, oldGrok))
        newgrok = next(x for x in newFileslist if x.endswith('.grok'))
        print('No need to rename << %s >>, nor corresponding batch files' % newgrok)

    if (oldGrok in newFileslist) and (new_str not in newFileslist):
        # grok_loc = newFileslist.index(oldGrok)
        os.rename(os.path.join(DestDir, oldGrok), os.path.join(DestDir, new_str))
        # os.remove(os.path.join(DestDir, oldGrok))

        # %% Create the prefix and "let me run " batch files with the proper string:
    CreatePrefixBatch(DestDir, new_str.split('.')[0])
    Createletmerun(DestDir, new_str.split('.')[0], hsplot=hsplot)

    return new_str


def changeStrGrok(DestDir, grokfile, StrFiles2Replace):
    In_grokFile = os.path.join(DestDir, grokfile)
    tempGrok = '%s_temp' % In_grokFile

    InFileObj = open(In_grokFile)
    InFile = InFileObj.read()
    OutFile = open(tempGrok, 'w')
    for i in list(StrFiles2Replace.keys()):
        InFile = InFile.replace(i, StrFiles2Replace[i])

    OutFile.write(InFile)
    OutFile.close()
    InFileObj.close()
    os.remove(In_grokFile)
    os.rename(tempGrok, In_grokFile)


def addstr2grok(grokfile, startstr='', endstr='', str2add=[]):
    """ Add a string or a list of strings to an existent grok file
        Arguments:
            startstr:
            endstr:
            str2add:
        :return:
        """

    tempgrokfile = '%s_temp' % grokfile
    oldstr_idx = dm.str_infile(grokfile, startstr, index=True)  # todo: reference properly this function

    if oldstr_idx is None:
        print('String < %s > not found in <%s>' % (startstr, os.path.split(grokfile)[-1]))
    elif oldstr_idx is not None:
        writing = True
        with open(grokfile) as f:
            with open(tempgrokfile, 'w') as out:
                for line in f:
                    if writing:
                        if startstr in line:
                            writing = False
                            out.write(startstr)
                            for ii in range(0, len(str2add)):
                                out.write('\n%s' % (str2add[ii]))
                            buffer = [line]
                            out.write("\n%s\n" % endstr)
                        else:
                            out.write(line)
                    elif endstr in line:
                        writing = True
                    else:
                        buffer.append(line)
                else:
                    if not writing:
                        # There wasn't a closing "event", so write buffer contents
                        out.writelines(buffer)
                out.close()
                f.close()
        os.remove(grokfile)
        os.rename(tempgrokfile, grokfile)
        # print('Given strings added to < %s > ' % grokfile)
        str_grokfile = os.path.join(os.path.split(os.path.split(grokfile)[0])[-1], os.path.split(grokfile)[-1])
        print('Strings: %s added to < %s >' % (str2add, str_grokfile))


def prepareGROK(SourceDir, DestDir, mymode, fmt_string, StrFiles2Replace, hsplot=False):
    """
    Support for the <prepfwd>, this is the function that does copy the folder. It copies all
    dependencies, adds an identifier to each file, creates a batch file with proper prefix
    to make GROK run automatically, and replace corresponding file-names within GROK file
        Arguments:
        ----------
    SourceDir:          str, folder to be copied (full path)
    DestDir:            str, destination directory
    mymode:             str, fl_ or tr_, defines flow or transport
    fmt_string:         str, Identifier to assign to each new copied folder
    StrFiles2Replace:   dict, files to be replaced within GROK (eg.
                        {'kkk_file.dat': cur_kkkFile,'InitialHeads.head_pm.001': 'iniHeads_%s.head_pm'}) Not full path
    hsplot:             bool, default is False: not include hsplot in the batch file sequence
        Returns:
        --------
    Creates a brand new folder with the necessary files to make a forward HGS run using a
    current KKK field
    """
    # %% Copy directories and files:
    Ignorefiles = 'mesh*'
    try:
        shutil.copytree(SourceDir, DestDir, ignore=shutil.ignore_patterns(Ignorefiles))
    except:
        myfiles = os.listdir(SourceDir)
        if 'meshtecplot.dat' in myfiles: myfiles.remove('meshtecplot.dat')
        for item in myfiles:
            if 'groks' in os.path.join(DestDir, item):
                pass
            else:
                try:
                    shutil.copy2(os.path.join(SourceDir, item), os.path.join(DestDir, item))
                except:
                    pass

    # %% Create a new GROk file with the KKK file updated:
    new_str = mymode + 'sim' + ('_%s.grok' % fmt_string)
    Out_grokFile = os.path.join(DestDir, new_str)

    if not os.path.isfile(Out_grokFile):

        In_grokFile = os.path.join(DestDir, mymode + 'sim' + '.grok')

        # %% Copy heads file if working with transport:
        # No!!! This will be done in the get states function, here just the name
        # is replaced in the GROK file

        InFileObj = open(In_grokFile)
        InFile = InFileObj.read()
        OutFile = open(Out_grokFile, 'w')
        for i in list(StrFiles2Replace.keys()):
            InFile = InFile.replace(i, StrFiles2Replace[i])

        OutFile.write(InFile)
        OutFile.close()
        InFileObj.close()
        os.remove(In_grokFile)

        # %% Create the prefix and "let me run " batch files with the proper string:
        CreatePrefixBatch(DestDir, new_str.split('.')[0])
        Createletmerun(DestDir, new_str.split('.')[0], hsplot=hsplot)


def fwdHGS(mydir, mystr, mytype='', hsplot=False):
    """
    Run GROK and HGS and if stated, run also hsplot
        Arguments:
        ----------
    mydir:          str, full path directory where GROK and HGS should be run
    mystr:          str, flag to print no. of realization in the cmd
    mytype:         str, flag to print flow or transport identifier in the cmd
    hsplot:         bool, (optional) if false, hspot is not executed
    """
    print('Process No. %s: running GROK (%s)... ' % (mystr, mytype))
    if platform.system() == 'Windows':
        qq = sp.Popen('grok', shell=True, cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    else:
        qq = sp.Popen('grok.x', shell=True, cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq, error_qq = qq.communicate()
    if 'Normal exit' not in str(output_qq):
        myfile = open(os.path.join(mydir, 'grokOutput.txt'), 'wb')
        myfile.write(output_qq)
        myfile.close()
        sys.exit(
            'Something went wrong when executing GROK in process %s(%s)... Check grokOutput.txt file' % (mystr, mytype))

    print('Process No. %s: running PHGS (%s)... ' % (mystr, mytype))
    if platform.system() == 'Windows':
        qq_2 = sp.Popen('phgs', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    else:
        qq_2 = sp.Popen('hgs.x', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq_2, error_qq_2 = qq_2.communicate()
    if 'NORMAL EXIT' not in str(output_qq_2):
        myfile = open(os.path.join(mydir, 'hgsOutput.txt'), 'wb')
        myfile.write(output_qq_2)
        myfile.close()
        if ('No more mass' in str(output_qq_2)) or ('Warning -1' in str(output_qq_2)):
            print('No more mass stored in the system: Process No. << %s: %s >> ... ' % (mystr, mytype))
            print('Check hgsOutput.txt file')
        else:
            sys.exit('Something went wrong when executing HGS in process %s(%s)... Check hgsOutput.txt file' % (
                mystr, mytype))

    if hsplot is True:
        print('Process No. %s: running HSPLOT(%s)... ' % (mystr, mytype))
        if platform.system() == 'Windows':
            qq_3 = sp.Popen('hsplot', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
        else:
            qq_3 = sp.Popen('hsplot.x', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
        output_qq_3, error_qq_3 = qq_3.communicate()
        if platform.system() == 'Windows':
            qq_3.kill()
    if platform.system() == 'Windows':
        qq.kill()
        qq_2.kill()
    print('Successful fwd model run, Process No. %s (%s)' % (mystr, mytype))


def fwdHGS_batch_caller(mydir, batchfile, mymode='fl_', updateGrok=False, newtimes='', parallel=False, cpus=0,
                        remove=False, files2keep='', linux=False, runbatch=False):
    """ Run batch load of HGS models. e.g. run all models within the ensemble: allproc
    Args:
        mydir:          str, directory containing all folders with models
        batchfile:      str, name of the common batchfile that runs GROK,PHGS and HSPLOT
        mymode:         str, identifier to discriminate between type of models
        updateGrok:     bool, update times in grok or not
        newtimes:       array, new times to add to grok if updateGrok is True
        files2keep:
        remove:
    Returns:
        Exit banner if all runs were successfully
    """

    procfolderlist = os.listdir(mydir)

    if parallel is True:
        # Create the pool if in parallel:
        mypool = mp.Pool(cpus)
        mypool.starmap(fwdHGS_batch, zip(procfolderlist, repeat(mymode), repeat(mydir), repeat(updateGrok),
                                         repeat(newtimes), repeat(batchfile), repeat(remove),
                                         repeat(files2keep), repeat(linux), repeat(runbatch)))
        mypool.close()
        mypool.join()

    if parallel is False:
        for idx, cur_procfolder in enumerate(procfolderlist):
            fwdHGS_batch(cur_procfolder, mymode, mydir, updateGrok, newtimes, batchfile, remove, files2keep, linux,
                         runbatch)


def fwdHGS_batch(procfolder, mymode, mydir, updateGrok, newtimes, batchfile, remove, files2keep, linux, runbatch):
    """
    Function designed to run HGS from a batchfile (e.g. "letmerun"). It is designed to be able to run in parallel and
    the directory structure for the EnKF...
    Args:
        procfolder:     str, folder containing all processes of the ensemble
        mymode:         str, 'fl_' for flow model or 'tr_' for transport
        mydir:          str, main directory (one level above procfolder)
        updateGrok:     bool, if it is wanted to update output times in each grok file
        newtimes:       np.array, with the output times
        batchfile:      str, name of the batch file to run the HGS model
        files2keep:
        remove:
    Returns:

    """

    modelfolder = os.listdir(os.path.join(mydir, procfolder))

    for idy, cur_modelfolder in enumerate(modelfolder):

        if mymode in cur_modelfolder:
            cur_dir = os.path.join(mydir, procfolder, cur_modelfolder)

            if updateGrok is True:
                mydict = {'fld_mode': cur_dir}
                addt2grok(mydict, None, newtimes, mode=mymode, mytype='full')
                print('Working with directory << %s >>' % os.path.split(mydir)[0])
            if runbatch is True:
                print('Running model %s. Executing batchfile %s' % (cur_modelfolder, batchfile))

            if linux is False:
                changeStrGrok(cur_dir, batchfile, {'grok.x': 'grok',
                                                   'hgs.x': 'phgs'})
            elif linux is True:
                changeStrGrok(cur_dir, batchfile, {'grok': 'grok.x', 'phgs': 'hgs.x'})

            if runbatch is False:
                fwdHGS(cur_dir, cur_modelfolder, mytype=mymode, hsplot=False)
            elif runbatch is True:
                qq = sp.Popen(batchfile, shell=True, cwd=cur_dir, stdout=sp.PIPE, stderr=sp.PIPE)
                output_qq, error_qq = qq.communicate()
                if 'Normal exit' not in str(output_qq):
                    myfile = open(os.path.join(cur_dir, '%s.txt' % batchfile), 'wb')
                    myfile.write(output_qq)
                    myfile.close()
                    sys.exit('Something went wrong when executing %s in %s... Check %s.txt file' % (
                        batchfile, cur_modelfolder, batchfile))

        if remove is True:
            dummypaths = dm.rmfiles(files2keep, cur_modelfolder, os.path.join(mydir, procfolder))
            # dummypaths = dm.rmfiles(files2keep, '%s%.5d' % (mymode, zz + 1), os.path.join(allprocDir, curprocess))
            print('Removing files from folder < %s >' % cur_modelfolder)
            del dummypaths


def getmodout(mypath, mode='', outputtime='', initial_head=7.0, neg2zero=False, head2dwdn=False, cumbtc=False,
              norm=False, moments=False, curtimestep=None, curreal=None, dirtree=None, ref=False, hgs='2018'):
    """
    Read model output from a file at specific output times.
    Time values will be integers. Corrects negative concentrations when
    working in transport mode.
        Arguments:
        ----------
    mypath:         str, directory where model outputs are located
    mode:           Str, 'fl_' or 'tr_'
    outputtime:     output times of interest
        Returns:
        --------
    all_modout:   2D np array, observed values at defined times
                   size of the array = [No times x No Obs Points]. Fortran order
    """
    # assert outputtime != '', 'No output times provided'
    if not mode:
        print('Model outputs not extracted. No running mode provided')
    elif mode == 'fl_':
        mystrs = 'well_flow'
    elif mode == 'tr_':
        mystrs = 'well_conc'

    # Get a list of the files of all observation points:
    files = dm.getdirs(mypath, mystr=mystrs, fullpath=True, onlylast=False)

    # Start collecting them in a single 2D array
    all_modout = np.array([])

    for idx, cur_file in enumerate(files):
        selMeas = rmodout(cur_file, outputtime, mode, hgs=hgs)

        if not isinstance(initial_head, (float, str)):
            local_inihead = initial_head[idx]
        else:
            local_inihead = initial_head

        transfMeas = procmodout(selMeas, outputtime, ini_head=local_inihead, neg2zero=neg2zero,
                                head2dwdn=head2dwdn, cum=cumbtc, norm=norm, moments=moments,
                                curtimestep=curtimestep, curreal=curreal, mymode=mode, obsid=idx, dirtree=dirtree,
                                ref=ref)
        # selMeas = rmodout(ii, outputtime.astype(np.int))
        all_modout = np.append(all_modout, transfMeas)  # All selected heads

    all_modout = np.reshape(all_modout, (-1, len(files)), order='F')

    return all_modout


def read_save_ow_coord(listfiles, savefile):
    """ Read and save observation file coordinates and node number (all related to the model grid)
    Args:
        listfiles:     list, all files to be used to extract X,Y,Z, node. Provide full path for each
        savefile:       str, full path directory to store the observation well coordinates
    Returns:
        creates an ascii "savefile" with X,Y,Z, NoNode columns
    """
    ow_all = np.empty((len(listfiles), 4), dtype=float)
    ow_name_all = np.empty((len(listfiles),), dtype='|S15')

    for ow_id, curfile in enumerate(listfiles):

        assert os.path.isfile(curfile) is True, '<< %s >> is not a file. Check the list'

        if len(os.path.split(curfile)[-1].split('.')) == 5:  # for the extra name when running transport
            ow_name_all[ow_id] = os.path.split(curfile)[-1].split('.')[-3]
        else:
            ow_name_all[ow_id] = os.path.split(curfile)[-1].split('.')[-2]

        try:
            ow_all[ow_id, :] = np.genfromtxt(curfile, skip_header=3, usecols=(4,5,6,7), max_rows=1)
        except ValueError:
            try:
                ow_all[ow_id, :] = np.genfromtxt(curfile, skip_header=3, usecols=(3,4,5,6), max_rows=1)
            except:
                ow_all[ow_id, :] = np.genfromtxt(curfile, skip_header=3, usecols=(2,3,4,5), max_rows=1)

    joined_set = np.c_[ow_name_all.astype('str'), ow_all]
    np.savetxt(savefile, joined_set, fmt='%s', header='name, Xcoo, Ycoo, Zcoo, Node')

    return joined_set

def readHGSbin(myfile, nnodes, endfile=False):
    """
    Read a HGS output bin file. The function has been tested with
    subsurface hydraulic heads and overland heads.
        Arguments:
        ----------
    myfile:     str, filename *full path(
    nnodes:     int, number of nodes in the mesh
        Returns:
        --------
    n_param:    np.array with the nodal parameter values
    time:       float, model time that corresponds to the param field values
    """
    with open(myfile, "rb") as f:
        if endfile is False:
            pad = np.fromfile(f, dtype='uint8', count=4)
            mytime = np.fromfile(f, dtype='uint8', count=80)

            tt = [''.join(chr(t)) for t in mytime]
            mytime = float(''.join(tt))

            pad = np.fromfile(f, dtype='uint8', count=4)
            pad = np.fromfile(f, dtype='uint8', count=4)
            
            if 'sat_pm' in myfile:
                param = np.fromfile(f, dtype='f', count=nnodes)
            else:
                param = np.fromfile(f, dtype='float64', count=nnodes)
            
            pad = np.fromfile(f, dtype='uint8', count=4)

        if endfile is True:
            pad = np.fromfile(f, dtype='uint8', count=4)
            param = np.fromfile(f, dtype='float64', count=nnodes)
            mytime=None

    return param, mytime


def rdparam_bin(myPath, cmstr=''):
    """ Read parameter fields from binary files. Binary file names should contain a specific string identifier
    to constrain the file list.
    Arguments:
        myPath:     Str, directory where the information is stored
        cmstr:      Str, identifier for the files to work with
    Returns:
        nel:        Integer, number of elements of the model grid
        nreal:      Integer, number of realizations in the ensemble
        Y:          Numpy array, 2D Array with the proper data
    """
    # myFiles = os.listdir(myPath)
    myFiles = [x for x in os.listdir(myPath) if cmstr in x]
    myFiles.sort()

    for idx, curkkk in enumerate(myFiles):
        if idx == 0:
            kkkmat = np.load(os.path.join(myPath, curkkk))

        elif idx > 0:
            kkk = np.load(os.path.join(myPath, curkkk))
            kkkmat = np.c_[kkkmat, kkk]  # Parameter field matrix (nel,nreal)

    nel, nreal = kkkmat.shape  # Number of elements in the domain
    return nel, nreal, kkkmat


def rdmodelout_bin(myPath, mymode='fl_'):
    """ Read Modeled outputs from binary files. Either heads or concentrations.
    Arguments:
        myPath:             Str, directory where the information is stored
        mymode:             str, type of data to read
    Returns:
        num_modout:              Integer, number of model outputs
        modoutput:       Numpy array, modeled measurement values (ndata x nrealizations)

    Note: The matrices of measurements are flattened following Fortran
        convention (column-wise or column major). If work with both types of
        measurements, heads are listed first and then concentrations
    """
    myFiles = os.listdir(myPath)
    myFiles.sort()
    basket = 0
    for curid, kk in enumerate(myFiles):

        if mymode in kk:
            cur_file = os.path.join(myPath, myFiles[curid])

            if basket == 0:
                modoutput = np.load(cur_file).flatten('F')

            elif basket > 0:
                cur_modout = np.load(cur_file).flatten('F')
                modoutput = np.c_[modoutput, cur_modout]
            basket += 1

    num_modout = modoutput.shape[0]  # Number of observations

    return num_modout, modoutput


def addt2grok(mypath_dict, cur_timestep, modeltimes, mode='fl_', mytype='restart'):
    """ Add/subtract new output times to GROK. The times are taken from the
    '_outputtimesFlowUpdate.dat' and/or '_outputtimesTransUpdate.dat'
    Arguments:
        mypath_dict:
        cur_timestep:
        modeltimes:
        mode:               'fl_'
        mytype:             'restart' cumulative
    Returns:
        GROK file with updated time step
    """
    # %% Directories and file names:

    mygrokfile = dm.getdirs(mypath_dict['fld_mode'], mystr='%ssim_' % mode, fullpath=True)
    mygrokfile = mygrokfile[0]
    tempgrokfile = mygrokfile + 'temp'

    # %% Working with the time steps to modify in grok: # todo: THis is how to control time steps in different filter modes!!!!
    if 'std' in mytype:
        mytimes = modeltimes[int(cur_timestep)][np.newaxis]
    elif 'restart' in mytype:
        mytimes = modeltimes[0:int(cur_timestep + 1)]
    elif 'full' in mytype:
        mytimes = modeltimes
    # if ('restart' in mytype) or ('std' in mytype):
    #     mytimes = modeltimes[int(cur_timestep)][np.newaxis]
    # elif 'cum' in mytype:
    #     mytimes = modeltimes[0:int(cur_timestep + 1)]
    # elif 'full' in mytype:
    #     mytimes = modeltimes

    writing = True
    with open(mygrokfile) as f:

        with open(tempgrokfile, 'w') as out:
            for line in f:
                if writing:
                    if "output times" in line:
                        writing = False
                        out.write("output times")
                        for ii in range(0, len(mytimes)):
                            out.write('\n%s' % (mytimes[ii]))
                        buffer = [line]
                        out.write("\nend\n")
                    else:
                        out.write(line)
                elif "end" in line:
                    writing = True
                else:
                    buffer.append(line)
            else:
                if not writing:
                    # There wasn't a closing "event", so write buffer contents
                    out.writelines(buffer)
            out.close()
            f.close()
    os.remove(mygrokfile)
    os.rename(tempgrokfile, mygrokfile)

    print('grok file < %s > updated...' % (os.path.split(str(mygrokfile))[-1]))


def add_head2grok(grokfile, headsFile1):
    """

    Args:
        grokfile: full path to the grok file of interest

    Returns:

    """
    newgrok = os.path.join('%stemp' % grokfile)

    h_str = dm.str_infile(grokfile, '!restart file for heads', index=True)

    if h_str:
        replace = {'!restart file for heads': 'restart file for heads'}
        # replace = {'Initial head from file': 'Initial head from file'}
        InFileObj = open(grokfile)
        InFile = InFileObj.read()
        for i in list(replace.keys()):
            InFile = InFile.replace(i, replace[i])
        OutFile = open(newgrok, 'w')
        OutFile.write(InFile)
        OutFile.close()
        InFileObj.close()
        os.remove(grokfile)
        OutFile = open(newgrok)
        strfound = dm.str_infile(newgrok, list([replace.values()][0])[0], index=True)
        fp = open(grokfile, 'w')
        for i, line in enumerate(OutFile):
            if (i - 1) == strfound[0]:
                fp.write(headsFile1 + '\n')
            else:
                fp.write(line)
        fp.close()
        OutFile.close()
        os.remove(newgrok)
    else:
        print('< Initial head > command was already updated...')  # todo: improve this print statement


def run_refmodel(mainPath, curKFile, Flowdir, modelDataFolder, mymodel, mymode='fl_', runmodel=True, std_dev=0,
                 ini_head=0, neg2zero=False, head2dwdn=False, cumbtc=False, norm=False, moments=False,
                 irregularGrid=False,
                 ref=False, gridformat = 'tp2018'):
    """
    Perform all sequence to obtain a synthetic model with heterogeneous fields and extracting model outputs. It is
    possible to add measurement noise to model outputs.
        Args:
    mainPath:   str, directory where the model sub-directory is located, eg ../myModels
    curKFile:   str, filename containing K parameters, eg kkkGaus.dat
    Flowdir:    str, folder name where flow synthetic model is stored, eg _fl_26062015_synthetic
    modelDataFolder: str, folder name containing model input data, eg ModelData_26062015_synthetic
    mymodel:    str, folder of the model that is wished to run eg _fl_26062015_synthetic or tr_...
    mymode:     str, 'fl_':flow , 'tr_':transport. Default fl_
    runmodel:   bool, whether it is required to run the HGS model. Default is True
    std_dev:    float, measurement noise std deviation added to model outputs. Default 0 (no noise)
    ini_head:   float, initial head value in case of running flow model. Default 0

        Returns:    All output generated from GROK, PHGS and HSPLOT, and two arrays with model outputs and model times
    Input example:
        >>> mainPath = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\myModels'
        >>> curKFile = 'kkkGaus.dat'
        >>> mymode = 'fl_'  # 'tr_'
        >>> mymodel = '_fl_26062015_synthetic'
        >>> Flowdir = '_fl_26062015_synthetic'
        >>> modelDataFolder = 'ModelData_26062015_synthetic'
        >>> std_dev = 3.0e-2  # As an example assumed a std error measurement of 1.0 mm on each measurement 1.5e-3, tr = 2.5e-6
        >>> runmodel = True
        >>> ini_head = 20.0
    """
    import re
    print('You have to have at least the following files ready:')
    print('Grok file, model parameters file, outputtimes, meshtecplot')

    # Create additional directories
    mainPath = os.path.join(mainPath, 'model')
    curDestDir = os.path.join(mainPath, mymodel)
    grid_file = os.path.join(curDestDir, 'meshtecplot.dat')
    myParameterDir = os.path.join(modelDataFolder, '_RandFieldsParameters.dat')
    curFlowdir = os.path.join(mainPath, Flowdir)
    # Read parameters
    xlen, ylen, zlen, nxel, nyel, nzel,\
    xlim, ylim, zlim, Y_model, Y_var,\
    beta_Y, lambda_x, lambda_y, lambda_z = fg.readParam(myParameterDir)

    # Read model mesh components
    # Big and small grid:
    # nnodes_big, elements_big = gm.readmesh(grid_file, fulloutput=False)
    nnodes_big, elements_big, nodes_coord, element_nodes, smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_file, xlim,
                                                                                                        ylim, zlim,
                                                                                                        store=True,
                                                                                                          return_val=True,
                                                                                                        fulloutput=True,
                                                                                                        gridformat=gridformat)
    del nodes_coord, element_nodes
    biggrid_elem_ids = np.arange(1, elements_big + 1, 1)
    # smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_file, xlim, ylim, zlim)
    # smallgrid_elem_ids = np.arange(1, nnodes + 1, 1)

    # If K field does not exists, make a random realizationRead parameter field setup:
    if not os.path.isfile(os.path.join(curFlowdir, 'kkkGaus.dat')):
        kkk = fg.genFields_FFT_3d(xlim[1] - xlim[0], ylim[1] - ylim[0], zlim[1] - zlim[0], nxel, nyel, nzel,
                                  Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot=True,
                                  mydir=curDestDir)
        if irregularGrid is True:
            from scipy.interpolate import griddata
            reggrid_oordinatesSorted, element_centroid = gm.interpToirregGrid(grid_file, nxel, nyel, nzel, xlim, ylim,
                                                                              zlim)
            kkk = griddata(reggrid_oordinatesSorted, kkk, element_centroid, method='nearest')
        elif irregularGrid is False:
            kkk = gm.expshr_param(kkk, biggrid_elem_ids, smallgrid_elem, what='expand')

        np.savetxt(os.path.join(curFlowdir, curKFile), np.transpose((np.arange(1, len(kkk) + 1, 1), kkk, kkk, kkk)),
                   fmt='%d %.6e %.6e %.6e')

    if not os.path.isfile(os.path.join(curDestDir, 'kkkGaus.dat')):
        if mymode == 'tr_':
            kkk = np.loadtxt(os.path.join(curFlowdir, 'kkkGaus.dat'), usecols=(1,))
            kkk = kkk[smallgrid_elem - 1]
            np.savetxt(os.path.join(curDestDir, curKFile),
                       np.transpose((np.arange(1, len(kkk) + 1, 1), kkk, kkk, kkk)),
                       fmt='%d %.6e %.6e %.6e')

    # Run the model if required
    if runmodel is True:
        fwdHGS(curDestDir, 'reference', mytype=mymode, hsplot=True)

    # Get the synthetic observations:
    # -----------------------------
    hgs =  re.search('[0-9]{4}',gridformat).group()    # HGS version, eg. 2014
    myouttimes = np.loadtxt(os.path.join(modelDataFolder, '_outputtimes%s.dat' % mymode))
    selObs_All = getmodout(curDestDir, mode=mymode, outputtime=myouttimes, initial_head=ini_head, neg2zero=neg2zero,
                           head2dwdn=head2dwdn, cumbtc=cumbtc, norm=norm, moments=moments, ref=ref, hgs=hgs)

    # Add gaussian measurement noise if std_dev is not zero:
    if std_dev != 0:
        num_modmeas = selObs_All.shape[0]
        meanNoise = np.zeros((num_modmeas,))  # Mean value of white noise equal to zero (num_modmeas x 1)
        R = ssp.dia_matrix(
            (np.multiply(np.eye(num_modmeas, num_modmeas), std_dev ** 2)))  # sparse Matrix (nmeas x nmeas)
        E = np.empty((num_modmeas, selObs_All.shape[1]))
        for ii in range(0, selObs_All.shape[1], 1):
            # E[:,ii] = np.expand_dims(np.random.multivariate_normal(mean,R.toarray()),axis = 1)
            E[:, ii] = np.random.multivariate_normal(meanNoise, R.toarray())

        noisy_data = selObs_All + E
    else:
        noisy_data = selObs_All

    # # For concentrations
    # if mymode is 'tr_':
    #     zero_id = np.where(noisy_data < 0)
    #     noisy_data[zero_id[0], zero_id[1]] = 0

    return myouttimes, noisy_data


def str2bool(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        raise ValueError


def kkk_bin2ascii(infile, outfile, orig_shape, biggrid_elid='', smallgrid_elid='', get_mean=True, exptransf=True,
                  logtransf=False, expand=True):
    """ Read binary files generated during inversion and creates a parameter file (kkk) that HGS can read
    Args:
        infile:
        outfile:
        orig_shape:
        biggrid_elid
        smallgrid_elid
        get_mean:
        exptransf:
        logtransf:
        expand:
    Returns:
    """
    if type(infile) == str:
        myparam = np.load(infile)
    else:
        myparam = infile

    if get_mean is True:
        myparam_mn = myparam.mean(axis=1)  # todo: check if parameters are in log scale or not. I suppose not
        # myparam_mn = np.exp(np.log(myparam).mean(axis=1))
    elif get_mean is False:
        myparam_mn = myparam

    if exptransf is True:
        myparam_mn = np.exp(myparam_mn)
    if logtransf is True:
        myparam_mn = np.log(myparam_mn)

    if expand is True:
        myparam_mn = gm.expshr_param(myparam_mn, biggrid_elid, smallgrid_elid, what='expand')

    assert myparam_mn.shape[0] == orig_shape, 'shape of original and modified parameter arrays do NOT match!'
    np.savetxt(outfile, np.transpose((np.arange(1, len(myparam_mn) + 1, 1), myparam_mn, myparam_mn, myparam_mn)),
               fmt='%d %.18e %.18e %.18e')

    print('File <%s> successfully stored' % outfile)


# Hasta aqui! 09 June 2016


def syncModel2Data(heads, concentrations):
    """ Synchronize the number of model outputs with the number of
    time-steps-updates to perform
    Arguments:
        heads:          Boolean, include head data or not
        concentrations: Boolean, include conc. data or not
    Returns:
        len(cur_heads_time):    Zero if heads == False
        len(cur_conc_time):     Zero if concentration == False
        curTimes:               Number of CURRENT output times dealing with
    Dependencies:
        dm.myMainDir()
    """
    myDir = dm.myMainDir()
    KalmanFilterPath = 'kalmanfilter'

    if heads == True:
        cur_heads_time = np.loadtxt(
            os.path.join(myDir, '..', KalmanFilterPath, 'Model_data', '_outputtimesFlowUpdate.dat'))
        try:
            len(cur_heads_time)
        except:
            cur_heads_time = np.expand_dims(cur_heads_time, 1)

    if concentrations == True:
        cur_conc_time = np.loadtxt(
            os.path.join(myDir, '..', KalmanFilterPath, 'Model_data', '_outputtimesTransUpdate.dat'))
        try:
            len(cur_conc_time)
        except:
            cur_conc_time = np.expand_dims(cur_conc_time, 1)

    # %% Synchronize the number of model outputs with the number
    #  of time-steps-updates to perform:
    try:
        curTimes = len(cur_heads_time) + len(cur_conc_time)
    except:
        if heads == True:
            curTimes = len(cur_heads_time)
        if concentrations == True:
            curTimes = len(cur_conc_time)

    try:
        return len(cur_heads_time), len(cur_conc_time), curTimes
    except:
        if heads == True:
            return len(cur_heads_time), 0, curTimes
        if concentrations == True:
            return 0, len(cur_conc_time), curTimes


def writeinGrok(myInDir, myOutDir, myoutfile, probdescFile, grid_coordFile, gridmethod,
                Transient, Transport, Echo2Output, FD, CV, porousfile, kfile, heads_init, transport_init, heads_bc,
                TimeStepCtrl, well_propsFile, pumpwellsFile, tracer_injFile, outputimesFile, obswellsFile):
    """ Type in the main parts of a grok file
    Arguments:
        myInDir:        Str, directory where input files are located
        myOutDir:       Str, directory where GROK file is/will be located
        myoutfile:      Str, name assigned to the GROK file
        probdescFile:   Str, filename containing description of the project
        grid_coordFile: Str, filename containing grid coordinates
        gridmethod:     Str, method to generate grid-'HGS_uniform' 'HGS_grade' 'GB_file'
        Transient:      Boolean, run transient mode or not
        Transport:      Boolean, run transport mode or not
        Echo2Output:    Boolean, create echo output file or not
        FD:             Boolean, finite difference mode or not
        CV:             Boolean, control volume mode or not
        porousfile:     Str, porous media properties file
        kfile:          Str, KKK filename for homogeneous fields, if homogeneous type ''
        heads_init:     Tuple [mode, arg]:
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number
                        mode: 'raster', arg: string file name
                        mode:'txt', arg: string file name
        transport_init: Tuple [mode, arg]
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number.
                        If transport is False, just type ''
        heads_bc:       tuple [X1, X2], X corrdinate of the X plane to be selected to assign constant head equals to initial value.
                        tuple ['all', 'head equals initial']: all heads stay the same
                        (In the future should support Y and Z planes and possibility
                        to assign boundary conditions from GB files)
        TimeStepCtrl:   Boolean, et the default time step controlers or not
        well_propsFile: Str, filename containing well construction details
        pumpwellsFile:  Str, filename containing well coordinates, rates, etc.
        tracer_injFile: Str, filename containing tracr injection data, if Transport is False, type ''
        outputimesFile: Str, filename containing defined output times
        obswellsFile:   Str, filename containing observation well information

    Returns:
        A new grok file
    """
    # %% PROBLEM DESCRIPTION:
    # Read problem description content from file:
    with open(os.path.join(myInDir, probdescFile), "r") as myfile:
        banner = myfile.read()  # .replace('\n', '')
    # Generate the grok file:
    Ofile = open(os.path.join(myOutDir, myoutfile), 'w')  # Generate the file
    Ofile.write(banner % (datetime.datetime.now()))  # %(time.strftime("%d/%m/%Y"))
    Ofile.flush()
    del banner
    print('<Problem description> written ok...')

    # %% GRID GENERATION: gb or directly in HGS
    banner = '\n\n!---  Grid generation\n'
    Ofile.write('%s' % banner)
    del banner
    if gridmethod == 'HGS_grade':
        Ofile.write('generate blocks interactive\n')
        dim = ['x', 'y', 'z']

        try:
            coordinates = np.loadtxt(os.path.join(myInDir, grid_coordFile))
        except IOError('Unable to load instructions'):
            raise

        if len(dim) != len(coordinates):
            raise IndexError('number of lines in coordinate file must be equal to length of dim vector')

        for cur_dim in range(0, len(dim), 1):
            Ofile.write('grade %s\n' % dim[cur_dim])

            for cur_val in range(0, coordinates.shape[1], 1):
                Ofile.write('%.8e\t' % coordinates[cur_dim, cur_val])
            Ofile.write('\n')

        Ofile.write('end generate blocks interactive\nend\n')
        Ofile.write('Mesh to tecplot\nmeshtecplot.dat\n')

    Ofile.flush()
    print('<Grid generation> written ok...')

    # %% SIMULATION PARAMETERS GROK:
    units = 'units: kilogram-metre-second'
    banner = '\n\n!---  General simulation parameters\n'

    Ofile.write('%s' % banner)
    Ofile.write('%s\n' % units)

    if FD == True: Ofile.write('Finite difference mode\n')
    if CV == True: Ofile.write('Control volume\n')
    if Transient == True: Ofile.write('Transient flow\n')
    if Transport == True: Ofile.write('do Transport\n')
    if Echo2Output == True: Ofile.write('Echo to output\n')

    # %% POROUS MEDIA PROPERTIES:
    Ofile.write('\n!--- Porous media properties\n')
    Ofile.write('use domain type\nporous media\nproperties file\n')
    Ofile.write('./%s\n\nclear chosen zones\nchoose zones all\nread properties\nporous medium\n' % (porousfile))

    if len(kfile) > 0:
        Ofile.write('\nclear chosen elements\nchoose elements all\nRead elemental k from file\n./%s\n' % (kfile))

    if Transport == True:
        name = 'fluorescein'
        difCoef = 1.000e-10
        Ofile.write('\n!--- Solute properties\n')
        Ofile.write('solute\n name\n %s\n free-solution diffusion coefficient\n %.3e\nend solute\n' % (name, difCoef))

    Ofile.flush()
    print('<Simulation param and porous media written ok...')

    # %% INITIAL CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nchoose nodes all\n'
    Ofile.write('\n!--- IC\n!--- Flow:\n%s' % banner)

    if heads_init[0] == 'output':
        try:
            Ofile.write('initial head from output file\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'onevalue':
        Ofile.write('Initial head\n')
        try:
            Ofile.write('%.3e\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'raster':
        try:
            Ofile.write('Map initial head from raster\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'txt':
        try:
            Ofile.write('Initial head from file\n')
            Ofile.write('%s\n' % (heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise

    if Transport == True:
        if transport_init[0] == 'onevalue':
            Ofile.write('\n!--- Transport:\n%s' % banner)
            Ofile.write('Initial concentration\n%.3e' % transport_init[1])

        elif transport_init[0] == 'output':
            try:
                Ofile.write('Initial concentration from output file\n')
                Ofile.write('%s\n' % (transport_init[1]))
            except TypeError('Innapropiate argument type in transport_init'):
                raise

    Ofile.flush()
    print('<Initial conditions written ok...')

    # %% BOUNDARY CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nuse domain type\nporous media\n'
    Ofile.write('\n\n!--- BC\n!--- Flow:\n%s' % banner)

    if heads_bc[0] == 'all':
        Ofile.write('\nchoose nodes all\n')
    else:
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n' % (heads_bc[0]))
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n' % (heads_bc[1]))

    Ofile.write('\ncreate node set\nconstant_head_boundary\n')
    Ofile.write('\nBoundary condition\n type\n head equals initial\n node set\n constant_head_boundary\nEnd\n')

    Ofile.flush()
    print('<Boundary conditions written ok...')

    # %% TIME STEP CONTROLS:
    if TimeStepCtrl == True:
        flowsolver_conv_ = 1.0e-6
        flowsolver_max_iter = 500
        # head_control = 0.01
        initial_timestep = 1.0e-5
        min_timestep = 1.0e-9
        max_timestep = 100.0
        max_timestep_mult = 2.0
        min_timestep_mult = 1.0e-2
        underrelax_factor = 0.5

        Ofile.write('\n!--- Timestep controls\n')
        Ofile.write('flow solver convergence criteria\n%.2e\n' % (flowsolver_conv_))
        Ofile.write('flow solver maximum iterations\n%d\n' % (flowsolver_max_iter))
        # Ofile.write('head control\n%.2e\n'%(head_control))
        Ofile.write('initial timestep\n%.2e\n' % (initial_timestep))
        Ofile.write('minimum timestep\n%.2e\n' % (min_timestep))
        Ofile.write('maximum timestep\n%.2e\n' % (max_timestep))
        Ofile.write('maximum timestep multiplier\n%.2e\n' % (max_timestep_mult))
        Ofile.write('minimum timestep multiplier\n%.2e\n' % (min_timestep_mult))
        Ofile.write('underrelaxation factor\n%.2f\n' % (underrelax_factor))
        Ofile.write('No runtime debug\n\n')
        Ofile.flush()
        print('<Default time step controllers written ok...')

    # %% WELL DEFINITION:

    if pumpwellsFile:
        Ofile.write('\n!--- Pumping wells\n\n')
        try:
            wells_data = np.genfromtxt(os.path.join(myInDir, pumpwellsFile), comments="#", delimiter='',
                                       dtype='|S12, f8, f8, f4, f4, f4, f8, f8, f8, f8')
            len(wells_data)
        except (IOError, TypeError):
            print('There was an error with the <wells> file')
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            Ofile.flush()
            Ofile.close()
            raise

        if wells_data[0] == wells_data[1]:
            length_wellsdata = 1
        else:
            length_wellsdata = len(wells_data)

        for ii in range(0, len(wells_data)):
            wellID = wells_data[ii][0]
            X = wells_data[ii][1]
            Y = wells_data[ii][2]
            top = wells_data[ii][3]
            bot = wells_data[ii][4]
            ext_point = wells_data[ii][5]
            pumprate_ini = wells_data[ii][6]
            pumprate_end = wells_data[ii][7]
            time_ini = wells_data[ii][8]
            time_end = wells_data[ii][9]

            Ofile.write('!-Well: %s\nuse domain type\nwell\nproperties file\n%s\n\n' % (wellID, well_propsFile))
            Ofile.write('clear chosen zones\nclear chosen segments\nchoose segments polyline\n2\n')
            Ofile.write('%s, %s, %s ! xyz of top of well\n' % (X, Y, top))
            Ofile.write('%s, %s, %s ! xyz of bottom of well\n' % (X, Y, bot))
            Ofile.write('new zone\n%d\n\n' % (ii + 1))
            Ofile.write('clear chosen zones\nchoose zone number\n%d\n' % (ii + 1))
            Ofile.write(
                'read properties\ntheis well\nclear chosen nodes\nchoose node\n%s, %s, %s\n' % (X, Y, ext_point))
            Ofile.write(
                'create node set\n %s\n\nboundary condition\n  type\n  flux nodal\n\n  name\n  %s\n  node set\n  %s\n' % (
                    wellID, wellID, wellID))
            Ofile.write('\n  time value table\n  %f   %f\n  %f   %f\n end\nend\n\n' % (
                time_ini, pumprate_ini, time_end, pumprate_end))

        Ofile.flush()
        print('<Pumping wells written ok...')
    if not pumpwellsFile:
        print('<Pumping wells not included in grok>')

    # %% TRACER INJECTION:
    if Transport == True:
        Ofile.write('\n!--- Tracer injection\n\n')

        try:
            tracer_data = np.genfromtxt(os.path.join(myInDir, tracer_injFile), comments="#", delimiter='',
                                        dtype='f8, f8, f4, f4, f4, f4')
            len(tracer_data)
        except:
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            print('There was an error with the Tracer Injection File')
            Ofile.flush()
            Ofile.close()
            raise
        if tracer_data[0] == tracer_data[1]:
            length_injdata = 1
        else:
            length_injdata = len(tracer_data)

        for ii in range(0, length_injdata):
            X = tracer_data[ii][0]
            Y = tracer_data[ii][1]
            Z = tracer_data[ii][2]
            time_ini = tracer_data[ii][3]
            time_end = tracer_data[ii][4]
            massflux = tracer_data[ii][5]

            Ofile.write(
                'clear chosen zones\nclear chosen segments\nclear chosen nodes\n\nuse domain type\nporous media\n')
            Ofile.write('\nchoose node\n')
            Ofile.write('%s, %s, %s\n' % (X, Y, Z))
            Ofile.write('\nspecified mass flux\n1\n%s,%s,%s\n\n' % (time_ini, time_end, massflux))

        Ofile.flush()
        print('<Tracer injection written ok...')

    # %% OUTPUT TIMES:
    if outputimesFile:
        try:
            times = np.loadtxt(os.path.join(myInDir, outputimesFile))
        except IOError:
            print('Check the directories of the output-times file')
            raise
        Ofile.write('!--- Outputs\noutput times\n')
        for ii in range(0, len(times)):
            Ofile.write('%s\n' % times[ii])

        Ofile.write('end\n' % times[ii])
        Ofile.flush()
        print('<Output times written ok...')
    if not outputimesFile:
        print('<No output times written in grok...>')

        # %% OBSERVATION WELLS:
    if obswellsFile:
        try:
            obs_wellsData = np.genfromtxt(os.path.join(myInDir, obswellsFile), comments="#", delimiter='',
                                          dtype='|S12, f8, f8, f4')
            len(obs_wellsData)
        except (IOError, TypeError, ValueError, OSError):
            print('Check the directories of the grok and output-times files')
            print('observation wells file must contain at least two lines, if only one entrance, just duplicate it')
            raise

        Ofile.write('\n!--- Observation wells\nuse domain type\nporous media\n\n')
        if obs_wellsData[0] == obs_wellsData[1]:
            length_obsData = 1
        else:
            length_obsData = len(obs_wellsData)

        for ii in range(0, length_obsData):
            ID = obs_wellsData[ii][0]
            X = obs_wellsData[ii][1]
            Y = obs_wellsData[ii][2]
            Z = obs_wellsData[ii][3]

            Ofile.write('make observation point\n')
            Ofile.write('%s\n%f  %f  %f\n' % (ID, X, Y, Z))
        Ofile.flush()
        print('<Observation wells written ok...')

    if not obswellsFile:
        print('<No observation wells written in grok...>')

    Ofile.close()
    print('GROK file finished!')


def timeStepUpdate(heads, concentrations, update_heads, update_concentrations, subtract=False):
    """ Add/subtract a time value in the outputtimes_xxx.dat files
    Arguments:
        heads:                  Boolean, add time step to heads or not
        concentrations:         Boolean, add time step to concentrations or not
        update_heads:           Integer, how many time steps should be added to heads
        update_concentrations:  Integer, how many time steps should be added to concentrations
        subtract:               Str, if it is wanted to delete last time step from file (for restart mode). Default False
    Returns:
        Length of the current times being analyzed
    """
    KalmanFilterPath = 'kalmanfilter'

    myCodeDir = dm.myMainDir()
    myPath = os.path.join(myCodeDir, '..', KalmanFilterPath, 'Model_data')

    if heads == True:
        AllTimes = np.loadtxt(os.path.join(myPath, '_outputtimesFlow.dat'))
        len_curTimes = len(np.loadtxt(os.path.join(myPath, '_outputtimesFlowUpdate.dat')))

        if subtract is False:
            len_curTimes += update_heads
            with open(os.path.join(myPath, '_outputtimesFlowUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' % AllTimes[len_curTimes - 1])  # to correct the zero index from python
            mytext.close()
        elif subtract is True:
            len_curTimes -= update_heads
            w = open(os.path.join(myPath, '_outputtimesFlowUpdate.dat'), 'w')
            for zz in range(0, len_curTimes):
                if zz < len_curTimes - 1:  # -1 is to avoid writting an empty line
                    w.write('%s\n' % AllTimes[zz])
                else:
                    w.write('%s' % AllTimes[zz])
            w.close()

    if concentrations is True:
        AllTimes1 = np.loadtxt(os.path.join(myPath, '_outputtimesTrans.dat'))
        len_curTimes1 = len(np.loadtxt(os.path.join(myPath, '_outputtimesTransUpdate.dat')))

        if subtract is False:
            len_curTimes1 += update_concentrations
            with open(os.path.join(myPath, '_outputtimesTransUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' % AllTimes1[len_curTimes1 - 1])  # to correct the zero index from python
            mytext.close()
        if subtract is True:
            len_curTimes1 -= update_concentrations
            w = open(os.path.join(myPath, '_outputtimesTransUpdate.dat'), 'w')
            for zz in range(0, len_curTimes1):
                if zz < len_curTimes1 - 1:  # -3 is to avoid writting an empty line
                    w.write('%s\n' % AllTimes1[zz])
                else:
                    w.write('%s' % AllTimes1[zz])
            w.close()

    print('Times updated...')

    if (heads is True) and (concentrations is True):
        return len_curTimes + len_curTimes1
    elif (concentrations is False) and (heads is True):
        return len_curTimes
    elif (concentrations is True) and (heads is False):
        return len_curTimes1


def setParam2value():
    print('construction...')

def rd_all_insta_mprops(all_insta_dir,nsoil_zones,mystr):
    """read all the mprops inside the instances dirs
    Arguments:
        #all_insta_dir: directory where the isntances are(one folder for each instance)
        #nsoil_zones: number of soil zones, would be equal to the total number of parameters
        # mystr: the string of the parameter to extract from the .mprops file e.g. alphaini
    Returns:
        parameter: array with the parameters values
    """
    insta_list = dm.getdirs(all_insta_dir,'hgs_inst',fullpath=True)
    mprops_param=np.zeros([nsoil_zones,len(insta_list)])
    for i,curr_insta in enumerate(insta_list):
        mprops_file = dm.getdirs(curr_insta, '.mprops',fullpath=True)[0]
        temp_param = rd_mprops_parameter(mprops_file, mystr)
        mprops_param[:,i] = temp_param
    return mprops_param

def rd_mprops_parameter(myfile, mystr):
    """ reads the parameter inside the .mprops file
    # myfile: the .mprops file
    # mystr: the string of the parameter to extract from the .mprops file e.g. alphaini
    
    Returns:
        parameter: array with the parameters values
    """
    par_id, next_str = dm.str_infile(myfile, mystr, index=True)
    parameter = np.empty((par_id.shape[0],))
    with open(myfile) as fp:
        mylines = fp.readlines()
    for ii in range(par_id.shape[0]):
        
        if 'alphaini' in mystr or 'betaini' in mystr:
            parameter[ii] = float(mylines[par_id[ii]+2])
        else:
            parameter[ii] = float(mylines[par_id[ii]+1])
    return parameter

def copy_mprops(src_instadir,dst_instadir):
    # copy the .mprops files from the pre_spinup instances from a source dir to the current test dir
    src_insta_list = os.listdir(src_instadir)
    dst_insta_list = os.listdir(src_instadir)
    if len(src_insta_list) != len(dst_insta_list): 
        print('ERROR: pre_spinup number of instance must be equal to defined in filter config')
        sys.exit
    for curr_insta in src_insta_list:
        curr_mprops = dm.getdirs(os.path.join(src_instadir,curr_insta),mystr='.mprops')[0]
        shutil.copyfile(os.path.join(src_instadir,curr_insta,curr_mprops),
                        os.path.join(dst_instadir,curr_insta,curr_mprops))

def settings_verification(kkk,kzones,alpha_vanG,beta_vanG,hyd_heads,head2gwt,wat_cont,copy_all_insta,only_mprops):
    # Verify that the user did not choose incompatible settings
    if kkk is True and kzones is True:
        print('\nOnly kkk or kzones can be "True", not both')
        print('Program interrupted')
        sys.exit()
        
    if kkk is False and kzones is False and alpha_vanG is False and beta_vanG is False:
        print('\nNo parameters to optimize have been selected, please select at least 1 "True"')
        print('Program interrupted')
        sys.exit()
    
    if hyd_heads is False and head2gwt is False and wat_cont is False:
        print('\nNo observation has been selected')
        print('Program interrupted')
        sys.exit()

    if (hyd_heads+head2gwt+wat_cont) >1:
        print('\n2 Types of observations have been selected, only 1 is allowed')
        print('Program interrupted')
        sys.exit()

    if wat_cont==True:
        print('****  WARNING  ****')
        print('Water Content is selected as Observation')
        print('The id of the obs must be the element and NOT the node')
        print('*******************')
        
    if copy_all_insta == True and only_mprops == True:
        print('copy_all_insta and only_mprops are both True,')
        print('Only one can be true')


def printSummary(nmem, kkk, inihead, OL, iniwatdepth, spinup, parallel, cpus, subgrid, plot, model_name, test_name):
        # Print a summary of the user configuration
    print('==============       Summary of user settings       ==============\n')
    print('Parallel Computing: ', parallel)
    print('N° of CPUs: ', cpus)
    print('Subgrid: ', subgrid)
    print('Plot', plot)
    print('Hydraulic conductivity field kkk: ', kkk)
    print('Initial head: ', inihead)
    print('Initial Water depth', iniwatdepth)
    print('Open Loop: ', OL)
    print('Spinup: ', spinup)
    print(' --------- Filter settings   ---------')
    print('N° of ensemble members: ', nmem)
    print('Model name:', model_name)
    print('Test name: ', test_name)
    print('====================================================================== \n', )


import re

def tecplot_reader(file, separator_key='#'):
    # an alternative function to read tecplot files
    var_dics = dict()
    
    i=0
    with open(file, 'r') as a:
        for idx, line in enumerate(a.readlines()):
            if idx < 3 : #or '#' in line
                continue
            
            elif separator_key in line:
                i=i+1
                result = re.search(separator_key+' (.*)\n', line).group(1)
                var_dics['var'+str(i)+'_'+str(result)]=[]
                continue
            if i>0:
                var_dics['var'+str(i)+'_'+str(result)].extend([float(s) for s in line.split()])

    return var_dics




def nodes2cc(array_nodes,element_nodes):
    """ function to transform data array from nodes to cell centered data, this is useful
    # for data like saturation when multiplied by porosity
    """
    array_cc = np.zeros(element_nodes.shape[0])
    for i in range(element_nodes.shape[0]):
        array_cc[i] = np.mean(array_nodes[element_nodes[i,:]-1])

    return array_cc

def cell_centered2nodes(poro_data, nodes_coord):
    """
    #function to transform data from cell centered to nodes.
    #(this would be the inversre of the nodes2cc)
    """
    x = np.unique(nodes_coord[:,0]).shape[0]
    y = np.unique(nodes_coord[:,1]).shape[0]
    z = int(nodes_coord.shape[0]/(x*y))
    
    x,y,z = x-1, y-1, z -1
        
    if type(poro_data) is list or poro_data.ndim==1:

        poro_3Darray = np.reshape(poro_data,(x,y,z), order='F')
        
    elif poro_data.ndim==3:
        poro_3Darray = poro_data
        #x = poro_3Darray.shape[0]  #for individual variable definition use like this
        #y = poro_3Darray.shape[1]
        #z = poro_3Darray.shape[2]
    
    poro_nodes = np.append(poro_3Darray,np.reshape(poro_3Darray[-1,:,:],(1,y,z)),axis=0)
                          
    poro_nodes = np.append(poro_nodes,np.reshape(poro_nodes[:,-1,:],(x+1,1,z)), axis=1)
    poro_nodes = np.append(poro_nodes,np.reshape(poro_nodes[:,:,-1],(x+1,y+1,1)), axis=2)
    
    poro_nodes_1D = np.reshape(poro_nodes,(x+1)*(y+1)*(z+1), order='F')
    
    return poro_nodes_1D


def verify_constrain_kzones(postX,nsoil_zones,min_kzones=None,max_kzones=None):
    # function just to verify that the constrains are being set correctly
    postX_exp = np.exp(postX)
    for i in range(0,nsoil_zones):
        if min_kzones is not None:
            min_kzones_id =  np.where(np.exp(postX[i,:])<min_kzones)
            postX[i,min_kzones_id] = np.log(min_kzones)
        if max_kzones is not None:
            max_kzones_id = np.where(np.exp(postX[i,:])>max_kzones)
            postX[i,max_kzones_id] = np.log(max_kzones)
    postX_exp = np.exp(postX)    
    return postX

def verify_constrain_alphavg(postX,nsoil_zones,min_alphavg=None,max_alphavg=None):
    # function just to verify that the constrains are being set correctly
    postX_exp = np.exp(postX)
    for i in range(nsoil_zones,nsoil_zones+nsoil_zones):
        if min_alphavg is not None:
            min_alphavg_id =  np.where(np.exp(postX[i,:])<min_alphavg)
            postX[i,min_alphavg_id] = np.log(min_alphavg)
        if max_alphavg is not None:
            max_alphavg_id = np.where(np.exp(postX[i,:])>max_alphavg)
            postX[i,max_alphavg_id] = np.log(max_alphavg)
    postX_exp = np.exp(postX)    
    return postX

def verify_constrain_betavg(postX,nsoil_zones,min_betavg=None,max_betavg=None):
    # function just to verify that the constrains are being set correctly
    postX_exp = np.exp(postX)
    for i in range(nsoil_zones*2,nsoil_zones*3):
        if min_betavg is not None:
            min_betavg_id =  np.where(np.exp(postX[i,:])<min_betavg)
            postX[i,min_betavg_id] = np.log(min_betavg)
        if max_betavg is not None:
            max_betavg_id = np.where(np.exp(postX[i,:])>max_betavg)
            postX[i,max_betavg_id] = np.log(max_betavg)
    postX_exp = np.exp(postX)    
    return postX


def save_other_obsdata(keyword, da_dirs, nnodes, test_name, counter_simsteps, et_obsnodes=None, wc_obselem=None, poro_data=None, element_nodes=None, HH_obsnodes=None, et_sum=False, OL=False, upd=False):
    """ saves other observation data that is not used explicitely for the update
    #One at a time, can be:
    # disch: river discharge
    # et: evapotranspiration
    # WC: water content 
    # HH: hydraulic head
    #print(keyword+' Saving other_obs_data to: '+da_dirs['other_obsdata_dir'])
    
    Returns:
        None
    """
    insta_list = dm.getdirs(da_dirs['inst_dir'], mystr='hgs_inst', fullpath=True)
    #if it is open loop
    if OL==True:
        
        
        if keyword == 'disch':
            
            
            for j in range(len(insta_list)):
                
                disch_file = dm.getdirs(insta_list[j], mystr='hydrograph.Outlet.dat', fullpath=True)[0]
                shutil.copy(disch_file, os.path.join(da_dirs['extra_obs_disch_dir'], '%s_OL.disch_obs_%.3d.dat' % ('hydrograph.Outlet', j)))
                
            # this is for the case of only 1 discharge data, if there are more than 1, another implementation must be done. and then save the desired data as shown below
            #np.save(os.path.join(da_dirs['other_obsdata_dir'],'disch_obs_OL'+'.npy'),disch_obs)                
            
        elif keyword =='et':
            et_file_list = dm.getdirs(insta_list[0], mystr='ETTotal_olf', fullpath=True)
            
            if et_sum==True: et_obs = np.zeros([len(et_file_list), len(insta_list)])
            if et_sum==False: et_obs = np.zeros([len(et_file_list), len(insta_list), len(et_obsnodes)])
            
            for j in range(len(insta_list)):
                
                et_files = dm.getdirs(insta_list[j], mystr='ETTotal_olf', fullpath=True)
                
               
                for i in range(len(et_files)):
    
                    et_bin , ignore= readHGSbin(et_files[i],nnodes)
                    
                    if et_sum==True: et_obs[i,j] = np.sum(et_bin)
                    if et_sum==False: et_obs[i,j,:] = et_bin[et_obsnodes]
                    
            np.save(os.path.join(da_dirs['extra_obs_et_dir'],'et_obs_OL'+'.npy'),et_obs)
    
        elif keyword =='WC':
            wc_file_list_preview = dm.getdirs(insta_list[0], mystr='sat_pm', fullpath=True)
            wc_obs = np.zeros([len(wc_obselem),len(insta_list),len(wc_file_list_preview)])
            for j in range(len(insta_list)):
                
                wc_files = dm.getdirs(insta_list[j], mystr='sat_pm', fullpath=True)
                
               
                for i in range(len(wc_files)):
    
                    sat_bin , ignore= readHGSbin(wc_files[i],nnodes)
                    
                    sat_states_cc = nodes2cc(sat_bin,element_nodes) #jaime
                    wc_states = sat_states_cc*poro_data #jaime
                    
                    wc_obs[:,j,i] = wc_states[wc_obselem-1]
      
            np.save(os.path.join(da_dirs['extra_obs_WC_dir'],'wc_obs_OL'+'.npy'),wc_obs)
            
        elif keyword =='HH':
            HH_file_list_preview = dm.getdirs(insta_list[0], mystr='head_pm', fullpath=True)
            HH_obs = np.zeros([len(HH_obsnodes), len(insta_list), len(HH_file_list_preview)])
            for j in range(len(insta_list)):
                
                HH_files = dm.getdirs(insta_list[j], mystr='head_pm', fullpath=True)
                
               
                for i in range(len(HH_files)):
    
                    HH_bin , ignore= readHGSbin(HH_files[i],nnodes)
                    HH_obs[:,j,i] = HH_bin[HH_obsnodes]
      
            np.save(os.path.join(da_dirs['extra_obs_HH_dir'],'HH_obs_OL'+'.npy'),HH_obs)
            
            
    elif OL==False:
        
        if keyword == 'disch':
        
            disch_obs_dic = {}
            
            for j in range(len(insta_list)):
                disch_file = dm.getdirs(insta_list[j], mystr='hydrograph.Outlet.dat', fullpath=True)[0]
                disch_load_file = np.loadtxt(disch_file,skiprows=3)
                disch_obs_dic['insta'+str(j)] = disch_load_file[:,[0,-1]]
            if upd==True:   
                np.save(os.path.join(da_dirs['extra_obs_disch_dir'], '%s_all.disch_obs.%.3d.upd.npy' % (test_name, counter_simsteps)), disch_obs_dic)
            if upd==False:    
                np.save(os.path.join(da_dirs['extra_obs_disch_dir'], '%s_all.disch_obs.%.3d.npy' % (test_name, counter_simsteps)), disch_obs_dic)    
        
        
        elif keyword =='et':
        
            if et_sum==True:  et_obs = np.zeros([len(insta_list)])
            if et_sum==False: et_obs = np.zeros([len(et_obsnodes),len(insta_list)])
            for i in range(len(insta_list)):
                
                et_file = dm.getdirs(insta_list[i], mystr='ETTotal_olf', fullpath=True)[0]
                et_bin , ignore= readHGSbin(et_file,nnodes)
                
                if et_sum==True:  et_obs[i] = np.sum(et_bin)
                if et_sum==False: et_obs[:,i] = et_bin[et_obsnodes]
            
            
            if upd==True:   
                np.savetxt(os.path.join(da_dirs['extra_obs_et_dir'], '%s_all.et_obs.%.3d.upd.txt' % (test_name, counter_simsteps)), et_obs,
                           header="et_obs. ts=%5.4e" % counter_simsteps)
                
            if upd==False:    
    
                np.savetxt(os.path.join(da_dirs['extra_obs_et_dir'], '%s_all.et_obs.%.3d.txt' % (test_name, counter_simsteps)), et_obs,
                           header="et_obs. ts=%5.4e" % counter_simsteps)

        elif keyword =='WC':
        
            wc_obs = np.zeros([len(wc_obselem),len(insta_list)])
            for i in range(len(insta_list)):
                
                sat_file = dm.getdirs(insta_list[i], mystr='sat_pm', fullpath=True)[0]
                sat_bin , ignore = readHGSbin(sat_file,nnodes)
                sat_states_cc = nodes2cc(sat_bin,element_nodes) #jaime
                wc_states = sat_states_cc*poro_data #jaime
                
                wc_obs[:,i] = wc_states[wc_obselem-1]


            
            if upd==True:   
                np.savetxt(os.path.join(da_dirs['extra_obs_WC_dir'], '%s_all.wc_obs.%.3d.upd.txt' % (test_name, counter_simsteps)), wc_obs,
                           header="wc_obs. ts=%5.4e" % counter_simsteps)
            if upd==False:    
                np.savetxt(os.path.join(da_dirs['extra_obs_WC_dir'], '%s_all.wc_obs.%.3d.txt' % (test_name, counter_simsteps)), wc_obs,
                           header="wc_obs. ts=%5.4e" % counter_simsteps)
        
        
        elif keyword =='HH':
        
            HH_obs = np.zeros([len(HH_obsnodes),len(insta_list)])
            for i in range(len(insta_list)):
                
                HH_file = dm.getdirs(insta_list[i], mystr='head_pm', fullpath=True)[0]
                HH_bin , ignore= readHGSbin(HH_file,nnodes)
                HH_obs[:,i] = HH_bin[HH_obsnodes]
            
            
            if upd==True:   
                np.savetxt(os.path.join(da_dirs['extra_obs_HH_dir'], '%s_all.HH_obs.%.3d.upd.txt' % (test_name, counter_simsteps)), HH_obs,
                           header="HH_obs. ts=%5.4e" % counter_simsteps)
                
            if upd==False:    

                np.savetxt(os.path.join(da_dirs['extra_obs_HH_dir'], '%s_all.HH_obs.%.3d.txt' % (test_name, counter_simsteps)), HH_obs,
                           header="HH_obs. ts=%5.4e" % counter_simsteps)            
